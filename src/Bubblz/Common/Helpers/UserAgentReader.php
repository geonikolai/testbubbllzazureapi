<?php
namespace Bubblz\Common\Helpers;

use Bubblz\Common\Models\UserAgent;

class UserAgentReader 
{    
    /**
     * 
     * @param string $ua
     * @return UserAgent
     */
    public static function analyzeUserAgent($ua)
    {
        $userAgent = new UserAgent();
        
        $temp = split(";", $ua);
        foreach ($temp as $val)
        {
            if(strpos($val, "app/") > -1)
            {
                $t = split("/", $val);
                $userAgent->appVersion = $t[1];
            }
            else if(strpos($val, "API-Version:") > -1)
            {
                $t = split(":", $val);
                $userAgent->api = $t[1];
            }
            else if(strpos($val, "platform:") > -1)
            {
                $t = split(":", $val);
                $userAgent->platform = $t[1];
            }
            else if(strpos($val, "os_version:") > -1)
            {
                $t = split(":", $val);
                $userAgent->os_version = $t[1];
            }
            else if(strpos($val, "model:") > -1)
            {
                $t = split(":", $val);
                $userAgent->model = $t[1];
            }
            else if(strpos($val, "manufacturer:") > -1)
            {
                $t = split(":", $val);
                $userAgent->manufacturer = $t[1];
            }
        }
        
        return $userAgent;
    }
    
    /**
     * 
     * @param UserAgent $userAgent
     * @return mixed
     */
    public static function checkUserAgentIsValid(UserAgent $userAgent)
    {
        return true;
    }
}
