<?php
namespace Bubblz\Common\Models;

class UserAgent {
    public $appVersion;
    public $platform;
    public $api;
    public $os_version;
    public $model;
    public $manufacturer;
}
