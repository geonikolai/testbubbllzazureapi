<?php
namespace Bubblz\ApiBundle\Models;

class ApiResponse {
    /**
     *
     * @var string
     */
    public $message;
    
    /**
     *
     * @var integer
     */
    public $status;
    
    /**
     *
     * @var mixed
     */
    public $data;
    
    public function __construct($message, $status, $data) {
        $this->message = $message;
        $this->status = $status;
        $this->data = $data;
    }
}
