<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use Bubblz\EntitiesBundle\Entity\Boom;
use Bubblz\EntitiesBundle\Entity\Checkin;
use Bubblz\EntitiesBundle\Managers\BubbllzManager;
use Bubblz\EntitiesBundle\Managers\CheckinManager;
use Bubblz\EntitiesBundle\Managers\MarketingManager;
use DateTime;
use DateTimeZone;
use Exception;
use Facebook\Facebook;
use Facebook\FacebookApp;
use Facebook\FacebookRequest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class BubbllzController extends FOSRestController
{

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function createBubbllzAction(Request $request, $storeid, $offerid, $offertype)
    {
        try
        {
            $message = $request->request->get('message');
            
            /* @var $checkinManager CheckinManager */
            $checkinManager = $this->get("bubblz_entities.checkin_manager");
            
            /* @var $checkin Checkin */
            $checkin = $checkinManager->GetCheckinIsBubbllByUserAndStore($storeid, $this->getUser()->getId());
            
            if($checkin == null)
            {
                throw new Exception("The user has not checked in on store.");
            }

            /* @var $bubbllzManager BubbllzManager */
            $bubbllzManager = $this->get("bubblz_entities.bubbllz_manager");
            
            /* @var $boom Boom */
            $boom = $bubbllzManager->CreateNewBubbll($storeid, $checkin->getId(), $this->getUser()->getId(), $message, $offerid, $offertype);

            /* @var $marketingManager MarketingManager */
            $marketingManager = $this->get("bubblz_entities.marketing_manager");
            
            $isUnattended = $marketingManager->CheckIsUnattended($offerid, $offertype);
            if($isUnattended)
            {
                $now = new DateTime("now", new DateTimeZone("Europe/Athens"));
//                $boom->setModifiedDate($now);
                $boom->setPDate($now);
                $boom->setStatus(1);
                if($offertype == "contest")
                {
                    $marketingManager->AddContestEntry($offerid);
//                    $boom->setStatus(1);
                }
            }
            
            $this->getDoctrine()->getManager()->persist($boom);

            $checkin->setIsBubbll(true);

            $this->getDoctrine()->getManager()->flush();

            $responseData = New ApiResponse("", 200, array('status' => intval($boom->getStatus()), 'message' => '', 'ticket' => $boom->getTicket(), 'bubbllzId' => intval($boom->getId())));
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function uploadBubbllzImageAction(Request $request, $bubbllId)
    {
        try
        {
            $image64 = $request->request->get('image');
            $image64 = trim($image64);
            $image64 = str_replace(' ', '+', $image64);
            $imageData = base64_decode($image64);

            $basePath = $this->get('kernel')->getRootDir() . "/../web/images/booms/";
            $imageName = $this->generateRandomString(32) . ".jpg";

            file_put_contents($basePath . $imageName, $imageData);

            /* @var $bubbll Boom */
            $bubbllz = $this->getDoctrine()->getRepository(Boom::class)->find($bubbllId);
            $bubbllz->setImageName($imageName);

            $this->getDoctrine()->getManager()->flush();

            $responseData = New ApiResponse("", 200, array('image' => 'http://bubbllz.com/post/index.php?id='.$bubbllId));
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function updateFacebookLikesAction(Request $request)
    {
        try
        {
            $bubbllzArray = json_decode($request->request->get('likes'));
            if (!is_array($bubbllzArray))
            {
                throw new Exception("Η παράμετρος δεν ειναι σε μορφή array.");
            }
            
            foreach ($bubbllzArray as $val)
            {
                /* @var $bubbl Boom */
                $bubbllz = $this->getDoctrine()->getRepository(Boom::class)->find($val->bubbllId);
                if($bubbllz != null)
                {
                    $bubbllz->setFbLikes($val->likes);
                    $bubbllz->setFbLove($val->love);
                    $bubbllz->setFbHaha($val->haha);
                    $bubbllz->setFbWow($val->wow);
                    $bubbllz->setFbSad($val->sad);
                    $bubbllz->setFbAngry($val->angry);

                    $this->getDoctrine()->getManager()->flush();
                }
            }

            $responseData = New ApiResponse("", 200, array());
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function updateFacebookIdAction(Request $request)
    {
        try
        {
            $bubbllzArray = json_decode($request->request->get('bubbllz'));
            if (!is_array($bubbllzArray))
            {
                throw new Exception("Η παράμετρος δεν ειναι σε μορφή array.");
            }
            
            foreach ($bubbllzArray as $val)
            {
                /* @var $bubbl Boom */
                $bubbllz = $this->getDoctrine()->getRepository(Boom::class)->find($val->bubbllId);
                if($bubbllz != null)
                {
                    $bubbllz->setFbId($val->facebookId);
                    $this->getDoctrine()->getManager()->flush();
                }
            }

            $responseData = New ApiResponse("", 200, array());
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    
   
    public function updatePageReactionsAction($bubbllzId)
    {
        
        try
        {
            $id = (int)$bubbllzId;
            $em = $this->getDoctrine()->getManager();
            $bubbllz = $em->getRepository(Boom::class)->find($id);
            $user = $bubbllz->getBoomUser();
            $fbToken = $user->getFbAccessToken();
            $fbId = $bubbllz->getFbId();
            $fb = new Facebook([
                  'app_id' => '1468213856775096',
                  'app_secret' => '7ddf42854e072006300ab93111b53af1',
                  'default_graph_version' => 'v2.8'
            ]);
        
            $fbApp = new FacebookApp('1468213856775096', '7ddf42854e072006300ab93111b53af1');
            $request = new FacebookRequest(
                $fbApp,
                $fbToken,
                'GET',
                '/',
                array(
                    'ids' => $fbId,
                    'fields' => 'reactions.type(LIKE).summary(total_count).limit(0).as(like),'
                    . ' reactions.type(LOVE).summary(total_count).limit(0).as(love),'
                    . ' reactions.type(WOW).summary(total_count).limit(0).as(wow),'
                    . ' reactions.type(HAHA).summary(total_count).limit(0).as(haha),'
                    . ' reactions.type(SAD).summary(total_count).limit(0).as(sad),'
                    . ' reactions.type(ANGRY).summary(total_count).limit(0).as(angry)')
                );

            // Send the request to Graph
            try {
              $response = $fb->getClient()->sendRequest($request);
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
              // When Graph returns an error
              echo 'Graph returned an error: ' . $e->getMessage();
              exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
              // When validation fails or other local issues
              echo 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
            }
            
            $array = $response->getDecodedBody();
            if($array[$fbId]['id']){
                $bubbllz->setFbLikes($array[$fbId]['like']['summary']['total_count']);
                $bubbllz->setFbLove($array[$fbId]['love']['summary']['total_count']);
                $bubbllz->setFbHaha($array[$fbId]['haha']['summary']['total_count']);
                $bubbllz->setFbWow($array[$fbId]['wow']['summary']['total_count']);
                $bubbllz->setFbSad($array[$fbId]['sad']['summary']['total_count']);
                $bubbllz->setFbAngry($array[$fbId]['angry']['summary']['total_count']);
                $em->flush();
            }
            
            $responseData = New ApiResponse($array[$fbId]['like']['summary']['total_count'].' '.
                    $array[$fbId]['love']['summary']['total_count'].' '.
                    $array[$fbId]['haha']['summary']['total_count'].' '.
                    $array[$fbId]['wow']['summary']['total_count'].' '.
                    $array[$fbId]['sad']['summary']['total_count'].' '.
                    $array[$fbId]['angry']['summary']['total_count'], 200, array());

        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
