<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use Bubblz\EntitiesBundle\Entity\Checkin;
use Bubblz\EntitiesBundle\Entity\Contest;
use Bubblz\EntitiesBundle\Managers\BubbllzManager;
use Bubblz\EntitiesBundle\Managers\CheckinManager;
use Bubblz\EntitiesBundle\Managers\StoreManager;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;

class CheckinController extends FOSRestController
{

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function checkingMessageAction($storeid, $offerid = -1, $offertype = "")
    {
        try
        {
            /* @var $bubbllzManager BubbllzManager */
            $bubbllzManager = $this->get("bubblz_entities.bubbllz_manager");


            //if ($bubbllzManager->HasPendingBubbllz($storeid, $this->getUser()->getId()))
            if ($bubbllzManager->GetPendingBubbllz($this->getUser()->getId()))
            {
                $responseData = New ApiResponse("Pending checkins", 502, null);
                $view = $this->view($responseData, 200);

                return $this->handleView($view);
            }

            /* @var $checkinManager CheckinManager */
            $checkinManager = $this->get("bubblz_entities.checkin_manager");

            /* @var $checkin Checkin */
            $pendingCheckin = $checkinManager->GetActiveCheckins($storeid, $this->getUser()->getId());

            if (count($pendingCheckin) > 0)
            {
                foreach ($pendingCheckin as &$v)
                {
                    $v->setStatus(0);
                    $this->getDoctrine()->getManager()->flush();
                }
            }

            /* @var $storeManager StoreManager */
            $storeManager = $this->get("bubblz_entities.store_manager");
            $message = $storeManager->GetStoreMessageByStoreId($offerid, $offertype);

            try
            {
                $checkin = $checkinManager->CreateNewCheckin($this->getUser(), $storeid, $offerid, $offertype);
            } catch (Exception $ex)
            {
                $responseData = new ApiResponse($ex->getMessage(), 504, null);
                return $responseData;
            }


            $this->getDoctrine()->getManager()->persist($checkin);
            $this->getDoctrine()->getManager()->flush();

            if ($offerid != -1 && $offertype != "")
            {
                switch ($offertype)
                {
                    case "contest":
                        /* @var $marketingPlan Contest */
                        $marketingPlan = $this->getDoctrine()->getRepository(Contest::class)->find($offerid);
                        if ($marketingPlan != null)
                        {
                            if ($marketingPlan->getBrandContest() != null)
                            {
                                $connection = $this->getDoctrine()->getConnection();
                                $stmt = $connection->prepare("update checkin set brand_id=" . $marketingPlan->getBrandContest()->getBrand()->getId() . " where id=" . $checkin->getId());
                                $stmt->execute();
                            }
                        }
                        break;
                    case "offer":
                    case "discount":
                    case "loyalty":
                        break;
                }
            }

            $responseData = new ApiResponse("", 200, $message);
        } catch (Exception $ex)
        {
            $responseData = new ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function cancelActiveBubbllAction($storeId, $offerId)
    {
        try
        {
            /* @var $bubbllzManager BubbllzManager */
            $bubbllzManager = $this->get("bubblz_entities.bubbllz_manager");

            $bubbllzManager->CancelBubbllz($storeId, $this->getUser()->getId());

            $responseData = New ApiResponse("", 200, array());
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

}
