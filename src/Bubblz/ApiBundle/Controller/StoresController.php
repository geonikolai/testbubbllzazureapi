<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;

class StoresController extends FOSRestController {

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getAllStoresAction(\Symfony\Component\HttpFoundation\Request $request) {
        try {
            $stores = $this->get("bubblz_entities.store_manager")->GetStoresForApi($request);

            $responseData = New ApiResponse("", 200, $stores);
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getStoresAfterDatetimeAction($datetime) {
        try {
            $stores = $this->get("bubblz_entities.store_manager")->GetStoresAfterDatetime($datetime);

            $responseData = New ApiResponse("", 200, $stores);
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

}
