<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\Exception\Exception;

class MarketingController extends FOSRestController {

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getAllMarketingsAction() {
        try {
            
            $entities = $this->get("bubblz_entities.marketing_manager")->GetAllMarketing();

            $responseData = New ApiResponse("", 200, $entities);
           
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getMarketingByStoreAction($storeId) {
        try {
            
            $entities = $this->get("bubblz_entities.marketing_manager")->GetMarketingByStore($storeId);

            $responseData = New ApiResponse("", 200, $entities);
           
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getMarketingByGeoRadiusAction($lat, $long, $radius) {
        try {
            
            $entities = $this->get("bubblz_entities.marketing_manager")->GetMarketingByGeoRadius($lat, $long, $radius);

            $responseData = New ApiResponse("", 200, $entities);
           
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getMarketingsAfterDatetimeAction($datetime) {
        try {
            
            $entities = $this->get("bubblz_entities.marketing_manager")->GetMarketingAfterDatetime($datetime);

            $responseData = New ApiResponse("", 200, $entities);
           
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    
    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function jsonAction() {
        try {
            $data = $this->get("bubblz_entities.marketing_manager")->jsonData();

            $responseData = New ApiResponse("", 200, $data);
        } catch (Exception $ex) {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }
    

    
}
