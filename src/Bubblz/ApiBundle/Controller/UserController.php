<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use Bubblz\EntitiesBundle\Entity\Boom;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class UserController extends FOSRestController
{

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getBubbllzCountAction(Request $request)
    {
        try
        {
            $user = $this->getUser();

            $booms = $this->getDoctrine()->getRepository(Boom::class)->createQueryBuilder('b')
                    ->select("b.id, s.id as storeId, s.storename, b.ticket, b.boomUserMessage as hashtags,"
                            . " CONCAT('http://" . $request->getHost() . "/images/booms/', b.imageName) as image,"
                            . " CONCAT('http://bubbllz.com/post/index.php?id=', b.id) as postUrl,"
                            . " b.cDate as time, b.status, COALESCE(b.fbLikes, '') as fbLikes, COALESCE(b.fbId, '') as fbId")
                    ->leftJoin('b.boomUser', 'u')
                    ->leftJoin('b.store', 's')
                    ->where('u.id = :id')
//                    ->andWhere('b.status = 1 or b.status = 0')
                    ->setParameter('id', $user->getId())
                    ->getQuery()
                    ->getArrayResult();

            foreach ($booms as &$val)
            {
                $hashes = explode(" ", $val["hashtags"]);
                $newHashArray = [];
                foreach ($hashes as $vval)
                {
                    if ($vval != null || $vval != "")
                    {
                        $newHashArray[] = str_replace("\\", "", $vval);
                    }
                }
                $val["hashtags"] = $newHashArray;
            }

            $responseData = New ApiResponse("", 200, $booms);
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

}
