<?php

namespace Bubblz\ApiBundle\Controller;

use Bubblz\ApiBundle\Models\ApiResponse;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;

class HashTagsController extends FOSRestController
{

    /**
     * @FOS\RestBundle\Controller\Annotations\View
     */
    public function getHashtagsAction($storeid, $offerid)
    {
        try
        {
            $hashtags = $this->getDoctrine()->getRepository(\Bubblz\EntitiesBundle\Entity\StoreToCategory::class)->createQueryBuilder('s')
                    ->select('distinct c.hashTags')
                    ->leftJoin('BubblzEntitiesBundle:Category', 'c', \Doctrine\ORM\Query\Expr\Join::WITH, 's.categoryId=c.id')
                    ->where('s.storeId = :storeId')
                    ->andWhere('c.hashTags != :hastag')
                    ->setParameter('storeId', $storeid)
                    ->setParameter('hastag', '')
                    ->getQuery()
                    ->getArrayResult();
            
            $hasString = "";
            $counter = 0;
            foreach ($hashtags as $val)
            {
                if($counter != 0)
                {
                    $hasString .= ",";
                }
                $hasString .= $val["hashTags"];
                $counter++;
            }
            trim($hasString, ",");
            $responseData = New ApiResponse("", 200, split(",", $hasString));
        } catch (Exception $ex)
        {
            $responseData = New ApiResponse($ex->getMessage(), 400, null);
        }

        $view = $this->view($responseData, 200);

        return $this->handleView($view);
    }

}
