<?php

namespace Bubblz\OAuthServerBundle\Security\Firewall;

use Bubblz\OAuthServerBundle\Exceptions\Oauth2TokenNotFoundException;
use FOS\OAuthServerBundle\Security\Authentication\Token\OAuthToken;
use FOS\OAuthServerBundle\Security\Firewall\OAuthListener as BaseListener;
use OAuth2\OAuth2AuthenticateException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class OAuthListener extends BaseListener {

    /**
     * @param GetResponseEvent $event The event.
     */
    public function handle(GetResponseEvent $event) {
        try {
            if (null === $oauthToken = $this->serverService->getBearerToken($event->getRequest(), true)) {
                throw new Oauth2TokenNotFoundException('invalid_request', 'The token was not found in headers');
            }
        }
        catch (Oauth2TokenNotFoundException $ex) {
            $customReposnse = new Response(
                    json_encode(array('data' => '', 'message' => $ex->getDescription(), 'status' => $ex->getHttpCode())), 200, array(
                'Content-Type' => 'application/json',
                'Cache-Control' => 'no-store',
                'Pragma' => 'no-cache',
                    )
            );

            $event->setResponse($customReposnse);
            return;
        }
        catch (OAuth2AuthenticateException $ex) {
            $customReposnse = new Response(
                    json_encode(array('data' => '', 'message' => $ex->getMessage(), 'status' => 501)), 200, array(
                'Content-Type' => 'application/json',
                'Cache-Control' => 'no-store',
                'Pragma' => 'no-cache',
                    )
            );

            $event->setResponse($customReposnse);
            return;
        }

        $token = new OAuthToken();
        $token->setToken($oauthToken);

        try {
            $returnValue = $this->authenticationManager->authenticate($token);

            if ($returnValue instanceof TokenInterface) {
                return $this->securityContext->setToken($returnValue);
            }

            if ($returnValue instanceof Response) {
                return $event->setResponse($returnValue);
            }
        } catch (AuthenticationException $e) {
            if (null !== $p = $e->getPrevious()) {
                $customReposnse = new Response(
                        json_encode(array('data' => '', 'message' => 'The token has expired', 'status' => 501)), 200, array(
                    'Content-Type' => 'application/json',
                    'Cache-Control' => 'no-store',
                    'Pragma' => 'no-cache',
                        )
                );

                $event->setResponse($customReposnse);
            }
        }
    }

}
