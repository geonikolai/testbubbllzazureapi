<?php

/*
 * This file is part of the FOSOAuthServerBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Bubblz\OAuthServerBundle\Storage;

use Bubblz\Common\Helpers\UserAgentReader;
use Bubblz\EntitiesBundle\Entity\BoomUser;
use Bubblz\OAuthServerBundle\Events\UserLogedInEvent;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use FOS\OAuthServerBundle\Storage\OAuthStorage as BaseStorage;
use FOS\UserBundle\Model\UserManagerInterface;
use InvalidArgumentException;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OAuthStorage extends BaseStorage
{

    /**
     *
     * @var UserManagerInterface
     */
    private $userManager;
    
    /**
     *
     * @var ContainerInterface
     */
    private $container;
    
    /**
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    /**
     *
     * @var EventDispatcher
     */
    private $eventDispatcher;

    public function __construct(ClientManagerInterface $clientManager, AccessTokenManagerInterface $accessTokenManager, RefreshTokenManagerInterface $refreshTokenManager, AuthCodeManagerInterface $authCodeManager, UserProviderInterface $userProvider = null, EncoderFactoryInterface $encoderFactory = null, UserManagerInterface $userManager = null, $container = null)
    {
        parent::__construct($clientManager, $accessTokenManager, $refreshTokenManager, $authCodeManager, $userProvider, $encoderFactory);

        if (!$userManager instanceof UserManagerInterface)
        {
            throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, 'Invalid UserManager', '$userManager is not of type UserManagerInterface.');
        }
        $this->userManager = $userManager;
        $this->container = $container;
        $this->entityManager = $this->container->get("doctrine.orm.entity_manager");
        $this->eventDispatcher = $this->container->get("event_dispatcher");
    }

    public function checkUserCredentials(IOAuth2Client $client, $username, $password)
    {
        if (!$client instanceof ClientInterface)
        {
            throw new InvalidArgumentException('Client has to implement the ClientInterface');
        }

        try
        {
            $user = $this->userProvider->loadUserByFacebookId($username);
        } catch (AuthenticationException $e)
        {
            return false;
        }

        if (null !== $user)
        {
            $encoder = $this->encoderFactory->getEncoder($user);
            return array(
                'data' => $user,
            );
        }

        return false;
    }

    public function checkUserFacebookCredentials(IOAuth2Client $client, $input)
    {
        if (!$client instanceof ClientInterface)
        {
            throw new InvalidArgumentException('Client has to implement the ClientInterface');
        }

        try
        {
            $user = $this->userProvider->loadUserByFacebookId($input["code"]);
                
                /* @var $user BoomUser */
                $user->setCredentialsExpired(false);
//                $user->setEmail($input["email"]);
                if($input["email"]!=null){
                    $user->setEmail($input["email"]);
                }else{
                    $user->setEmail("");
                }
                $user->setEnabled(true);
                $user->setStatus(true);
                $user->setExpired(false);
                $user->setFbId($input["code"]);
                $user->setGender($input["gender"]);
                $user->setFirstname($input["firstname"]);
                $user->setLastname($input["lastname"]);
                $user->setLastLogin(new DateTime());
                $user->setLocked(false);
                $user->setProfileImage("https://graph.facebook.com/".$input["code"]."/picture?width=400");
                $user->setPlainPassword($input["code"]);
                $user->setBirth($input["birthday"]);
                $user->setMaxAge($input["ageRangeMax"]);
                $user->setMinAge($input["ageRangeMin"]);
                $user->setFbAccessToken($input["fbAccessToken"]);
                if($input["email"]!=null){
                    $user->setUsername($input["email"]);
                }else{
                    $user->setUsername("");
                }
                $this->userManager->updateUser($user);


            
        } catch (AuthenticationException $e)
        {
            if ($input["code"] != "")
            {
                
                /* @var $user BoomUser */
                $user = $this->userManager->createUser();
                $user->setCredentialsExpired(false);
//                $user->setEmail($input["email"]);
                if($input["email"]!=null){
                    $user->setEmail($input["email"]);
                }else{
                    $user->setEmail("");
                }
                $user->setEnabled(true);
                $user->setStatus(true);
                $user->setExpired(false);
                $user->setFbId($input["code"]);
                $user->setGender($input["gender"]);
                $user->setFirstname($input["firstname"]);
                $user->setLastname($input["lastname"]);
                $user->setRegistrationDate(new DateTime());
                $user->setLastLogin(new DateTime());
                $user->setLocked(false);
                $user->setProfileImage("https://graph.facebook.com/".$input["code"]."/picture?width=400");
                $user->setPlainPassword($input["code"]);
                $user->setBirth($input["birthday"]);
                $user->setMaxAge($input["ageRangeMax"]);
                $user->setMinAge($input["ageRangeMin"]);
                $user->setFbAccessToken($input["fbAccessToken"]);
                if($input["email"]!=null){
                    $user->setUsername($input["email"]);
                }else{
                    $user->setUsername("");
                }
                $user->addRole("ROLE_API");

                $this->userManager->updateUser($user);
            }
            else
            {
                return false;
            }
        }

        if (null !== $user)
        {
            return array(
                'data' => $user,
            );
        }

        return false;
    }

    public function createAccessToken($tokenString, IOAuth2Client $client, $data, $expires, $scope = null)
    {
        $token = parent::createAccessToken($tokenString, $client, $data, $expires, $scope);
        
        $request = $this->container->get("request_stack")->getCurrentRequest();
        $userAgent = $request->headers->get("useragent");
        
        $analyzedUserAgent = UserAgentReader::analyzeUserAgent($userAgent);
            
        if(!UserAgentReader::checkUserAgentIsValid($analyzedUserAgent))
        {
//            var_dump($analyzedUserAgent);die;
            
        }
        
        $event = new UserLogedInEvent($token->getData(), $userAgent);
        $this->eventDispatcher->dispatch(UserLogedInEvent::NAME, $event);
        
        return $token;
    }
}
