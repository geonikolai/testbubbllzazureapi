<?php

namespace Bubblz\OAuthServerBundle\Exceptions;

use OAuth2\OAuth2ServerException;

class Oauth2TokenNotFoundException extends OAuth2ServerException
{
    public  function __construct($error, $errorDescription = null)
    {
        parent::__construct(503, $error, $errorDescription);
    }
}
