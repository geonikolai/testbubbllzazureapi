<?php

namespace Bubblz\OAuthServerBundle\Controller;

use Bubblz\Common\Helpers\UserAgentReader;
use Bubblz\EntitiesBundle\Entity\BoomUser;
use Bubblz\OAuthServerBundle\Extensions\OAuth2;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Controller\TokenController as BaseTokenController;
use OAuth2\OAuth2ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TokenController extends BaseTokenController
{

    /**
     * @var OAuth2
     */
    protected $server;

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(\OAuth2\OAuth2 $server, ContainerInterface $container)
    {
        parent::__construct($server);
        $this->container = $container;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function tokenAction(Request $request)
    {
        try
        {
            if ($request->request->has('grant_type'))
            {
                if ($request->request->get('grant_type') == "refresh_token")
                {
                    if($request->request->get('access_token') == "")
                    {
                        return new JsonResponse(array('data' => [], 'message' => 'Το refresh_token δεν βρέθηκε.', 'status' => 200));
                    }
                    $response = $this->server->refreshAccessToken($request, $this->container->get("doctrine")->getManager());
                    $customResponse = array(
                        'access_token' => $response["access_token"],
                        'refresh_token' => $response['refresh_token']
                    );
                    return new JsonResponse(array('data' => $customResponse, 'message' => '', 'status' => 200));
                }
            }
            
            $response = $this->server->grantAccessToken($request);

            /* @var $em EntityManager */
            $em = $this->container->get('doctrine')->getManager();

            $request->getSession()->set('bms', $request->request->get("client_secret"));
            /* @var $user BoomUser */
            $user = $em->getRepository(BoomUser::class)->createQueryBuilder('u')
                    ->select('partial u.{id, firstname, username, countBooms, osVersion, deviceModel, manufacturer}')
                    ->where('u.id = :id')
                    ->setParameter('id', $response['user']->getId())
                    ->getQuery()
                    ->getSingleResult();
            
            $customResponse = array(
                'access_token' => $response["access_token"],
                'refresh_token' => $response['refresh_token'],
                'user' => array(
                    'name' => $user->getFirstname() == null || $user->getFirstname() == '' ? $user->getUsername() : $user->getFirstname() . " " . $user->getLastname(),
                    'username' => strpos($user->getUsername(), '@') || $user->getUsername() == null || $user->getUsername() == '' ? $user->getFirstname() . " " . $user->getLastname() : '' ,
                    'email' => $user->getEmail(),
                    'countBooms' => $user->getCountBooms() == null ? 0 : $user->getCountBooms(),
                    'image' => $user->getProfileImage()
            ));
            
            $header = UserAgentReader::analyzeUserAgent($request->headers->get("UserAgent"));
            $user->setOsVersion($header->os_version);
            $user->setDeviceModel($header->model);
            $user->setManufacturer($header->manufacturer);
            $em->flush();

            return new JsonResponse(array('data' => $customResponse, 'message' => '', 'status' => 200));
        } catch (OAuth2ServerException $e)
        {
            return new JsonResponse(array('data' => '', 'message' => $e->getDescription(), 'status' => 400));
        } catch (\Exception $e)
        {
            return new JsonResponse(array('data' => '', 'message' => $e->getMessage(), 'status' => 400));
        }
    }

}
