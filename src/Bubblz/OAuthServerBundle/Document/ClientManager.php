<?php

namespace Bubblz\OAuthServerBundle\Document;

use FOS\OAuthServerBundle\Document\ClientManager as BaseClientManager;

class ClientManager extends BaseClientManager
{
    /**
     * {@inheritdoc}
     */
    public function findClientByPublicId($publicId) 
    {
        return $this->findClientBy(array(
            'randomId' => $publicId,
        ));
    }
}
