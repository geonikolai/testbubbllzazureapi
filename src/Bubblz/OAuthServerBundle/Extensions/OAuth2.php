<?php

namespace Bubblz\OAuthServerBundle\Extensions;

use Facebook\Facebook;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2 as BaseOAuth2;
use OAuth2\OAuth2ServerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OAuth2 extends BaseOAuth2
{
    const GRANT_TYPE_FACEBOOK_ID = 'facebook_id';
    
    /**
     * When using the bearer token type, there is a specifc Authorization header
     * required: "Bearer"
     *
     * @see http://tools.ietf.org/html/draft-ietf-oauth-v2-bearer-04#section-2.1
     *
     * @var string
     */
    const TOKEN_BEARER_HEADER_NAME = '';
    
    /**
     * Regex to filter out the grant type.
     * NB: For extensibility, the grant type can be a URI
     *
     * @see http://tools.ietf.org/html/draft-ietf-oauth-v2-20#section-4.5
     */
    const GRANT_TYPE_REGEXP = '#^(authorization_code|token|password|client_credentials|refresh_token|facebook_id|https?://.+|urn:.+)$#';
    
    /**
     * Get the access token from the header
     *
     * Old Android version bug (at least with version 2.2)
     *
     * @see http://code.google.com/p/android/issues/detail?id=6684
     *
     * @param Request $request
     * @param bool    $removeFromRequest
     *
     * @return string|null
     */
    protected function getBearerTokenFromHeaders(Request $request, $removeFromRequest)
    {
        $header = null;
        if (!$request->headers->has('AUTHORIZATION')) {
            // The Authorization header may not be passed to PHP by Apache;
            // Trying to obtain it through apache_request_headers()
            if (function_exists('apache_request_headers')) {
                $headers = apache_request_headers();

                if (is_array($headers)) {

                    // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                    $headers = array_combine(array_map('ucwords', array_keys($headers)), array_values($headers));

                    if (isset($headers['Authorization'])) {
                        $header = $headers['Authorization'];
                    }
                }
            }
        } else {
            $header = $request->headers->get('AUTHORIZATION');
        }

        if (!$header) {
            return null;
        }


//        if (!preg_match('/' . preg_quote(self::TOKEN_BEARER_HEADER_NAME, '/') . '\s(\S+)/', $header, $matches)) {
//            return null;
//        }

        $token = $header;

        if ($removeFromRequest) {
            $request->headers->remove('AUTHORIZATION');
        }

        return $token;
    }
    
    protected function grantAccessTokenFacebookId(IOAuth2Client $client, array $input)
    {
        if (!$input["code"]) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Missing parameter. "code" is required');
        }
        
        if($input['grant_type'] !== 'facebook_id'){
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Missing parameter. "code" is required');
        }

        if ($this->getVariable(self::CONFIG_ENFORCE_INPUT_REDIRECT) && !$input["redirect_uri"]) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, "The redirect URI parameter is required.");
        }

        $user = $this->storage->checkUserFacebookCredentials($client, $input);
        
        if(!$user)
        {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, 'Invalid user', 'The facebook id given is not valid.');
        }
        return array(
            'scope' => 'user',
            'data' => $user['data'],
        );
    }
    
    public  function refreshAccessToken($request, \Doctrine\ORM\EntityManager $em)
    {
        $filters = array(
            "grant_type" => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => self::GRANT_TYPE_REGEXP),
                "flags" => FILTER_REQUIRE_SCALAR
            ),
            "access_token" => array("flags" => FILTER_REQUIRE_SCALAR),
        );
        $inputData = $request->request->all();
        $input = filter_var_array($inputData, $filters);
        
        if (!$input["grant_type"]) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Invalid grant_type parameter or parameter missing');
        }
        
        $clientCreds = $em->getRepository(\Bubblz\EntitiesBundle\Entity\AccessToken::class)->createQueryBuilder('c')
                ->select("cl.randomId, cl.secret")
                ->leftJoin('c.client', 'cl')
                ->where('c.token = :token')
                ->setParameter('token', $input["access_token"])
                ->getQuery()
                ->getSingleResult();
        
        $clientCredentials = array($clientCreds["randomId"], $clientCreds["secret"]);

        $client = $this->storage->getClient($clientCredentials[0]);
        
        if (!$client) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
        }
        
        $t = $this->storage->getAccessToken($input["access_token"]);
        
        $tokenLifeTime = $this->getVariable(self::CONFIG_ACCESS_LIFETIME);
        $issueRefreshToken = true;
        $refreshTokenLifetime = $this->getVariable(self::CONFIG_REFRESH_LIFETIME);
        
        $token = $this->createAccessToken($client, $t->getData(), 'user', $tokenLifeTime, $issueRefreshToken, $refreshTokenLifetime);
        return $token;
    }


    /**
     * Grant or deny a requested access token.
     *
     * This would be called from the "/token" endpoint as defined in the spec.
     * Obviously, you can call your endpoint whatever you want.
     * Draft specifies that the authorization parameters should be retrieved from POST, but you can override to whatever method you like.
     *
     * @param  Request $request (optional) The request
     *
     * @return Response
     * @throws OAuth2ServerException
     *
     * @see      http://tools.ietf.org/html/draft-ietf-oauth-v2-20#section-4
     * @see      http://tools.ietf.org/html/draft-ietf-oauth-v2-21#section-10.6
     * @see      http://tools.ietf.org/html/draft-ietf-oauth-v2-21#section-4.1.3
     *
     * @ingroup  oauth2_section_4
     */
    public function grantAccessToken(Request $request = null)
    {
        $facebookUser = $this->getFacebookUserDetails(\Bubblz\OAuthServerBundle\Common\Globals::$facebookAppId, \Bubblz\OAuthServerBundle\Common\Globals::$facebookAppSecret, $request->request->get('fb_access_token'), $request->request->get('code'));
        $fbAccessToken = $request->request->get('fb_access_token');
        
        $filters = array(
            "grant_type" => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => self::GRANT_TYPE_REGEXP),
                "flags" => FILTER_REQUIRE_SCALAR
            ),
            "scope" => array("flags" => FILTER_REQUIRE_SCALAR),
            "code" => array("flags" => FILTER_REQUIRE_SCALAR),
            "redirect_uri" => array("filter" => FILTER_SANITIZE_URL),
            "username" => array("flags" => FILTER_REQUIRE_SCALAR),
            "password" => array("flags" => FILTER_REQUIRE_SCALAR),
            "refresh_token" => array("flags" => FILTER_REQUIRE_SCALAR),
            "email" => array("flags" => FILTER_REQUIRE_SCALAR),
            "gender" => array("flags" => FILTER_REQUIRE_SCALAR),
            "name" => array("flags" => FILTER_REQUIRE_SCALAR),
            "ageRangeMin" => array("flags" => FILTER_REQUIRE_SCALAR),
            "ageRangeMax" => array("flags" => FILTER_REQUIRE_SCALAR),
            "fbAccessToken" => array("flags" => FILTER_REQUIRE_SCALAR),

        );

        if ($request === null) {
            $request = Request::createFromGlobals();
        }

        // Input data by default can be either POST or GET
        if ($request->getMethod() === 'POST') {
            $inputData = $request->request->all();
        } else {
            $inputData = $request->query->all();
        }

        // Basic authorization header
        $authHeaders = $this->getAuthorizationHeader($request);

        // Filter input data
        $input = filter_var_array($inputData, $filters);

        $input['email'] = $facebookUser->getEmail();
        $input['username'] = $facebookUser->getEmail();
        $input['gender'] = $facebookUser->getGender();
        $input['name'] = $facebookUser->getName();
        $input['firstname'] = $facebookUser->getFirstName();
        $input['lastname'] = $facebookUser->getLastName();
        $input['hometown'] = $facebookUser->getHometown();
        $input['birthday'] = $facebookUser->getBirthday();
        $input['picture'] = $facebookUser->getPicture()->getUrl();
        $input['ageRangeMin'] = $facebookUser->getField('age_range')->getField('min');
        $input['ageRangeMax'] = $facebookUser->getField('age_range')->getField('max');
        $input['fbAccessToken'] = $fbAccessToken;
        
        // Grant Type must be specified.
        if (!$input["grant_type"]) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Invalid grant_type parameter or parameter missing');
        }

        // Authorize the client
        $clientCredentials = $this->getClientCredentials($inputData, $authHeaders);

        $client = $this->storage->getClient($clientCredentials[0]);

        if (!$client) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
        }

        if ($this->storage->checkClientCredentials($client, $clientCredentials[1]) === false) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
        }

        if (!$this->storage->checkRestrictedGrantType($client, $input["grant_type"])) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_UNAUTHORIZED_CLIENT, 'The grant type is unauthorized for this client_id');
        }

        // Do the granting
        switch ($input["grant_type"]) {
            case self::GRANT_TYPE_FACEBOOK_ID:
                // returns array('data' => data, 'scope' => scope)
                $stored = $this->grantAccessTokenFacebookId($client, $input);
                break;
            case self::GRANT_TYPE_AUTH_CODE:
                // returns array('data' => data, 'scope' => scope)
                $stored = $this->grantAccessTokenAuthCode($client, $input);
                break;
            case self::GRANT_TYPE_USER_CREDENTIALS:
                // returns: true || array('scope' => scope)
                $stored = $this->grantAccessTokenUserCredentials($client, $input);
                break;
            case self::GRANT_TYPE_CLIENT_CREDENTIALS:
                // returns: true || array('scope' => scope)
                $stored = $this->grantAccessTokenClientCredentials($client, $input, $clientCredentials);
                break;
            case self::GRANT_TYPE_REFRESH_TOKEN:
                // returns array('data' => data, 'scope' => scope)
                $stored = $this->grantAccessTokenRefreshToken($client, $input);
                break;
            default:
                if (substr($input["grant_type"], 0, 4) !== 'urn:'
                    && !filter_var($input["grant_type"], FILTER_VALIDATE_URL)
                ) {
                    throw new OAuth2ServerException(
                        self::HTTP_BAD_REQUEST,
                        self::ERROR_INVALID_REQUEST,
                        'Invalid grant_type parameter or parameter missing'
                    );
                }

                // returns: true || array('scope' => scope)
                $stored = $this->grantAccessTokenExtension($client, $inputData, $authHeaders);
        }

        if (!is_array($stored)) {
            $stored = array();
        }

        // if no scope provided to check against $input['scope'] then application defaults are set
        // if no data is provided than null is set
        $stored += array('scope' => $this->getVariable(self::CONFIG_SUPPORTED_SCOPES, null), 'data' => null,
                         'access_token_lifetime' => $this->getVariable(self::CONFIG_ACCESS_LIFETIME),
                         'issue_refresh_token' => true, 'refresh_token_lifetime' => $this->getVariable(self::CONFIG_REFRESH_LIFETIME));

        $scope = $stored['scope'];
        if ($input["scope"]) {
            // Check scope, if provided
            if (!isset($stored["scope"]) || !$this->checkScope($input["scope"], $stored["scope"])) {
                throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_SCOPE, 'An unsupported scope was requested.');
            }
            $scope = $input["scope"];
        }

        $token = $this->createAccessToken($client, $stored['data'], $scope, $stored['access_token_lifetime'], $stored['issue_refresh_token'], $stored['refresh_token_lifetime']);
        
        $token["user"] = $stored['data'];
        return $token;
    }
    
    private function getFacebookUserDetails($appId, $appSecret, $fbAccessToken, $currentFacebookId) {
        
        $fb = new Facebook([
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.7',
        ]);

        $fb->setDefaultAccessToken($fbAccessToken);

        $helper = $fb->get('/me?fields=id,name, email,picture, age_range, gender, link, first_name, last_name, hometown, birthday');

        $userNode = $helper->getGraphUser();
        
        if($currentFacebookId !== $userNode->getId())
        {
            throw new OAuth2ServerException(400, 'invalid_user', 'Facebook id does not match with user credentials from facebook.');
        }

        return $userNode;
    }
}
