<?php
namespace Bubblz\OAuthServerBundle\Events;

use Bubblz\EntitiesBundle\Entity\BoomUser;
use Symfony\Component\EventDispatcher\Event;

class UserLogedInEvent extends Event {
    const NAME = 'user.loged_id';

    /**
     *
     * @var BoomUser
     */
    protected $user;
    
    /**
     *
     * @var string
     */
    protected $userAgent;

    public function __construct(BoomUser $user, $userAgent = null)
    {
        $this->user = $user;
        $this->userAgent = $userAgent;
    }

    /**
     * 
     * @return BoomUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @return stirng
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }
}
