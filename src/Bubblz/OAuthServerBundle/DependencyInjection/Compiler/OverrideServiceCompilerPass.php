<?php

namespace Bubblz\OAuthServerBundle\DependencyInjection\Compiler;

use Bubblz\UserBundle\Security\UserProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        /* @var $definition Symfony/Component/DependencyInjection/Definition */
//        $definition = $container->getDefinition('fos_oauth_server.storage.default');
//        $definition->setClass('Bubblz\OAuthServerBundle\Storage\OAuthStorage');
        
        /* @var $clientManagerDefinition Symfony/Component/DependencyInjection/Definition */
        $clientManagerDefinition = $container->getDefinition('fos_oauth_server.client_manager.default');
        $clientManagerDefinition->setClass('Bubblz\OAuthServerBundle\Entity\ClientManager');
        
        /* @var $oauthServerDefinition Symfony/Component/DependencyInjection/Definition */
//        $oauthServerDefinition = $container->getDefinition('fos_oauth_server.server');
//        $oauthServerDefinition->setClass('Bubblz\OAuthServerBundle\Extensions\OAuth2');
        
        /* @var $oauthServerDefinition Symfony/Component/DependencyInjection/Definition */
        $oauthServerDefinition = $container->getDefinition('fos_oauth_server.security.authentication.listener');
        $oauthServerDefinition->setClass('Bubblz\OAuthServerBundle\Security\Firewall\OAuthListener');
        
        /* @var $oauthTokenControllerDefinition Symfony/Component/DependencyInjection/Definition */
//        $oauthTokenControllerDefinition = $container->getDefinition('fos_oauth_server.controller.token');
//        $oauthTokenControllerDefinition->setClass('Bubblz\OAuthServerBundle\Controller\TokenController');
    }
}