<?php
namespace Bubblz\OAuthServerBundle\Entity;

class FacebookData
{
    public $facebookId;
    public $username;
    public $email;
    public $gender;
    public $name;
    public $ageRangeMin;
    public $ageRangeMax;
}
