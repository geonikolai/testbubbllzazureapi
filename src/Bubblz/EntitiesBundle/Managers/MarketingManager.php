<?php

namespace Bubblz\EntitiesBundle\Managers;

use Bubblz\EntitiesBundle\Entity\Contest;
use DateTime;


class MarketingManager extends BaseManager
{

    public function GetAllMarketing()
    {
        $connection = $this->entityManager->getConnection();
        $sql = " 
                select 
                    id,
                    store_id,
                    'discount' as marketing_type,
                    CONVERT(coalesce(title, '') USING utf8) as title,
                    COALESCE(description, '') as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Έκπτωση' USING utf8) as onoma_koumpiou,
                    remote,
                    unattended,
                    '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    value as offer_value,
                    steady_value
                from discount 
                union all
                select 
                    id,
                    store_id,
                    'contest' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Διαγωνισμός' USING utf8) as onoma_koumpiou,
                    remote,
                    unattended,
                    '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value
                from contest 
                union all
                select 
                    id,
                    store_id,
                    'offer' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Προσφορά' USING utf8) as onoma_koumpiou,
                    remote,
                    unattended,
                    '' as eikona,
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value
                from offer 
            ";

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $marketings = $stmt->fetchAll();

        $response = [];
        foreach ($marketings as $val)
        {
            $model = new MarketingModel();
            $model->id = intval($val["id"]);
            $model->storeId = intval($val["store_id"]);
            $model->marketingType = $val["marketing_type"];
            $model->title = $val["title"];
            $model->description = $val["description"];
            $model->beforeCheckinMessage = $val["before_checkin_message"];
            $model->afterCheckinMessage = $val["after_checkin_message"];
            $model->onomaKoumpiou = $val["onoma_koumpiou"];
            $model->eikona = $val["eikona"];
            $model->endDate = $val["end_date"];
            $model->status = intval($val["status"]);
            $model->unattended = intval($val["unattended"]);
            $model->remote = intval($val["remote"]);
            $model->offerValue = $val["offer_value"];
            $model->steadyValue = intval($val["steady_value"]);
            
            $response[] = $model;
        }

        return $response;
    }
    
    public function GetMarketingAfterDatetime($datetime)
    {
        
        $datetimeString = $this->getDatetimeFromStringFormat($datetime);
        $connection = $this->entityManager->getConnection();
        $sql = " select * from (
                select 
                    id,
                    store_id,
                    'discount' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    COALESCE(description, '') as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Έκπτωση' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona, 
                    remote,
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status, 
                    value as offer_value,
                    steady_value,
                    last_update
                from discount 
                union all
                select 
                    id,
                    store_id,
                    'contest' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Διαγωνισμός' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona, 
                    remote,
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value,
                    last_update
                from contest ".
//                union all
//                select 
//                    id,store_id,'coupon' as marketing_type,'' as title,'' as description,'' as before_checkin_message,'' as after_checkin_message,'Κουπόνι' as onoma_koumpiou,0 as unattended, '' as eikona, COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H'), '') as end_date, (if( end_date >= now() ,0, 1)) as status, last_update 
//                from coupon 
                
                "union all
                select 
                    id,
                    store_id,
                    'offer' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Προσφορά' USING utf8) as onoma_koumpiou,
                    remote,
                    unattended,
                    '' as eikona,
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value,
                    last_update 
                    from offer ) a where last_update > '".$datetimeString." 00:00:00' and last_update is not null
            ";

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $marketings = $stmt->fetchAll();

        $response = [];
        foreach ($marketings as $val)
        {
            $model = new MarketingModel();
            $model->id = intval($val["id"]);
            $model->storeId = intval($val["store_id"]);
            $model->marketingType = $val["marketing_type"];
            $model->title = $val["title"];
            $model->description = $val["description"];
            $model->beforeCheckinMessage = $val["before_checkin_message"];
            $model->afterCheckinMessage = $val["after_checkin_message"];
            $model->onomaKoumpiou = $val["onoma_koumpiou"];
            $model->eikona = '';
            $model->endDate = $val["end_date"];
            $model->lastUpdate = $val["last_update"];
            $model->offerValue = $val["offer_value"];
            $model->steadyValue = intval($val["steady_value"]);
            $model->status = intval($val["status"]);
            $model->remote = intval($val["remote"]);
            $model->unattended = intval($val["unattended"]);
            
            $response[] = $model;
        }

        return $response;
    }
    
    public function GetMarketingByStore($storeId)
    {
        $connection = $this->entityManager->getConnection();
        $sql = " select * from (
                select 
                    id,
                    store_id,
                    'discount' as marketing_type,
                    CONVERT('' USING utf8) as title,
                    '' as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Έκπτωση' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status, 
                    value as offer_value,
                    steady_value,
                    last_update
                from discount 
                union all
                select 
                    id,
                    store_id,
                    'contest' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Διαγωνισμός' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value,
                    last_update
                from contest ".
//                union all
//                select 
//                    id,store_id,'coupon' as marketing_type,'' as title,'' as description,'' as before_checkin_message,'' as after_checkin_message,'Κουπόνι' as onoma_koumpiou,0 as unattended, '' as eikona, COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H'), '') as end_date, (if( end_date >= now() ,0, 1)) as status, last_update 
//                from coupon 
                
                "union all
                select 
                    id,
                    store_id,
                    'offer' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Προσφορά' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value,
                    last_update 
                from offer ) a where store_id = ".$storeId."
            ";

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $marketings = $stmt->fetchAll();

        $response = [];
        foreach ($marketings as $val)
        {
            $model = new MarketingModel();
            $model->id = intval($val["id"]);
            $model->storeId = intval($val["store_id"]);
            $model->marketingType = $val["marketing_type"];
            $model->title = $val["title"];
            $model->description = $val["description"];
            $model->beforeCheckinMessage = $val["before_checkin_message"];
            $model->afterCheckinMessage = $val["after_checkin_message"];
            $model->onomaKoumpiou = $val["onoma_koumpiou"];
            $model->eikona = $val["eikona"];
            $model->endDate = $val["end_date"];
            $model->lastUpdate = $val["last_update"];
            $model->offerValue = $val["offer_value"];
            $model->steadyValue = intval($val["steady_value"]);
            $model->status = intval($val["status"]);
            
            $response[] = $model;
        }

        return $response;
    }
    
    public function GetMarketingByGeoRadius($lat, $long, $radius)
    {
        $connection = $this->entityManager->getConnection();
        $sql = " select * from (
                select 
                    a.id,
                    store_id,
                    'discount' as marketing_type,
                    CONVERT('' USING utf8) as title,
                    '' as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Έκπτωση' USING utf8) as onoma_koumpiou,
                    unattended, '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    value as offer_value,
                    steady_value,
                    a.last_update,
                    latitude,
                    longitute
                from discount a
                inner join store b on a.store_id=b.id
                union all
                select 
                    a.id,
                    store_id,
                    'contest' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Διαγωνισμός' USING utf8) as onoma_koumpiou,
                    unattended, '' as eikona, 
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status, 
                    0 as offer_value,
                    -1 as steady_value,
                    a.last_update,
                    latitude,
                    longitute
                from contest a
                inner join store b on a.store_id=b.id ".
//                union all
//                select 
//                    id,
//                    store_id,'coupon' as marketing_type,'' as title,'' as description,'' as before_checkin_message,'' as after_checkin_message,'Κουπόνι' as onoma_koumpiou,0 as unattended, '' as eikona, COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H'), '') as end_date, (if( end_date >= now() ,0, 1)) as status, last_update 
//                from coupon 
                
                "union all
                select 
                    a.id,
                    store_id,
                    'offer' as marketing_type,
                    CONVERT(title USING utf8) as title,
                    CONVERT(description USING utf8) as description,
                    COALESCE(before_checkin_message, '') as before_checkin_message,
                    COALESCE(after_checkin_message, '') as after_checkin_message,
                    CONVERT('Προσφορά' USING utf8) as onoma_koumpiou,
                    unattended,
                    '' as eikona,
                    COALESCE(DATE_FORMAT(end_date, '%d/%m/%Y %H:%i:%s'), '') as end_date,
                    (if (end_date >= now(), 0, 1)) as status,
                    0 as offer_value,
                    -1 as steady_value,
                    a.last_update,
                    latitude,
                    longitute
                from offer a
                inner join store b on a.store_id=b.id ) a 
                where abs(latitude-".$radius.")<".$lat." and abs(latitude+".$radius.")>".$lat." and abs(longitute-".$radius."2)<".$long." and abs(longitute+".$radius.")>".$long."
            ";

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $marketings = $stmt->fetchAll();

        $response = [];
        foreach ($marketings as $val)
        {
            $model = new MarketingModel();
            $model->id = intval($val["id"]);
            $model->storeId = intval($val["store_id"]);
            $model->marketingType = $val["marketing_type"];
            $model->title = $val["title"];
            $model->description = $val["description"];
            $model->beforeCheckinMessage = $val["before_checkin_message"];
            $model->afterCheckinMessage = $val["after_checkin_message"];
            $model->onomaKoumpiou = $val["onoma_koumpiou"];
            $model->eikona = $val["eikona"];
            $model->endDate = $val["end_date"];
            $model->lastUpdate = $val["last_update"];
            $model->offerValue = $val["offer_value"];
            $model->steadyValue = intval($val["steady_value"]);
            $model->status = intval($val["status"]);
            
            $response[] = $model;
        }

        return $response;
    }
    
    private function getDatetimeFromStringFormat($datetime)
    {
        $date = substr($datetime, 0, 2);
        $month = substr($datetime, 2, 2);
        $year = substr($datetime, 4, 4);
        
        $dateOb = new DateTime($year ."-" . $month . "-" . $date);
        
        return $dateOb->format("Y-m-d");
    }
    
    public function CheckIsUnattended($offerId, $offerType)
    {
        $repository = "";
        switch ($offerType)
        {
            case "contest":
                $repository = "BubblzEntitiesBundle:Contest";
                break;
            case "offer":
                $repository = "BubblzEntitiesBundle:Offer";
                break;
            case "discount":
                $repository = "BubblzEntitiesBundle:Discount";
                break;
        }
        
        $offer = $this->entityManager->getRepository($repository)->createQueryBuilder('r')
                ->select("partial r.{id, unattended}")
                ->where('r.id = :id')
                ->setParameter('id', $offerId)
                ->getQuery()
                ->getSingleResult();
        
        return $offer->getUnattended();
    }
    
    public function AddContestEntry($offerId)
    {
        $contest = $this->entityManager->getRepository(Contest::class)->createQueryBuilder('c')
                ->select("partial c.{id, entries}")
                ->where("c.id = :id")
                ->setParameter('id', $offerId)
                ->getQuery()
                ->getSingleResult();
        
        $contest->setEntries($contest->getEntries() + 1);
        
        $this->entityManager->flush($contest);
    }
           
    public function jsonData(){

        $connection = $this->entityManager->getConnection();
        $sql = "select * from contest_push where store_id not in (65)";
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $notifications = $stmt->fetchAll();
        $response = [];
        
        foreach ($notifications as $val)
        {
            $model = [];
            $model["store_id"] = intval($val["store_id"]);
            $model["contest_id"] = intval($val["contest_id"]);
            $model["image"] = $val["image"];
            $model["store_title"] = $val["store_title"];
            $model["store_subtitle"] = $val["store_subtitle"];
            $model["store_type"] = $val["store_type"];
            $model["store_icon"] = $val["store_icon"];
            $model["offer_title"] = $val["offer_title"];
            $model["offer_subtitle"] = $val["offer_subtitle"];
            $model["offer_description"] = $val["offer_description"];
            $model["offer_type"] = $val["offer_type"];
            $model["offer_end_date"] = $val["offer_end_date"];
            $model["winner_name"] = $val["winner_name"];
            $model["winner_image"] = $val["winner_image"];
            $model["offer_title_winner"] = $val["offer_title_winner"];
            $model["offer_subtitle_winner"] = $val["offer_subtitle_winner"];
            $model["ticket_number"] = $val["ticket_number"];
            $model["bubbllz_id"] = $val["bubbllz_id"];

            $response[] = $model;
        }
                
        return $response;
    }

}
class MarketingModel
{
    public $id;
    
    /**
     *
     * @var integer
     */
    public $storeId;
    
    /**
     *
     * @var string
     */
    public $endDate;
    
    /**
     *
     * @var string
     */
    public $lastUpdate;
    
    /**
     *
     * @var stirng
     */
    public $marketingType;
    
    /**
     *
     * @var stirng
     */
    public $title;
    
    /**
     *
     * @var stirng
     */
    public $description;
    
    /**
     *
     * @var stirng
     */
    public $beforeCheckinMessage;
    
    /**
     *
     * @var stirng
     */
    public $afterCheckinMessage;
    
    /**
     *
     * @var stirng
     */
    public  $onomaKoumpiou;
    
    /**
     *
     * @var stirng
     */
    public $eikona;
    
    /**
     *
     * @var integer
     */
    public $status;
    
    /**
     *
     * @var integer
     */
    public $remote;
    
    /**
     *
     * @var integer
     */
    public $unattended;
    
    /**
     *
     * @var integer
     */
    public $offerValue;
    
    /**
     *
     * @var integer
     */
    public $steadyValue;
}