<?php

namespace Bubblz\EntitiesBundle\Managers;

use Doctrine\ORM\EntityManager;

/**
 * Description of BaseManager
 *
 * @author ggero
 */
abstract class BaseManager {
    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
}
