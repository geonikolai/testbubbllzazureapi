<?php

namespace Bubblz\EntitiesBundle\Managers;

use Bubblz\ApiBundle\Models\ApiResponse;
use Bubblz\EntitiesBundle\Entity\BoomUser;
use Bubblz\EntitiesBundle\Entity\Brand;
use Bubblz\EntitiesBundle\Entity\Checkin;
use Bubblz\EntitiesBundle\Entity\Contest;
use Bubblz\EntitiesBundle\Entity\Discount;
use Bubblz\EntitiesBundle\Entity\Offer;
use Bubblz\EntitiesBundle\Entity\Store;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\NoResultException;
use Exception;

class CheckinManager extends BaseManager
{

    /**
     * 
     * @param integer $storeId
     * @param integer $userId
     * @return Checkin
     */
    public function GetActiveCheckins($storeId, $userId)
    {
        try
        {
            /* @var $entity Checkin */
            $entity = $this->entityManager->getRepository(Checkin::class)->createQueryBuilder('c')
                    ->select('partial c.{id, status}')
                    ->where('c.boomUser = :userid')
                    ->andWhere('c.status = 1')
                    ->setParameter('userid', $userId)
                    ->getQuery()
                    ->getResult();
        } catch (NoResultException $ex)
        {
            return null;
        }

        return $entity;
    }

    /**
     * 
     * @param integer $storeId
     * @param integer $userId
     * @return Checkin
     */
    public function GetCheckinIsBubbllByUserAndStore($storeId, $userId)
    {
        try
        {
            /* @var $checkin Checkin */
            $entity = $this->entityManager->getRepository(Checkin::class)->createQueryBuilder('c')
                    ->select('partial c.{id,isBubbll}')
                    ->where('c.boomUser = :user')
                    ->andWhere('c.store = :store')
                    ->andWhere('c.status = :status')
                    ->setParameter('user', $userId)
                    ->setParameter('store', $storeId)
                    ->setParameter('status', true)
                    ->getQuery()
                    ->getSingleResult();
        } catch (NoResultException $ex)
        {
            return null;
        }

        return $entity;
    }

//    /**
//     * 
//     * @param BoomUser $user
//     * @param integer $storeId
//     * @return Checkin
//     */
//    public function CreateNewCheckin($user, $storeId, $offerId = -1, $offerType = "")
//    {
//        $checkin = new Checkin();
//        $checkin->setBoomUser($user);
//        $checkin->setCheckinTime(new DateTime("now", new DateTimeZone("Europe/Athens")));
//        
//        if ($offerId != -1 && $offerType != "")
//        {
//            switch ($offerType)
//            {
//                case "contest":
//                    /* @var $marketingPlan Contest */
////                    $marketingPlan = $this->entityManager->getRepository(Contest::class)->find($offerId);
////                    if ($marketingPlan != null)
////                    {
////                        if ($marketingPlan->getBrandContest() != null)
////                        {
////                            $brand = $this->entityManager->getRepository(Brand::class)->find($marketingPlan->getBrandContest()->getBrand()->getId());
////
////                            if ($brand != null)
////                            {
////                                $checkin->setBrand($brand);
////                            }
////                        }
////                    }
//                    break;
//                case "offer":
//                case "discount":
//                    /* @var $discount Discount */
//                    $discount = $this->entityManager->getRepository(Discount::class)->createQueryBuilder('c')
//                        ->select('c.endDate')
//                        ->where('c.id = :id')
//                        ->setParameter('id', $offerId)
//                        ->getQuery()->getArrayResult();
//                    //var_dump( $discount[0]["endDate"]);die;
//                    $date = new \DateTime($discount[0]["endDate"]->format("Y/m/d h:m:i"), new \DateTimeZone("Europe/Athens"));
//                    $now = new \DateTime('now', new \DateTimeZone("Europe/Athens"));
//                    if($now <$date){echo "dsd";die;}
//                    var_dump($date);die;
//                    break;
//                case "loyalty":
//                    break;
//            }
//        }
//        $checkin->setIsBubbll(false);
//        $checkin->setStatus(true);
//        $checkin->setStore($this->entityManager->getReference(Store::class, $storeId));
//
//        return $checkin;
//    }

    /**
     * 
     * @param BoomUser $user
     * @param integer $storeId
     * @return Checkin
     */
    public function CreateNewCheckin($user, $storeId, $offerId, $offerType)
    {

        $now = new DateTime("now", new DateTimeZone("Europe/Athens"));
        switch ($offerType)
        {
            case "discount":
                /* @var $discount Discount */
                $discount = $this->entityManager->getRepository(Discount::class)->createQueryBuilder('d')
                                ->select('d.endDate')
                                ->where('d.id = :id')
                                ->setParameter('id', $offerId)
                                ->getQuery()->getArrayResult();

                $date = new DateTime($discount[0]["endDate"]->format("Y/m/d h:i:s"), new DateTimeZone("Europe/Athens"));

                if ($date > $now)
                {
                    $checkin = new Checkin();
                    $checkin->setBoomUser($user);
                    $checkin->setCheckinTime($now);
                    $checkin->setIsBubbll(false);
                    $checkin->setStatus(true);
                    $checkin->setStore($this->entityManager->getReference(Store::class, $storeId));
                }
                break;
            case "contest":
                /* @var $contest Contest */
                $contest = $this->entityManager->getRepository(Contest::class)->createQueryBuilder('c')
                                ->select('c.endDate, bc.id')
                                ->leftJoin('c.brandContest', 'bc')
                                ->where('c.id = :id')
                                ->setParameter('id', $offerId)
                                ->getQuery()->getResult();

                $date = new DateTime($contest[0]["endDate"]->format("Y/m/d h:i:s"), new DateTimeZone("Europe/Athens"));
                if ($date > $now)
                {
                    $checkin = new Checkin();
                    $checkin->setBoomUser($user);
                    $checkin->setCheckinTime($now);
                    $checkin->setIsBubbll(false);
                    $checkin->setStatus(true);
                    $checkin->setStore($this->entityManager->getReference(Store::class, $storeId));

                    if ($contest[0]["id"] != null)
                    {
                        /* @var $brand Brand */
                        $brand = $this->entityManager->getRepository(Brand::class)->createQueryBuilder('b')
                                        ->select('b.id')
                                        ->where('b.id = :id')
                                        ->setParameter('id', $contest[0]["id"])
                                        ->getQuery()->getResult();

                        if ($brand != null)
                        {
                            $checkin->setBrand($brand);
                        }
                    }
                }
                break;
            case "offer":
                /* @var $offer Offer */
                $offer = $this->entityManager->getRepository(Offer::class)->createQueryBuilder('o')
                                ->select('o.endDate')
                                ->where('o.id = :id')
                                ->setParameter('id', $offerId)
                                ->getQuery()->getArrayResult();

                $date = new DateTime($offer[0]["endDate"]->format("Y/m/d h:i:s"), new DateTimeZone("Europe/Athens"));
                if ($date > $now)
                {
                    $checkin = new Checkin();
                    $checkin->setBoomUser($user);
                    $checkin->setCheckinTime($now);
                    $checkin->setIsBubbll(false);
                    $checkin->setStatus(true);
                    $checkin->setStore($this->entityManager->getReference(Store::class, $storeId));
                }
                break;
        }
        if (!isset($checkin))
        {
            throw new Exception("Η προσφορά έχει λήξει");
        }
        return $checkin;
    }

}
