<?php
namespace Bubblz\EntitiesBundle\Managers;

use Doctrine\ORM\EntityManager;

/**
 * Description of UserLoginHistoryManager
 *
 * @author ggero
 */
class UserLoginHistoryManager 
{
    /**
     * @var EntityManager
     */
    protected $entityManager;
    
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    /**
     * 
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @return \Bubblz\EntitiesBundle\Entity\BoomUserLoginHistory
     */
    public function CreateUserLoginHistory(\FOS\UserBundle\Model\UserInterface $user)
    {
        $entity = new \Bubblz\EntitiesBundle\Entity\BoomUserLoginHistory();
        $entity->setLoginDate(new \DateTime());
        $entity->setUser($user);
        
        return $entity;
    }
    
    public function UpdateUserLoginHistory(\Bubblz\EntitiesBundle\Entity\BoomUserLoginHistory $userHistory)
    {
        if($userHistory->getId() == null)
        {
            $this->entityManager->persist($userHistory);
        }
        
        $this->entityManager->flush($userHistory);
    }
}
