<?php

namespace Bubblz\EntitiesBundle\Managers;

use Bubblz\EntitiesBundle\Entity\Boom;
use Bubblz\EntitiesBundle\Entity\BoomUser;
use Bubblz\EntitiesBundle\Entity\Checkin;
use Bubblz\EntitiesBundle\Entity\Contest;
use Bubblz\EntitiesBundle\Entity\Discount;
use Bubblz\EntitiesBundle\Entity\Offer;
use Bubblz\EntitiesBundle\Entity\Store;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\NoResultException;

class BubbllzManager extends BaseManager
{

    public function GetPendingBubbllz($userId)
    {
        $entities = $this->entityManager->getRepository(Boom::class)->createQueryBuilder('c')
                ->select('partial c.{id, status}')
                ->where('c.boomUser = :userid')
                ->andWhere('c.status = 0')
                ->setParameter('userid', $userId)
                ->getQuery()
                ->getResult();

                return count($entities) > 0 ? true : false;

    }

    public function HasPendingBubbllz($storeId, $userId)
    {
        $entities = $this->entityManager->getRepository(Boom::class)->createQueryBuilder('c')
                ->select('partial c.{id, status}')
                ->where('c.store = :storeid')
                ->andWhere('c.boomUser = :userid')
                ->andWhere('c.status = 0')
                ->setParameter('storeid', $storeId)
                ->setParameter('userid', $userId)
                ->getQuery()
                ->getResult();

        return count($entities) > 0 ? true : false;
    }

    public function CancelBubbllz($storeId, $userId)
    {
        try
        {
            /* @var $entity Boom */
            $entity = $this->entityManager->getRepository(Boom::class)->createQueryBuilder('c')
                    ->select('partial c.{id, status}')
                    ->where('c.boomUser = :userid')
                    //->andWhere('c.store = :storeid')
                    ->andWhere('c.status = 0')
                    //->setParameter('storeid', $storeId)
                    ->setParameter('userid', $userId)
                    ->getQuery()
                    ->getResult();

            foreach ($entity as &$val)
            {
                $val->setStatus(4);
                $this->entityManager->flush($val);
            }
        } catch (NoResultException $ex)
        {
            
        }
    }

    public function CreateNewBubbll($storeId, $checkinId, $userId, $message, $offerId = -1, $offerType = "")
    {
        if ($this->HasPendingBubbllz($storeId, $userId))
        {
            throw new \Exception("The user has a pending bubbll on store.");
        }

        $boom = new Boom();
        $boom->setStore($this->entityManager->getReference(Store::class, $storeId));
        $boom->setCheckin($this->entityManager->getReference(Checkin::class, $checkinId));
        $boom->setBoomUser($this->entityManager->getReference(BoomUser::class, $userId));
        $boom->setBoomCategoryId(0);
        $boom->setFbLikes(0);
        $boom->setFbLove(0);
        $boom->setFbHaha(0);
        $boom->setFbWow(0);
        $boom->setFbSad(0);
        $boom->setFbAngry(0);

        if ($offerId != -1)
        {
            switch ($offerType)
            {
                case "discount":
                    $boom->setDiscount($this->entityManager->getReference(Discount::class, $offerId));
                    break;
                case "contest":
                    /* @var $marketingPlan Contest */
                    $marketingPlan = $this->entityManager->getRepository(Contest::class)->find($offerId);
                    if($marketingPlan->getBrandContest() != null)
                    {
                        $boom->setBrand($marketingPlan->getBrandContest()->getBrand());
                    }
                    $boom->setContest($this->entityManager->getReference(Contest::class, $offerId));
                    break;
                case "offer":
                    $boom->setOffer($this->entityManager->getReference(Offer::class, $offerId));
                    break;
            }
        }
        $boom->setImageName("no-image");
        $boom->setBoomUserMessage($message);
        $boom->setTicket(rand(1000, 9999));
        $boom->setCDate(new DateTime('now', new DateTimeZone('Europe/Athens')));
        $boom->setStatus(0);

        return $boom;
    }

}
