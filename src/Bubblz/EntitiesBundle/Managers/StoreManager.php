<?php

namespace Bubblz\EntitiesBundle\Managers;

use Bubblz\Common\Helpers\UserAgentReader;
use Bubblz\EntitiesBundle\Entity\KernelUser;
use DateTime;
use Symfony\Component\HttpFoundation\Request;


class StoreManager extends BaseManager
{

    public function GetStoresForApi(Request $request)
    {
 
        $header = UserAgentReader::analyzeUserAgent($request->headers->get("UserAgent"));
        $platform = $header->platform;
        $model = $header->model;
        $accessToken = $_SERVER['HTTP_AUTHORIZATION'];
        
        $connection = $this->entityManager->getConnection();
        $sql ="select at.user_id from access_token at where at.token = '$accessToken'";
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $user = $stmt->fetch();
        $userId = $user["user_id"];

        
//            from store s
//                inner join user_admin ad on ad.store_id=s.id
//                left join city c on c.id=s.city_id
//                left join category cat on cat.id=s.category_of 
//                where s.main_image is not null and s.status=1  

        $connection = $this->entityManager->getConnection();
        $sql = " 
                select 
                    s.id ,
                    s.longitute as longitude, 
                    s.latitude, 
                    COALESCE(s.storename, '') as storename, 
                    COALESCE(ad.telephone, '') as telephone, 
                    s.type_store,
                    c.id as cityId, 
                    s.city_title, 
                    COALESCE(s.slogan, '') as slogan, 
                    COALESCE(s.keywords, '') as keywords,
                    COALESCE(s.perma_tag, '') as hashtags,
                    CONCAT(s.address, ' ',  s.address2) as address, 
                    CONCAT('http://dev.bubbllz.com/images/stores/', s.main_image) as mainImage, 
                    CONCAT('http://dev.bubbllz.com/images/stores/logos/', s.logo_image) as logoImage, 
                    COALESCE(cat.title, '') as category,";
        switch($platform){
            case "android":
                $sql .= " CONCAT('http://bubbllz.com/images/pins/android/', cat.image,'.png') as pin ";
            break;
        
            case "ios":
                $img = $model == "iPhone 6 Plus" ? "3x" : $model == "iPhone 6s Plus" ? "3x" : $model == "iPhone 7 Plus" ? "3x" : "2x";
                $sql .= " CONCAT('http://bubbllz.com/images/pins/ios/$img/', cat.image,'Blue.png') as bluePin,"
                     ." CONCAT('http://bubbllz.com/images/pins/ios/$img/', cat.image,'Orange.png') as orangePin ";
            break;
        }
        $sql.=" from  store s
                inner join user_admin ad on ad.store_id = s.id
                left join city c on c.id = s.city_id
                left join category cat on cat.id = s.category_of 
                where s.main_image is not null and s.status = 1
                and s.id in (select st.id from store st
                where(st.id not in (select store_id from ghost))
                union all
                select store_id from ghost where user_id = $userId)
            ";
            

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $stores = $stmt->fetchAll();

        $imagesSql = "select store_id, concat('http://dev.bubbllz.com/images/stores/', image_name) as image from image where is_main=3";
        $stmt = $connection->prepare($imagesSql);
        $stmt->execute();
        $images = $stmt->fetchAll();

        $response = [];
        foreach ($stores as $val)
        {
            $model = [];
            $model["id"] = intval($val["id"]);
            $model["longitude"] = floatval($val["longitude"]);
            $model["latitude"] = floatval($val["latitude"]);
            $model["telephone"] = $val["telephone"];
            $model["type"] = $val["type_store"];
            $model["storename"] = $val["storename"];
            $model["cityId"] = intval($val["cityId"]);
            $model["city_name"] = $val["city_title"];
            $model["slogan"] = $val["slogan"];
            $model["address"] = $val["address"];
            $model["hashtags"] = $val["hashtags"];
            $model["keywords"] = $val["keywords"];
            $model["mainImage"] = $val["mainImage"];
            $model["logoImage"] = str_replace("\\", "", $val["logoImage"]);
            $model["category"] = $val["category"];
            if($platform == "android"){
                $model["pin"] = $val["pin"];
            }else{
                if($val["bluePin"] == 'http://bubbllz.com/images/pins/ios/'.$img.'/bubbllzBlue.png'){
                    $model["bluePin"] = 'http://bubbllz.com/images/pins/ios/'.$img.'/iosBlue.png';
                    $model["orangePin"] = 'http://bubbllz.com/images/pins/ios/'.$img.'/iosOrange.png';
                }else{
                    $model["bluePin"] = $val["bluePin"];
                    $model["orangePin"] = $val["orangePin"];
                }
            }
            $model["images"] = [];
            foreach ($images as $image)
            {
                if ($image["store_id"] == $val["id"])
                {
                    array_push($model["images"], $image["image"]);
                }
            }
            $response[] = $model;
        }

        foreach ($response as &$val)
        {
            rtrim($val["hashtags"], ",");
            $hashes = explode(",", $val["hashtags"]);
            $newHashArray = [];
            foreach ($hashes as $vval)
            {
                if ($vval != null || $vval != "")
                {
                    $newHashArray[] = str_replace("\\", "", $vval);
                }
            }
            $val["hashtags"] = $newHashArray;
            //$val["hashtags"] = str_replace("\"", "", json_encode($newHashArray, JSON_UNESCAPED_SLASHES));
        }

        return $response;
    }

    public function GetStoresAfterDatetime($datetime)
    {
        
        $header = UserAgentReader::analyzeUserAgent($_SERVER['HTTP_USERAGENT']);
        $platform = $header->platform;
        $model = $header->model;
        
        $datetimeString = $this->getDatetimeFromStringFormat($datetime);

        $connection = $this->entityManager->getConnection();
        $sql = " 
                select 
                    s.id ,
                    s.longitute as longitude, 
                    s.latitude, 
                    COALESCE(s.storename, '') as storename, 
                    COALESCE(s.telephone, '') as telephone, 
                    s.type_store,
                    c.id as cityId, 
                    s.city_title, 
                    COALESCE(s.slogan, '') as slogan, 
                    COALESCE(s.keywords, '') as keywords, 
                    CONCAT(s.address, ' ',  s.address2) as address, 
                    COALESCE(s.perma_tag, '') as hashtags,
                    CONCAT('http://dev.bubbllz.com/images/stores/', s.main_image) as mainImage, 
                    CONCAT('http://dev.bubbllz.com/images/stores/logos/', s.logo_image) as logoImage, 
                    COALESCE(cat.title, '') as category, ";
        switch($platform){
            case "android":
                $sql .= " CONCAT('http://bubbllz.com/images/pins/android/', cat.image,'.png') as pin ";
            break;
        
            case "ios":
                $img = $model == "iPhone 6 Plus" ? "3x" : $model == "iPhone 6s Plus" ? "3x" : $model == "iPhone 7 Plus" ? "3x" : "2x";
                $sql .= " CONCAT('http://bubbllz.com/images/pins/ios/$img/', cat.image,'Blue.png') as bluePin,"
                     ." CONCAT('http://bubbllz.com/images/pins/ios/$img/', cat.image,'Orange.png') as orangePin ";
            break;
        }
        $sql .=" from store s
                 left join city c on c.id=s.city_id
                 left join category cat on cat.id=s.category_of 
                 where s.main_image is not null
                 and s.last_update > '" . $datetimeString . " 00:00:00' and s.status=1
            ";

        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $stores = $stmt->fetchAll();

        $imagesSql = "select store_id, concat('http://dev.bubbllz.com/images/stores/', image_name) as image from image where is_main=3";
        $stmt = $connection->prepare($imagesSql);
        $stmt->execute();
        $images = $stmt->fetchAll();
        
        $response = [];
        foreach ($stores as $val)
        {
            $model = [];
            $model["id"] = intval($val["id"]);
            $model["longitude"] = floatval($val["longitude"]);
            $model["latitude"] = floatval($val["latitude"]);
            $model["telephone"] = $val["telephone"];
            $model["type"] = $val["type_store"];
            $model["storename"] = $val["storename"];
            $model["cityId"] = intval($val["cityId"]);
            $model["city_name"] = $val["city_title"];
            $model["slogan"] = $val["slogan"];
            $model["address"] = $val["address"];
            $model["hashtags"] = $val["hashtags"];
            $model["keywords"] = $val["keywords"];
            $model["mainImage"] = $val["mainImage"];
            $model["logoImage"] = str_replace("\\", "", $val["logoImage"]);
            $model["category"] = $val["category"];
            if($platform == "android"){
                $model["pin"] = $val["pin"];
            }else{
                if($val["bluePin"] == 'http://bubbllz.com/images/pins/ios/'.$img.'/bubbllzBlue.png'){
                    $model["bluePin"] = 'http://bubbllz.com/images/pins/ios/'.$img.'/iosBlue.png';
                    $model["orangePin"] = 'http://bubbllz.com/images/pins/ios/'.$img.'/iosOrange.png';
                }else{
                    $model["bluePin"] = $val["bluePin"];
                    $model["orangePin"] = $val["orangePin"];
                }
            }

            $model["images"] = [];
            foreach ($images as $image)
            {
                if ($image["store_id"] == $val["id"])
                {
                    array_push($model["images"], $image["image"]);
                }
            }
            
            $response[] = $model;
        }

        foreach ($response as &$val)
        {
            rtrim($val["hashtags"], ",");
            $hashes = explode(",", $val["hashtags"]);
            $newHashArray = [];
            foreach ($hashes as $vval)
            {
                if ($vval != null || $vval != "")
                {
                    $newHashArray[] = str_replace("\\", "", $vval);
                }
            }
            $val["hashtags"] = $newHashArray;
            //$val["hashtags"] = str_replace("\"", "", json_encode($newHashArray, JSON_UNESCAPED_SLASHES));
        }

        return $response;
    }

    /**
     * 
     * @param int $id
     * @return string
     */
    public function GetStoreMessageByStoreId($offerId, $offerType = "")
    {
        switch ($offerType)
        {
            case "contest":
                $class = "BubblzEntitiesBundle:Contest";
                break;
            case "offer":
                $class = "BubblzEntitiesBundle:Offer";
                break;
            case "loyalty":
                $class = "BubblzEntitiesBundle:Loyalty";
                break;
            default :
                $class = "BubblzEntitiesBundle:Discount";
                break;
        }
        
        $plan = $this->entityManager->getRepository($class)->createQueryBuilder('s')
                ->select('partial s.{id, afterCheckinMessage}')
                ->where('s.id = :id')
                ->setParameter('id', $offerId)
                ->getQuery()
                ->getSingleResult();

        $message = $plan->getAfterCheckinMessage();
        if($message == "")
        {
            $kernelUser = $this->entityManager->getRepository(KernelUser::class)->find(3);
            $message = $kernelUser->getDefaultMessage();
        }
        return $message;
    }

    private function getDatetimeFromStringFormat($datetime)
    {
        $date = substr($datetime, 0, 2);
        $month = substr($datetime, 2, 2);
        $year = substr($datetime, 4, 4);

        $dateOb = new DateTime($year . "-" . $month . "-" . $date);

        return $dateOb->format("Y-m-d");
    }

}
