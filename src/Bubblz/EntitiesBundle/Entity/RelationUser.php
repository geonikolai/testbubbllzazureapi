<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RelationUser
 *
 * @ORM\Table(name="relation_user", indexes={@ORM\Index(name="fk_relation_user_boom_user1_idx", columns={"master_user_id"}), @ORM\Index(name="fk_relation_user_boom_user2_idx", columns={"slave_user_id"})})
 * @ORM\Entity
 */
class RelationUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="master_user_id", referencedColumnName="id")
     * })
     */
    private $masterUser;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slave_user_id", referencedColumnName="id")
     * })
     */
    private $slaveUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return RelationUser
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return RelationUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set masterUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $masterUser
     *
     * @return RelationUser
     */
    public function setMasterUser(\Bubblz\EntitiesBundle\Entity\BoomUser $masterUser = null)
    {
        $this->masterUser = $masterUser;

        return $this;
    }

    /**
     * Get masterUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getMasterUser()
    {
        return $this->masterUser;
    }

    /**
     * Set slaveUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $slaveUser
     *
     * @return RelationUser
     */
    public function setSlaveUser(\Bubblz\EntitiesBundle\Entity\BoomUser $slaveUser = null)
    {
        $this->slaveUser = $slaveUser;

        return $this;
    }

    /**
     * Get slaveUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getSlaveUser()
    {
        return $this->slaveUser;
    }
}
