<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PolicyStoreRedeem
 *
 * @ORM\Table(name="policy_store_redeem", indexes={@ORM\Index(name="fk_policy_store_redeem_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class PolicyStoreRedeem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var string
     *
     * @ORM\Column(name="product_gift_title", type="string", length=255, nullable=true)
     */
    private $productGiftTitle;

    /**
     * @var float
     *
     * @ORM\Column(name="gift_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $giftPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="u_date", type="datetime", nullable=true)
     */
    private $uDate;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return PolicyStoreRedeem
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set productGiftTitle
     *
     * @param string $productGiftTitle
     *
     * @return PolicyStoreRedeem
     */
    public function setProductGiftTitle($productGiftTitle)
    {
        $this->productGiftTitle = $productGiftTitle;

        return $this;
    }

    /**
     * Get productGiftTitle
     *
     * @return string
     */
    public function getProductGiftTitle()
    {
        return $this->productGiftTitle;
    }

    /**
     * Set giftPrice
     *
     * @param float $giftPrice
     *
     * @return PolicyStoreRedeem
     */
    public function setGiftPrice($giftPrice)
    {
        $this->giftPrice = $giftPrice;

        return $this;
    }

    /**
     * Get giftPrice
     *
     * @return float
     */
    public function getGiftPrice()
    {
        return $this->giftPrice;
    }

    /**
     * Set uDate
     *
     * @param \DateTime $uDate
     *
     * @return PolicyStoreRedeem
     */
    public function setUDate($uDate)
    {
        $this->uDate = $uDate;

        return $this;
    }

    /**
     * Get uDate
     *
     * @return \DateTime
     */
    public function getUDate()
    {
        return $this->uDate;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return PolicyStoreRedeem
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
