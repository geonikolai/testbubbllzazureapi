<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="message_age_range_id", columns={"message_age_range_id"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gender", type="boolean", nullable=false)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="age_range", type="string", length=32, nullable=false)
     */
    private $ageRange;

    /**
     * @var string
     *
     * @ORM\Column(name="checkin_store_targeted_message", type="string", length=500, nullable=false)
     */
    private $checkinStoreTargetedMessage;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \MessageAgeRange
     *
     * @ORM\ManyToOne(targetEntity="MessageAgeRange")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="message_age_range_id", referencedColumnName="id")
     * })
     */
    private $messageAgeRange;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Message
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     *
     * @return Message
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return boolean
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set ageRange
     *
     * @param string $ageRange
     *
     * @return Message
     */
    public function setAgeRange($ageRange)
    {
        $this->ageRange = $ageRange;

        return $this;
    }

    /**
     * Get ageRange
     *
     * @return string
     */
    public function getAgeRange()
    {
        return $this->ageRange;
    }

    /**
     * Set checkinStoreTargetedMessage
     *
     * @param string $checkinStoreTargetedMessage
     *
     * @return Message
     */
    public function setCheckinStoreTargetedMessage($checkinStoreTargetedMessage)
    {
        $this->checkinStoreTargetedMessage = $checkinStoreTargetedMessage;

        return $this;
    }

    /**
     * Get checkinStoreTargetedMessage
     *
     * @return string
     */
    public function getCheckinStoreTargetedMessage()
    {
        return $this->checkinStoreTargetedMessage;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Message
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set messageAgeRange
     *
     * @param \Bubblz\EntitiesBundle\Entity\MessageAgeRange $messageAgeRange
     *
     * @return Message
     */
    public function setMessageAgeRange(\Bubblz\EntitiesBundle\Entity\MessageAgeRange $messageAgeRange = null)
    {
        $this->messageAgeRange = $messageAgeRange;

        return $this;
    }

    /**
     * Get messageAgeRange
     *
     * @return \Bubblz\EntitiesBundle\Entity\MessageAgeRange
     */
    public function getMessageAgeRange()
    {
        return $this->messageAgeRange;
    }
}
