<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lists
 *
 * @ORM\Table(name="lists", indexes={@ORM\Index(name="brand_id", columns={"brand_id"})})
 * @ORM\Entity
 */
class Lists
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="list_name", type="string", length=64, nullable=false)
     */
    private $listName;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listName
     *
     * @param string $listName
     *
     * @return Lists
     */
    public function setListName($listName)
    {
        $this->listName = $listName;

        return $this;
    }

    /**
     * Get listName
     *
     * @return string
     */
    public function getListName()
    {
        return $this->listName;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return Lists
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
