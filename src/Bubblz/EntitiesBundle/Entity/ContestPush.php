<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContestPush
 *
 * @ORM\Table(name="contest_push")
 * @ORM\Entity
 */
class ContestPush
{    
    /**
     * @var integer
     *
     * @ORM\Column(name="store_id", type="integer", nullable=true)
     */
    private $storeId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="contest_id", type="integer", nullable=true)
     */
    private $contestId;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="store_title", type="string", length=255, nullable=true)
     */
    private $storeTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="store_subtitle", type="string", length=255, nullable=true)
     */
    private $storeSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="store_type", type="string", length=255, nullable=true)
     */
    private $storeType;

    /**
     * @var string
     *
     * @ORM\Column(name="store_icon", type="string", length=255, nullable=true)
     */
    private $storeIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="offer_title", type="string", length=255, nullable=true)
     */
    private $offerTitle;
    
    /**
     * @var string
     *
     * @ORM\Column(name="offer_subtitle", type="string", length=255, nullable=true)
     */
    private $offerSubtitle;
    
    /**
     * @var string
     *
     * @ORM\Column(name="offer_description", type="string", length=255, nullable=true)
     */
    private $offerDescription;
    
    /**
     * @var string
     *
     * @ORM\Column(name="offer_type", type="string", length=64, nullable=true)
     */
    private $offerType;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="offer_end_date", type="datetime", nullable=true)
     */
    private $offerEndDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="winner_name", type="string", length=255, nullable=true)
     */
    private $winnerName;

    /**
     * @var string
     *
     * @ORM\Column(name="winner_image", type="string", length=255, nullable=true)
     */
    private $winnerImage;

    /**
     * @var string
     *
     * @ORM\Column(name="offer_title_winner", type="string", length=255, nullable=true)
     */
    private $offerTitleWinner;

    /**
     * @var string
     *
     * @ORM\Column(name="offer_subtitle_winner", type="string", length=255, nullable=true)
     */
    private $offerSubtitleWinner;

    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_number", type="integer", nullable=true)
     */
    private $ticketNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="bubbllz_id", type="integer", nullable=true)
     */
    private $bubbllzId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="terms", type="string", nullable=true)
     */
    private $terms;

    


    /**
     * Set storeId
     *
     * @param integer $storeId
     *
     * @return integer
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }

    /**
     * Get storeId
     *
     * @return integer
     */
    public function getStoreId()
    {
        return $this->storeId;
    }
    
    /**
     * Set contestId
     *
     * @param integer $contestId
     *
     * @return integer
     */
    public function setContestId($contestId)
    {
        $this->contestId = $contestId;

        return $this;
    }

    /**
     * Get contestId
     *
     * @return integer
     */
    public function getContestId()
    {
        return $this->contestId;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return string
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set storeTitle
     *
     * @param string $storeTitle
     *
     * @return string
     */
    public function setStoreTitle($storeTitle)
    {
        $this->storeTitle = $storeTitle;

        return $this;
    }

    /**
     * Get storeTitle
     *
     * @return string
     */
    public function getStoreTitle()
    {
        return $this->storeTitle;
    }
    
    /**
     * Set storeType
     *
     * @param string $storeType
     *
     * @return string
     */
    public function setStoreType($storeType)
    {
        $this->storeType = $storeType;

        return $this;
    }

    /**
     * Get storeType
     *
     * @return string
     */
    public function getStoreType()
    {
        return $this->storeType;
    }
    
        
    /**
     * Set storeIcon
     *
     * @param string $storeIcon
     *
     * @return string
     */
    public function setStoreIcon($storeIcon)
    {
        $this->storeIcon = $storeIcon;

        return $this;
    }

    /**
     * Get storeIcon
     *
     * @return string
     */
    public function getStoreIcon()
    {
        return $this->storeIcon;
    }
    
    /**
     * Set offerTitle
     *
     * @param string $offerTitle
     *
     * @return string
     */
    public function setOfferTitle($offerTitle)
    {
        $this->offerTitle = $offerTitle;

        return $this;
    }

    /**
     * Get offerTitle
     *
     * @return string
     */
    public function getOfferTitle()
    {
        return $this->offerTitle;
    }
        
    /**
     * Set offerSubtitle
     *
     * @param string $offerSubtitle
     *
     * @return string
     */
    public function setOfferSubtitle($offerSubtitle)
    {
        $this->offerSubtitle = $offerSubtitle;

        return $this;
    }

    /**
     * Get offerSubtitle
     *
     * @return string
     */
    public function getOfferSubtitle()
    {
        return $this->offerSubtitle;
    }
            
    /**
     * Set offerDescription
     *
     * @param string $offerDescription
     *
     * @return string
     */
    public function setOfferDescription($offerDescription)
    {
        $this->offerDescription = $offerDescription;

        return $this;
    }

    /**
     * Get offerDescription
     *
     * @return string
     */
    public function getOfferDescription()
    {
        return $this->offerDescription;
    }
                
    /**
     * Set offerType
     *
     * @param string $offerType
     *
     * @return string
     */
    public function setOfferType($offerType)
    {
        $this->offerType = $offerType;

        return $this;
    }

    /**
     * Get offerType
     *
     * @return string
     */
    public function getOfferType()
    {
        return $this->offerType;
    }
    
    /**
     * Set offerEndDate
     *
     * @param \DateTime $offerEndDate
     *
     * @return \DateTime
     */
    public function setOfferEndDate($offerEndDate)
    {
        $this->offerEndDate = $offerEndDate;

        return $this;
    }

    /**
     * Get offerEndDate
     *
     * @return \DateTime
     */
    public function getOfferEndDate()
    {
        return $this->offerEndDate;
    }
    
    /**
     * Set winnerName
     *
     * @param string $winnerName
     *
     * @return string
     */
    public function setWinnerName($winnerName)
    {
        $this->winnerName = $winnerName;

        return $this;
    }

    /**
     * Get winnerName
     *
     * @return string
     */
    public function getWinnerName()
    {
        return $this->winnerName;
    }
        
    /**
     * Set winnerImage
     *
     * @param string $winnerImage
     *
     * @return string
     */
    public function setWinnerImage($winnerImage)
    {
        $this->winnerImage = $winnerImage;

        return $this;
    }

    /**
     * Get winnerImage
     *
     * @return string
     */
    public function getWinnerImage()
    {
        return $this->winnerImage;
    }
            
    /**
     * Set offerTitleWinner
     *
     * @param string $offerTitleWinner
     *
     * @return string
     */
    public function setOfferTitleWinner($offerTitleWinner)
    {
        $this->offerTitleWinner = $offerTitleWinner;

        return $this;
    }

    /**
     * Get offerTitleWinner
     *
     * @return string
     */
    public function getOfferTitleWinner()
    {
        return $this->offerTitleWinner;
    }
                
    /**
     * Set offerSubtitleWinner
     *
     * @param string $offerSubtitleWinner
     *
     * @return string
     */
    public function setOfferSubtitleWinner($offerSubtitleWinner)
    {
        $this->offerSubtitleWinner = $offerSubtitleWinner;

        return $this;
    }

    /**
     * Get offerSubtitleWinner
     *
     * @return string
     */
    public function getOfferSubtitleWinner()
    {
        return $this->offerSubtitleWinner;
    }
        
    /**
     * Set ticketNumber
     *
     * @param integer $ticketNumber
     *
     * @return integer
     */
    public function setTicketNumber($ticketNumber)
    {
        $this->ticketNumber = $ticketNumber;

        return $this;
    }

    /**
     * Get ticketNumber
     *
     * @return integer
     */
    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }
        
    /**
     * Set bubbllzId
     *
     * @param integer $bubbllzId
     *
     * @return integer
     */
    public function setBubbllzId($bubbllzId)
    {
        $this->bubbllzId = $bubbllzId;

        return $this;
    }

    /**
     * Get bubbllzId
     *
     * @return integer
     */
    public function getBubbllzId()
    {
        return $this->bubbllzId;
    }
    
    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return string
     */
    public function setTerms($terms)
    {
        $this->$terms = $terms;

        return $this;
    }

    /**
     * Get $terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }
    
}
