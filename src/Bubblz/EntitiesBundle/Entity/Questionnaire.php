<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionnaire
 *
 * @ORM\Table(name="questionnaire")
 * @ORM\Entity
 */
class Questionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_id", type="string", length=100, nullable=false)
     */
    private $fbId;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=100, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=32, nullable=false)
     */
    private $manufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=32, nullable=false)
     */
    private $model;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer1_rating", type="integer", nullable=false)
     */
    private $answer1Rating;

    /**
     * @var boolean
     *
     * @ORM\Column(name="answer2", type="boolean", nullable=false)
     */
    private $answer2 = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fb_usage", type="boolean", nullable=false)
     */
    private $fbUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fb_speed", type="boolean", nullable=false)
     */
    private $fbSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="map_usage", type="boolean", nullable=false)
     */
    private $mapUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="map_speed", type="boolean", nullable=false)
     */
    private $mapSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="checkin_usage", type="boolean", nullable=false)
     */
    private $checkinUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="checkin_speed", type="boolean", nullable=false)
     */
    private $checkinSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="photo_usage", type="boolean", nullable=false)
     */
    private $photoUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="photo_speed", type="boolean", nullable=false)
     */
    private $photoSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="photoedit_usage", type="boolean", nullable=false)
     */
    private $photoeditUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="photoedit_speed", type="boolean", nullable=false)
     */
    private $photoeditSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hashtags_usage", type="boolean", nullable=false)
     */
    private $hashtagsUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hashtags_speed", type="boolean", nullable=false)
     */
    private $hashtagsSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="post_usage", type="boolean", nullable=false)
     */
    private $postUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="post_speed", type="boolean", nullable=false)
     */
    private $postSpeed = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_usage", type="boolean", nullable=false)
     */
    private $profileUsage = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_speed", type="boolean", nullable=false)
     */
    private $profileSpeed = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="problem_description", type="string", length=500, nullable=true)
     */
    private $problemDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="would_you_use", type="integer", nullable=false)
     */
    private $wouldYouUse;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fbId
     *
     * @param string $fbId
     *
     * @return Questionnaire
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;

        return $this;
    }

    /**
     * Get fbId
     *
     * @return string
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Questionnaire
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Questionnaire
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Questionnaire
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set answer1Rating
     *
     * @param integer $answer1Rating
     *
     * @return Questionnaire
     */
    public function setAnswer1Rating($answer1Rating)
    {
        $this->answer1Rating = $answer1Rating;

        return $this;
    }

    /**
     * Get answer1Rating
     *
     * @return integer
     */
    public function getAnswer1Rating()
    {
        return $this->answer1Rating;
    }

    /**
     * Set answer2
     *
     * @param boolean $answer2
     *
     * @return Questionnaire
     */
    public function setAnswer2($answer2)
    {
        $this->answer2 = $answer2;

        return $this;
    }

    /**
     * Get answer2
     *
     * @return boolean
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Set fbUsage
     *
     * @param boolean $fbUsage
     *
     * @return Questionnaire
     */
    public function setFbUsage($fbUsage)
    {
        $this->fbUsage = $fbUsage;

        return $this;
    }

    /**
     * Get fbUsage
     *
     * @return boolean
     */
    public function getFbUsage()
    {
        return $this->fbUsage;
    }

    /**
     * Set fbSpeed
     *
     * @param boolean $fbSpeed
     *
     * @return Questionnaire
     */
    public function setFbSpeed($fbSpeed)
    {
        $this->fbSpeed = $fbSpeed;

        return $this;
    }

    /**
     * Get fbSpeed
     *
     * @return boolean
     */
    public function getFbSpeed()
    {
        return $this->fbSpeed;
    }

    /**
     * Set mapUsage
     *
     * @param boolean $mapUsage
     *
     * @return Questionnaire
     */
    public function setMapUsage($mapUsage)
    {
        $this->mapUsage = $mapUsage;

        return $this;
    }

    /**
     * Get mapUsage
     *
     * @return boolean
     */
    public function getMapUsage()
    {
        return $this->mapUsage;
    }

    /**
     * Set mapSpeed
     *
     * @param boolean $mapSpeed
     *
     * @return Questionnaire
     */
    public function setMapSpeed($mapSpeed)
    {
        $this->mapSpeed = $mapSpeed;

        return $this;
    }

    /**
     * Get mapSpeed
     *
     * @return boolean
     */
    public function getMapSpeed()
    {
        return $this->mapSpeed;
    }

    /**
     * Set checkinUsage
     *
     * @param boolean $checkinUsage
     *
     * @return Questionnaire
     */
    public function setCheckinUsage($checkinUsage)
    {
        $this->checkinUsage = $checkinUsage;

        return $this;
    }

    /**
     * Get checkinUsage
     *
     * @return boolean
     */
    public function getCheckinUsage()
    {
        return $this->checkinUsage;
    }

    /**
     * Set checkinSpeed
     *
     * @param boolean $checkinSpeed
     *
     * @return Questionnaire
     */
    public function setCheckinSpeed($checkinSpeed)
    {
        $this->checkinSpeed = $checkinSpeed;

        return $this;
    }

    /**
     * Get checkinSpeed
     *
     * @return boolean
     */
    public function getCheckinSpeed()
    {
        return $this->checkinSpeed;
    }

    /**
     * Set photoUsage
     *
     * @param boolean $photoUsage
     *
     * @return Questionnaire
     */
    public function setPhotoUsage($photoUsage)
    {
        $this->photoUsage = $photoUsage;

        return $this;
    }

    /**
     * Get photoUsage
     *
     * @return boolean
     */
    public function getPhotoUsage()
    {
        return $this->photoUsage;
    }

    /**
     * Set photoSpeed
     *
     * @param boolean $photoSpeed
     *
     * @return Questionnaire
     */
    public function setPhotoSpeed($photoSpeed)
    {
        $this->photoSpeed = $photoSpeed;

        return $this;
    }

    /**
     * Get photoSpeed
     *
     * @return boolean
     */
    public function getPhotoSpeed()
    {
        return $this->photoSpeed;
    }

    /**
     * Set photoeditUsage
     *
     * @param boolean $photoeditUsage
     *
     * @return Questionnaire
     */
    public function setPhotoeditUsage($photoeditUsage)
    {
        $this->photoeditUsage = $photoeditUsage;

        return $this;
    }

    /**
     * Get photoeditUsage
     *
     * @return boolean
     */
    public function getPhotoeditUsage()
    {
        return $this->photoeditUsage;
    }

    /**
     * Set photoeditSpeed
     *
     * @param boolean $photoeditSpeed
     *
     * @return Questionnaire
     */
    public function setPhotoeditSpeed($photoeditSpeed)
    {
        $this->photoeditSpeed = $photoeditSpeed;

        return $this;
    }

    /**
     * Get photoeditSpeed
     *
     * @return boolean
     */
    public function getPhotoeditSpeed()
    {
        return $this->photoeditSpeed;
    }

    /**
     * Set hashtagsUsage
     *
     * @param boolean $hashtagsUsage
     *
     * @return Questionnaire
     */
    public function setHashtagsUsage($hashtagsUsage)
    {
        $this->hashtagsUsage = $hashtagsUsage;

        return $this;
    }

    /**
     * Get hashtagsUsage
     *
     * @return boolean
     */
    public function getHashtagsUsage()
    {
        return $this->hashtagsUsage;
    }

    /**
     * Set hashtagsSpeed
     *
     * @param boolean $hashtagsSpeed
     *
     * @return Questionnaire
     */
    public function setHashtagsSpeed($hashtagsSpeed)
    {
        $this->hashtagsSpeed = $hashtagsSpeed;

        return $this;
    }

    /**
     * Get hashtagsSpeed
     *
     * @return boolean
     */
    public function getHashtagsSpeed()
    {
        return $this->hashtagsSpeed;
    }

    /**
     * Set postUsage
     *
     * @param boolean $postUsage
     *
     * @return Questionnaire
     */
    public function setPostUsage($postUsage)
    {
        $this->postUsage = $postUsage;

        return $this;
    }

    /**
     * Get postUsage
     *
     * @return boolean
     */
    public function getPostUsage()
    {
        return $this->postUsage;
    }

    /**
     * Set postSpeed
     *
     * @param boolean $postSpeed
     *
     * @return Questionnaire
     */
    public function setPostSpeed($postSpeed)
    {
        $this->postSpeed = $postSpeed;

        return $this;
    }

    /**
     * Get postSpeed
     *
     * @return boolean
     */
    public function getPostSpeed()
    {
        return $this->postSpeed;
    }

    /**
     * Set profileUsage
     *
     * @param boolean $profileUsage
     *
     * @return Questionnaire
     */
    public function setProfileUsage($profileUsage)
    {
        $this->profileUsage = $profileUsage;

        return $this;
    }

    /**
     * Get profileUsage
     *
     * @return boolean
     */
    public function getProfileUsage()
    {
        return $this->profileUsage;
    }

    /**
     * Set profileSpeed
     *
     * @param boolean $profileSpeed
     *
     * @return Questionnaire
     */
    public function setProfileSpeed($profileSpeed)
    {
        $this->profileSpeed = $profileSpeed;

        return $this;
    }

    /**
     * Get profileSpeed
     *
     * @return boolean
     */
    public function getProfileSpeed()
    {
        return $this->profileSpeed;
    }

    /**
     * Set problemDescription
     *
     * @param string $problemDescription
     *
     * @return Questionnaire
     */
    public function setProblemDescription($problemDescription)
    {
        $this->problemDescription = $problemDescription;

        return $this;
    }

    /**
     * Get problemDescription
     *
     * @return string
     */
    public function getProblemDescription()
    {
        return $this->problemDescription;
    }

    /**
     * Set wouldYouUse
     *
     * @param integer $wouldYouUse
     *
     * @return Questionnaire
     */
    public function setWouldYouUse($wouldYouUse)
    {
        $this->wouldYouUse = $wouldYouUse;

        return $this;
    }

    /**
     * Get wouldYouUse
     *
     * @return integer
     */
    public function getWouldYouUse()
    {
        return $this->wouldYouUse;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Questionnaire
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Questionnaire
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }
}
