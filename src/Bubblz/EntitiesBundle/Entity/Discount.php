<?php

namespace Bubblz\EntitiesBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Discount
 *
 * @ORM\Table(name="discount", indexes={@ORM\Index(name="fk_discount_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class Discount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="steady_value", type="boolean", nullable=false)
     */
    private $steadyValue = '1';
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="remote", type="boolean", nullable=false)
     */
    private $remote = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="unattended", type="boolean", nullable=false)
     */
    private $unattended = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="before_checkin_message", type="string", length=100, nullable=true)
     */
    private $beforeCheckinMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="after_checkin_message", type="string", length=100, nullable=true)
     */
    private $afterCheckinMessage;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return Discount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set startDate
     *
     * @param DateTime $startDate
     *
     * @return Discount
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param DateTime $endDate
     *
     * @return Discount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set remote
     *
     * @param boolean $remote
     *
     * @return Discount
     */
    public function setRemote($remote)
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * Get remote
     *
     * @return boolean
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * Set unattended
     *
     * @param boolean $unattended
     *
     * @return Discount
     */
    public function setUnattended($unattended)
    {
        $this->unattended = $unattended;

        return $this;
    }

    /**
     * Get unattended
     *
     * @return boolean
     */
    public function getUnattended()
    {
        return $this->unattended;
    }

    /**
     * Set beforeCheckinMessage
     *
     * @param string $beforeCheckinMessage
     *
     * @return Discount
     */
    public function setBeforeCheckinMessage($beforeCheckinMessage)
    {
        $this->beforeCheckinMessage = $beforeCheckinMessage;

        return $this;
    }

    /**
     * Get beforeCheckinMessage
     *
     * @return string
     */
    public function getBeforeCheckinMessage()
    {
        return $this->beforeCheckinMessage;
    }

    /**
     * Set afterCheckinMessage
     *
     * @param string $afterCheckinMessage
     *
     * @return Discount
     */
    public function setAfterCheckinMessage($afterCheckinMessage)
    {
        $this->afterCheckinMessage = $afterCheckinMessage;

        return $this;
    }

    /**
     * Get afterCheckinMessage
     *
     * @return string
     */
    public function getAfterCheckinMessage()
    {
        return $this->afterCheckinMessage;
    }

    /**
     * Set cDate
     *
     * @param DateTime $cDate
     *
     * @return Discount
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set lastUpdate
     *
     * @param DateTime $lastUpdate
     *
     * @return Discount
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set store
     *
     * @param Store $store
     *
     * @return Discount
     */
    public function setStore(Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set steadyValue
     *
     * @param boolean $steadyValue
     *
     * @return Discount
     */
    public function setSteadyValue($steadyValue)
    {
        $this->steadyValue = $steadyValue;

        return $this;
    }

    /**
     * Get steadyValue
     *
     * @return boolean
     */
    public function getSteadyValue()
    {
        return $this->steadyValue;
    }
}
