<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address", indexes={@ORM\Index(name="fk_store_marketplace1_idx", columns={"marketplace_id"}), @ORM\Index(name="fk_store_city1_idx", columns={"city_id"}), @ORM\Index(name="fk_store_country1_idx", columns={"country_id"}), @ORM\Index(name="fk_store_address_state1_idx", columns={"state_id"}), @ORM\Index(name="fk_store_address_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=60, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=60, nullable=true)
     */
    private $address2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_boom_address", type="boolean", nullable=true)
     */
    private $isBoomAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_number", type="string", length=20, nullable=true)
     */
    private $vatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=13, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="broadcast_message", type="string", length=100, nullable=true)
     */
    private $broadcastMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=45, nullable=true)
     */
    private $mainImage;

    /**
     * @var float
     *
     * @ORM\Column(name="longitute", type="float", precision=10, scale=0, nullable=true)
     */
    private $longitute;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="bing_map_link", type="string", length=255, nullable=true)
     */
    private $bingMapLink;

    /**
     * @var string
     *
     * @ORM\Column(name="google_map_link", type="string", length=255, nullable=true)
     */
    private $googleMapLink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_broadcast_message", type="boolean", nullable=true)
     */
    private $permitBroadcastMessage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_diff_logo_image", type="boolean", nullable=true)
     */
    private $permitDiffLogoImage;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_image", type="string", length=45, nullable=true)
     */
    private $logoImage;

    /**
     * @var \Marketplace
     *
     * @ORM\ManyToOne(targetEntity="Marketplace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     * })
     */
    private $marketplace;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Address
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Address
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Address
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set isBoomAddress
     *
     * @param boolean $isBoomAddress
     *
     * @return Address
     */
    public function setIsBoomAddress($isBoomAddress)
    {
        $this->isBoomAddress = $isBoomAddress;

        return $this;
    }

    /**
     * Get isBoomAddress
     *
     * @return boolean
     */
    public function getIsBoomAddress()
    {
        return $this->isBoomAddress;
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return Address
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set broadcastMessage
     *
     * @param string $broadcastMessage
     *
     * @return Address
     */
    public function setBroadcastMessage($broadcastMessage)
    {
        $this->broadcastMessage = $broadcastMessage;

        return $this;
    }

    /**
     * Get broadcastMessage
     *
     * @return string
     */
    public function getBroadcastMessage()
    {
        return $this->broadcastMessage;
    }

    /**
     * Set mainImage
     *
     * @param string $mainImage
     *
     * @return Address
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Get mainImage
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set longitute
     *
     * @param float $longitute
     *
     * @return Address
     */
    public function setLongitute($longitute)
    {
        $this->longitute = $longitute;

        return $this;
    }

    /**
     * Get longitute
     *
     * @return float
     */
    public function getLongitute()
    {
        return $this->longitute;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Address
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set bingMapLink
     *
     * @param string $bingMapLink
     *
     * @return Address
     */
    public function setBingMapLink($bingMapLink)
    {
        $this->bingMapLink = $bingMapLink;

        return $this;
    }

    /**
     * Get bingMapLink
     *
     * @return string
     */
    public function getBingMapLink()
    {
        return $this->bingMapLink;
    }

    /**
     * Set googleMapLink
     *
     * @param string $googleMapLink
     *
     * @return Address
     */
    public function setGoogleMapLink($googleMapLink)
    {
        $this->googleMapLink = $googleMapLink;

        return $this;
    }

    /**
     * Get googleMapLink
     *
     * @return string
     */
    public function getGoogleMapLink()
    {
        return $this->googleMapLink;
    }

    /**
     * Set permitBroadcastMessage
     *
     * @param boolean $permitBroadcastMessage
     *
     * @return Address
     */
    public function setPermitBroadcastMessage($permitBroadcastMessage)
    {
        $this->permitBroadcastMessage = $permitBroadcastMessage;

        return $this;
    }

    /**
     * Get permitBroadcastMessage
     *
     * @return boolean
     */
    public function getPermitBroadcastMessage()
    {
        return $this->permitBroadcastMessage;
    }

    /**
     * Set permitDiffLogoImage
     *
     * @param boolean $permitDiffLogoImage
     *
     * @return Address
     */
    public function setPermitDiffLogoImage($permitDiffLogoImage)
    {
        $this->permitDiffLogoImage = $permitDiffLogoImage;

        return $this;
    }

    /**
     * Get permitDiffLogoImage
     *
     * @return boolean
     */
    public function getPermitDiffLogoImage()
    {
        return $this->permitDiffLogoImage;
    }

    /**
     * Set logoImage
     *
     * @param string $logoImage
     *
     * @return Address
     */
    public function setLogoImage($logoImage)
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    /**
     * Get logoImage
     *
     * @return string
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * Set marketplace
     *
     * @param \Bubblz\EntitiesBundle\Entity\Marketplace $marketplace
     *
     * @return Address
     */
    public function setMarketplace(\Bubblz\EntitiesBundle\Entity\Marketplace $marketplace = null)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return \Bubblz\EntitiesBundle\Entity\Marketplace
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return Address
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Address
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return Address
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return Address
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
