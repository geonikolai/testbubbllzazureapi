<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TotalPoints
 *
 * @ORM\Table(name="total_points", indexes={@ORM\Index(name="boom_user_id", columns={"boom_user_id"}), @ORM\Index(name="store_id", columns={"store_id"})})
 * @ORM\Entity
 */
class TotalPoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="all_time_points", type="integer", nullable=false)
     */
    private $allTimePoints = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="available_points", type="integer", nullable=false)
     */
    private $availablePoints = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="redeemed_points", type="integer", nullable=false)
     */
    private $redeemedPoints = '0';

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set allTimePoints
     *
     * @param integer $allTimePoints
     *
     * @return TotalPoints
     */
    public function setAllTimePoints($allTimePoints)
    {
        $this->allTimePoints = $allTimePoints;

        return $this;
    }

    /**
     * Get allTimePoints
     *
     * @return integer
     */
    public function getAllTimePoints()
    {
        return $this->allTimePoints;
    }

    /**
     * Set availablePoints
     *
     * @param integer $availablePoints
     *
     * @return TotalPoints
     */
    public function setAvailablePoints($availablePoints)
    {
        $this->availablePoints = $availablePoints;

        return $this;
    }

    /**
     * Get availablePoints
     *
     * @return integer
     */
    public function getAvailablePoints()
    {
        return $this->availablePoints;
    }

    /**
     * Set redeemedPoints
     *
     * @param integer $redeemedPoints
     *
     * @return TotalPoints
     */
    public function setRedeemedPoints($redeemedPoints)
    {
        $this->redeemedPoints = $redeemedPoints;

        return $this;
    }

    /**
     * Get redeemedPoints
     *
     * @return integer
     */
    public function getRedeemedPoints()
    {
        return $this->redeemedPoints;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return TotalPoints
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return TotalPoints
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }
}
