<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Store
 *
 * @ORM\Table(name="store", indexes={@ORM\Index(name="fk_store_organization1_idx", columns={"organization_id"}), @ORM\Index(name="fk_store_country2_idx", columns={"country_id"}), @ORM\Index(name="fk_store_state_union1_idx", columns={"state_id"}), @ORM\Index(name="fk_store_city2_idx", columns={"city_id"}), @ORM\Index(name="fk_store_marketplace2_idx", columns={"marketplace_id"}), @ORM\Index(name="fk_store_area1_idx", columns={"area_id"})})
 * @ORM\Entity
 */
class Store
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="storename", type="string", length=100, nullable=false)
     */
    private $storename;

    /**
     * @var integer
     *
     * @ORM\Column(name="afm", type="integer", nullable=true)
     */
    private $afm;

    /**
     * @var string
     *
     * @ORM\Column(name="real_name", type="string", length=255, nullable=true)
     */
    private $realName;

    /**
     * @var string
     *
     * @ORM\Column(name="doy", type="string", length=100, nullable=true)
     */
    private $doy;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_image", type="string", length=255, nullable=true)
     */
    private $logoImage;

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=255, nullable=true)
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_tags", type="string", length=255, nullable=true)
     */
    private $hashTags;

    /**
     * @var string
     *
     * @ORM\Column(name="perma_tag", type="string", length=64, nullable=true)
     */
    private $permaTag;

    /**
     * @var string
     *
     * @ORM\Column(name="broadcast_message", type="string", length=100, nullable=true)
     */
    private $broadcastMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="store_general_message", type="string", length=100, nullable=true)
     */
    private $storeGeneralMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="confirmation_date", type="datetime", nullable=true)
     */
    private $confirmationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="completion_date", type="datetime", nullable=true)
     */
    private $completionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_store", type="integer", nullable=true)
     */
    private $typeStore;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     */
    private $telephone;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_of", type="integer", nullable=true)
     */
    private $categoryOf;

    /**
     * @var string
     *
     * @ORM\Column(name="state_title", type="string", length=45, nullable=true)
     */
    private $stateTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="country_title", type="string", length=45, nullable=true)
     */
    private $countryTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace_title", type="string", length=45, nullable=true)
     */
    private $marketplaceTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="city_title", type="string", length=45, nullable=true)
     */
    private $cityTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=60, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=60, nullable=true)
     */
    private $address2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_boom_address", type="boolean", nullable=true)
     */
    private $isBoomAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_number", type="string", length=20, nullable=true)
     */
    private $vatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=13, nullable=true)
     */
    private $postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     * @var decimal
     *
     * @ORM\Column(name="longitute", type="decimal", precision=9, scale=6, nullable=true)
     */
    private $longitute;

    /**
     * @var decimal
     *
     * @ORM\Column(name="latitude", type="decimal", precision=9, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="bing_map_link", type="string", length=255, nullable=true)
     */
    private $bingMapLink;

    /**
     * @var string
     *
     * @ORM\Column(name="google_map_link", type="string", length=255, nullable=true)
     */
    private $googleMapLink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_broadcast_message", type="boolean", nullable=true)
     */
    private $permitBroadcastMessage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_diff_logo_image", type="boolean", nullable=true)
     */
    private $permitDiffLogoImage;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_booms", type="integer", nullable=true)
     */
    private $countBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes", type="integer", nullable=true)
     */
    private $fbLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes", type="integer", nullable=true)
     */
    private $twLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes", type="integer", nullable=true)
     */
    private $instLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes", type="integer", nullable=true)
     */
    private $pinterLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_counts", type="integer", nullable=true)
     */
    private $couponCounts;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_booms", type="integer", nullable=true)
     */
    private $fbBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_booms", type="integer", nullable=true)
     */
    private $twBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_booms", type="integer", nullable=true)
     */
    private $instBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_booms", type="integer", nullable=true)
     */
    private $pinterBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_views", type="integer", nullable=true)
     */
    private $fbViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_views", type="integer", nullable=true)
     */
    private $twViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_views", type="integer", nullable=true)
     */
    private $instViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_views", type="integer", nullable=true)
     */
    private $pinterViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares", type="integer", nullable=true)
     */
    private $fbShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares", type="integer", nullable=true)
     */
    private $twShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares", type="integer", nullable=true)
     */
    private $instShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares", type="integer", nullable=true)
     */
    private $pinterShares;

    /**
     * @var float
     *
     * @ORM\Column(name="total_causes", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_causes", type="integer", nullable=true)
     */
    private $countCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_points", type="integer", nullable=true)
     */
    private $loyaltyPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="total_boom_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalBoomDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_boom_discount", type="integer", nullable=true)
     */
    private $countBoomDiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_coupon_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCouponDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_coupon_discount", type="integer", nullable=true)
     */
    private $countCouponDiscount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="fb_page_id", type="string", length=100, nullable=true)
     */
    private $fbPageId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Area
     *
     * @ORM\ManyToOne(targetEntity="Area")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     * })
     */
    private $area;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \Marketplace
     *
     * @ORM\ManyToOne(targetEntity="Marketplace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     * })
     */
    private $marketplace;

    /**
     * @var \Organization
     *
     * @ORM\ManyToOne(targetEntity="Organization")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * })
     */
    private $organization;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set storename
     *
     * @param string $storename
     *
     * @return Store
     */
    public function setStorename($storename)
    {
        $this->storename = $storename;

        return $this;
    }

    /**
     * Get storename
     *
     * @return string
     */
    public function getStorename()
    {
        return $this->storename;
    }

    /**
     * Set afm
     *
     * @param integer $afm
     *
     * @return Store
     */
    public function setAfm($afm)
    {
        $this->afm = $afm;

        return $this;
    }

    /**
     * Get afm
     *
     * @return integer
     */
    public function getAfm()
    {
        return $this->afm;
    }

    /**
     * Set realName
     *
     * @param string $realName
     *
     * @return Store
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;

        return $this;
    }

    /**
     * Get realName
     *
     * @return string
     */
    public function getRealName()
    {
        return $this->realName;
    }
    
    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return String
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set doy
     *
     * @param string $doy
     *
     * @return Store
     */
    public function setDoy($doy)
    {
        $this->doy = $doy;

        return $this;
    }

    /**
     * Get doy
     *
     * @return string
     */
    public function getDoy()
    {
        return $this->doy;
    }

    /**
     * Set logoImage
     *
     * @param string $logoImage
     *
     * @return Store
     */
    public function setLogoImage($logoImage)
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    /**
     * Get logoImage
     *
     * @return string
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * Set mainImage
     *
     * @param string $mainImage
     *
     * @return Store
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Get mainImage
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Store
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set hashTags
     *
     * @param string $hashTags
     *
     * @return Store
     */
    public function setHashTags($hashTags)
    {
        $this->hashTags = $hashTags;

        return $this;
    }

    /**
     * Get hashTags
     *
     * @return string
     */
    public function getHashTags()
    {
        return $this->hashTags;
    }

    /**
     * Set permaTag
     *
     * @param string $permaTag
     *
     * @return Store
     */
    public function setPermaTag($permaTag)
    {
        $this->permaTag = $permaTag;

        return $this;
    }

    /**
     * Get permaTag
     *
     * @return string
     */
    public function getPermaTag()
    {
        return $this->permaTag;
    }

    /**
     * Set broadcastMessage
     *
     * @param string $broadcastMessage
     *
     * @return Store
     */
    public function setBroadcastMessage($broadcastMessage)
    {
        $this->broadcastMessage = $broadcastMessage;

        return $this;
    }

    /**
     * Get broadcastMessage
     *
     * @return string
     */
    public function getBroadcastMessage()
    {
        return $this->broadcastMessage;
    }

    /**
     * Set storeGeneralMessage
     *
     * @param string $storeGeneralMessage
     *
     * @return Store
     */
    public function setStoreGeneralMessage($storeGeneralMessage)
    {
        $this->storeGeneralMessage = $storeGeneralMessage;

        return $this;
    }

    /**
     * Get storeGeneralMessage
     *
     * @return string
     */
    public function getStoreGeneralMessage()
    {
        return $this->storeGeneralMessage;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Store
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set confirmationDate
     *
     * @param \DateTime $confirmationDate
     *
     * @return Store
     */
    public function setConfirmationDate($confirmationDate)
    {
        $this->confirmationDate = $confirmationDate;

        return $this;
    }

    /**
     * Get confirmationDate
     *
     * @return \DateTime
     */
    public function getConfirmationDate()
    {
        return $this->confirmationDate;
    }

    /**
     * Set completionDate
     *
     * @param \DateTime $completionDate
     *
     * @return Store
     */
    public function setCompletionDate($completionDate)
    {
        $this->completionDate = $completionDate;

        return $this;
    }

    /**
     * Get completionDate
     *
     * @return \DateTime
     */
    public function getCompletionDate()
    {
        return $this->completionDate;
    }

    /**
     * Set typeStore
     *
     * @param integer $typeStore
     *
     * @return Store
     */
    public function setTypeStore($typeStore)
    {
        $this->typeStore = $typeStore;

        return $this;
    }

    /**
     * Get typeStore
     *
     * @return integer
     */
    public function getTypeStore()
    {
        return $this->typeStore;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Store
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Store
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set categoryOf
     *
     * @param integer $categoryOf
     *
     * @return Store
     */
    public function setCategoryOf($categoryOf)
    {
        $this->categoryOf = $categoryOf;

        return $this;
    }

    /**
     * Get categoryOf
     *
     * @return integer
     */
    public function getCategoryOf()
    {
        return $this->categoryOf;
    }

    /**
     * Set stateTitle
     *
     * @param string $stateTitle
     *
     * @return Store
     */
    public function setStateTitle($stateTitle)
    {
        $this->stateTitle = $stateTitle;

        return $this;
    }

    /**
     * Get stateTitle
     *
     * @return string
     */
    public function getStateTitle()
    {
        return $this->stateTitle;
    }

    /**
     * Set countryTitle
     *
     * @param string $countryTitle
     *
     * @return Store
     */
    public function setCountryTitle($countryTitle)
    {
        $this->countryTitle = $countryTitle;

        return $this;
    }

    /**
     * Get countryTitle
     *
     * @return string
     */
    public function getCountryTitle()
    {
        return $this->countryTitle;
    }

    /**
     * Set marketplaceTitle
     *
     * @param string $marketplaceTitle
     *
     * @return Store
     */
    public function setMarketplaceTitle($marketplaceTitle)
    {
        $this->marketplaceTitle = $marketplaceTitle;

        return $this;
    }

    /**
     * Get marketplaceTitle
     *
     * @return string
     */
    public function getMarketplaceTitle()
    {
        return $this->marketplaceTitle;
    }

    /**
     * Set cityTitle
     *
     * @param string $cityTitle
     *
     * @return Store
     */
    public function setCityTitle($cityTitle)
    {
        $this->cityTitle = $cityTitle;

        return $this;
    }

    /**
     * Get cityTitle
     *
     * @return string
     */
    public function getCityTitle()
    {
        return $this->cityTitle;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Store
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Store
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Store
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set isBoomAddress
     *
     * @param boolean $isBoomAddress
     *
     * @return Store
     */
    public function setIsBoomAddress($isBoomAddress)
    {
        $this->isBoomAddress = $isBoomAddress;

        return $this;
    }

    /**
     * Get isBoomAddress
     *
     * @return boolean
     */
    public function getIsBoomAddress()
    {
        return $this->isBoomAddress;
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return Store
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Store
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set longitute
     *
     * @param decimal $longitute
     *
     * @return Store
     */
    public function setLongitute($longitute)
    {
        $this->longitute = $longitute;

        return $this;
    }

    /**
     * Get longitute
     *
     * @return decimal
     */
    public function getLongitute()
    {
        return $this->longitute;
    }

    /**
     * Set latitude
     *
     * @param integer $latitude
     *
     * @return Store
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return integer
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set bingMapLink
     *
     * @param string $bingMapLink
     *
     * @return Store
     */
    public function setBingMapLink($bingMapLink)
    {
        $this->bingMapLink = $bingMapLink;

        return $this;
    }

    /**
     * Get bingMapLink
     *
     * @return string
     */
    public function getBingMapLink()
    {
        return $this->bingMapLink;
    }

    /**
     * Set googleMapLink
     *
     * @param string $googleMapLink
     *
     * @return Store
     */
    public function setGoogleMapLink($googleMapLink)
    {
        $this->googleMapLink = $googleMapLink;

        return $this;
    }

    /**
     * Get googleMapLink
     *
     * @return string
     */
    public function getGoogleMapLink()
    {
        return $this->googleMapLink;
    }

    /**
     * Set permitBroadcastMessage
     *
     * @param boolean $permitBroadcastMessage
     *
     * @return Store
     */
    public function setPermitBroadcastMessage($permitBroadcastMessage)
    {
        $this->permitBroadcastMessage = $permitBroadcastMessage;

        return $this;
    }

    /**
     * Get permitBroadcastMessage
     *
     * @return boolean
     */
    public function getPermitBroadcastMessage()
    {
        return $this->permitBroadcastMessage;
    }

    /**
     * Set permitDiffLogoImage
     *
     * @param boolean $permitDiffLogoImage
     *
     * @return Store
     */
    public function setPermitDiffLogoImage($permitDiffLogoImage)
    {
        $this->permitDiffLogoImage = $permitDiffLogoImage;

        return $this;
    }

    /**
     * Get permitDiffLogoImage
     *
     * @return boolean
     */
    public function getPermitDiffLogoImage()
    {
        return $this->permitDiffLogoImage;
    }

    /**
     * Set countBooms
     *
     * @param integer $countBooms
     *
     * @return Store
     */
    public function setCountBooms($countBooms)
    {
        $this->countBooms = $countBooms;

        return $this;
    }

    /**
     * Get countBooms
     *
     * @return integer
     */
    public function getCountBooms()
    {
        return $this->countBooms;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Store
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set fbLikes
     *
     * @param integer $fbLikes
     *
     * @return Store
     */
    public function setFbLikes($fbLikes)
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    /**
     * Get fbLikes
     *
     * @return integer
     */
    public function getFbLikes()
    {
        return $this->fbLikes;
    }

    /**
     * Set twLikes
     *
     * @param integer $twLikes
     *
     * @return Store
     */
    public function setTwLikes($twLikes)
    {
        $this->twLikes = $twLikes;

        return $this;
    }

    /**
     * Get twLikes
     *
     * @return integer
     */
    public function getTwLikes()
    {
        return $this->twLikes;
    }

    /**
     * Set instLikes
     *
     * @param integer $instLikes
     *
     * @return Store
     */
    public function setInstLikes($instLikes)
    {
        $this->instLikes = $instLikes;

        return $this;
    }

    /**
     * Get instLikes
     *
     * @return integer
     */
    public function getInstLikes()
    {
        return $this->instLikes;
    }

    /**
     * Set pinterLikes
     *
     * @param integer $pinterLikes
     *
     * @return Store
     */
    public function setPinterLikes($pinterLikes)
    {
        $this->pinterLikes = $pinterLikes;

        return $this;
    }

    /**
     * Get pinterLikes
     *
     * @return integer
     */
    public function getPinterLikes()
    {
        return $this->pinterLikes;
    }

    /**
     * Set couponCounts
     *
     * @param integer $couponCounts
     *
     * @return Store
     */
    public function setCouponCounts($couponCounts)
    {
        $this->couponCounts = $couponCounts;

        return $this;
    }

    /**
     * Get couponCounts
     *
     * @return integer
     */
    public function getCouponCounts()
    {
        return $this->couponCounts;
    }

    /**
     * Set fbBooms
     *
     * @param integer $fbBooms
     *
     * @return Store
     */
    public function setFbBooms($fbBooms)
    {
        $this->fbBooms = $fbBooms;

        return $this;
    }

    /**
     * Get fbBooms
     *
     * @return integer
     */
    public function getFbBooms()
    {
        return $this->fbBooms;
    }

    /**
     * Set twBooms
     *
     * @param integer $twBooms
     *
     * @return Store
     */
    public function setTwBooms($twBooms)
    {
        $this->twBooms = $twBooms;

        return $this;
    }

    /**
     * Get twBooms
     *
     * @return integer
     */
    public function getTwBooms()
    {
        return $this->twBooms;
    }

    /**
     * Set instBooms
     *
     * @param integer $instBooms
     *
     * @return Store
     */
    public function setInstBooms($instBooms)
    {
        $this->instBooms = $instBooms;

        return $this;
    }

    /**
     * Get instBooms
     *
     * @return integer
     */
    public function getInstBooms()
    {
        return $this->instBooms;
    }

    /**
     * Set pinterBooms
     *
     * @param integer $pinterBooms
     *
     * @return Store
     */
    public function setPinterBooms($pinterBooms)
    {
        $this->pinterBooms = $pinterBooms;

        return $this;
    }

    /**
     * Get pinterBooms
     *
     * @return integer
     */
    public function getPinterBooms()
    {
        return $this->pinterBooms;
    }

    /**
     * Set fbViews
     *
     * @param integer $fbViews
     *
     * @return Store
     */
    public function setFbViews($fbViews)
    {
        $this->fbViews = $fbViews;

        return $this;
    }

    /**
     * Get fbViews
     *
     * @return integer
     */
    public function getFbViews()
    {
        return $this->fbViews;
    }

    /**
     * Set twViews
     *
     * @param integer $twViews
     *
     * @return Store
     */
    public function setTwViews($twViews)
    {
        $this->twViews = $twViews;

        return $this;
    }

    /**
     * Get twViews
     *
     * @return integer
     */
    public function getTwViews()
    {
        return $this->twViews;
    }

    /**
     * Set instViews
     *
     * @param integer $instViews
     *
     * @return Store
     */
    public function setInstViews($instViews)
    {
        $this->instViews = $instViews;

        return $this;
    }

    /**
     * Get instViews
     *
     * @return integer
     */
    public function getInstViews()
    {
        return $this->instViews;
    }

    /**
     * Set pinterViews
     *
     * @param integer $pinterViews
     *
     * @return Store
     */
    public function setPinterViews($pinterViews)
    {
        $this->pinterViews = $pinterViews;

        return $this;
    }

    /**
     * Get pinterViews
     *
     * @return integer
     */
    public function getPinterViews()
    {
        return $this->pinterViews;
    }

    /**
     * Set fbShares
     *
     * @param integer $fbShares
     *
     * @return Store
     */
    public function setFbShares($fbShares)
    {
        $this->fbShares = $fbShares;

        return $this;
    }

    /**
     * Get fbShares
     *
     * @return integer
     */
    public function getFbShares()
    {
        return $this->fbShares;
    }

    /**
     * Set twShares
     *
     * @param integer $twShares
     *
     * @return Store
     */
    public function setTwShares($twShares)
    {
        $this->twShares = $twShares;

        return $this;
    }

    /**
     * Get twShares
     *
     * @return integer
     */
    public function getTwShares()
    {
        return $this->twShares;
    }

    /**
     * Set instShares
     *
     * @param integer $instShares
     *
     * @return Store
     */
    public function setInstShares($instShares)
    {
        $this->instShares = $instShares;

        return $this;
    }

    /**
     * Get instShares
     *
     * @return integer
     */
    public function getInstShares()
    {
        return $this->instShares;
    }

    /**
     * Set pinterShares
     *
     * @param integer $pinterShares
     *
     * @return Store
     */
    public function setPinterShares($pinterShares)
    {
        $this->pinterShares = $pinterShares;

        return $this;
    }

    /**
     * Get pinterShares
     *
     * @return integer
     */
    public function getPinterShares()
    {
        return $this->pinterShares;
    }

    /**
     * Set totalCauses
     *
     * @param float $totalCauses
     *
     * @return Store
     */
    public function setTotalCauses($totalCauses)
    {
        $this->totalCauses = $totalCauses;

        return $this;
    }

    /**
     * Get totalCauses
     *
     * @return float
     */
    public function getTotalCauses()
    {
        return $this->totalCauses;
    }

    /**
     * Set countCauses
     *
     * @param integer $countCauses
     *
     * @return Store
     */
    public function setCountCauses($countCauses)
    {
        $this->countCauses = $countCauses;

        return $this;
    }

    /**
     * Get countCauses
     *
     * @return integer
     */
    public function getCountCauses()
    {
        return $this->countCauses;
    }

    /**
     * Set loyaltyPoints
     *
     * @param integer $loyaltyPoints
     *
     * @return Store
     */
    public function setLoyaltyPoints($loyaltyPoints)
    {
        $this->loyaltyPoints = $loyaltyPoints;

        return $this;
    }

    /**
     * Get loyaltyPoints
     *
     * @return integer
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Set totalBoomDiscount
     *
     * @param float $totalBoomDiscount
     *
     * @return Store
     */
    public function setTotalBoomDiscount($totalBoomDiscount)
    {
        $this->totalBoomDiscount = $totalBoomDiscount;

        return $this;
    }

    /**
     * Get totalBoomDiscount
     *
     * @return float
     */
    public function getTotalBoomDiscount()
    {
        return $this->totalBoomDiscount;
    }

    /**
     * Set countBoomDiscount
     *
     * @param integer $countBoomDiscount
     *
     * @return Store
     */
    public function setCountBoomDiscount($countBoomDiscount)
    {
        $this->countBoomDiscount = $countBoomDiscount;

        return $this;
    }

    /**
     * Get countBoomDiscount
     *
     * @return integer
     */
    public function getCountBoomDiscount()
    {
        return $this->countBoomDiscount;
    }

    /**
     * Set totalCouponDiscount
     *
     * @param float $totalCouponDiscount
     *
     * @return Store
     */
    public function setTotalCouponDiscount($totalCouponDiscount)
    {
        $this->totalCouponDiscount = $totalCouponDiscount;

        return $this;
    }

    /**
     * Get totalCouponDiscount
     *
     * @return float
     */
    public function getTotalCouponDiscount()
    {
        return $this->totalCouponDiscount;
    }

    /**
     * Set countCouponDiscount
     *
     * @param integer $countCouponDiscount
     *
     * @return Store
     */
    public function setCountCouponDiscount($countCouponDiscount)
    {
        $this->countCouponDiscount = $countCouponDiscount;

        return $this;
    }

    /**
     * Get countCouponDiscount
     *
     * @return integer
     */
    public function getCountCouponDiscount()
    {
        return $this->countCouponDiscount;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Store
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fbPageId
     *
     * @param string $fbPageId
     *
     * @return Store
     */
    public function setFbPageId($fbPageId)
    {
        $this->fbPageId = $fbPageId;

        return $this;
    }

    /**
     * Get fbPageId
     *
     * @return string
     */
    public function getFbPageId()
    {
        return $this->fbPageId;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Store
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set area
     *
     * @param \Bubblz\EntitiesBundle\Entity\Area $area
     *
     * @return Store
     */
    public function setArea(\Bubblz\EntitiesBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \Bubblz\EntitiesBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return Store
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return Store
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set marketplace
     *
     * @param \Bubblz\EntitiesBundle\Entity\Marketplace $marketplace
     *
     * @return Store
     */
    public function setMarketplace(\Bubblz\EntitiesBundle\Entity\Marketplace $marketplace = null)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return \Bubblz\EntitiesBundle\Entity\Marketplace
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set organization
     *
     * @param \Bubblz\EntitiesBundle\Entity\Organization $organization
     *
     * @return Store
     */
    public function setOrganization(\Bubblz\EntitiesBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \Bubblz\EntitiesBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return Store
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }
}
