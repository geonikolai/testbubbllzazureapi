<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contest
 *
 * @ORM\Table(name="contest", indexes={@ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="store_id_end_date", columns={"store_id", "end_date"}), @ORM\Index(name="store_id_status", columns={"store_id", "status"}), @ORM\Index(name="brand_contest_id", columns={"brand_contest_id"})})
 * @ORM\Entity
 */
class Contest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="string", length=255, nullable=false)
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unattended", type="boolean", nullable=false)
     */
    private $unattended = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="remote", type="boolean", nullable=false)
     */
    private $remote = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="winners", type="integer", nullable=false)
     */
    private $winners;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="entries", type="integer", nullable=false)
     */
    private $entries = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \BrandContest
     *
     * @ORM\ManyToOne(targetEntity="BrandContest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_contest_id", referencedColumnName="id")
     * })
     */
    private $brandContest;
    
    /**
     * @var string
     *
     * @ORM\Column(name="before_checkin_message", type="string", length=255, nullable=false)
     */
    private $beforeCheckinMessage;
    
    /**
     * @var string
     *
     * @ORM\Column(name="after_checkin_message", type="string", length=255, nullable=false)
     */
    private $afterCheckinMessage;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Contest
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set notification
     *
     * @param string $notification
     *
     * @return Contest
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return string
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Contest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Contest
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Contest
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Contest
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set unattended
     *
     * @param boolean $unattended
     *
     * @return Contest
     */
    public function setUnattended($unattended)
    {
        $this->unattended = $unattended;

        return $this;
    }

    /**
     * Get unattended
     *
     * @return boolean
     */
    public function getUnattended()
    {
        return $this->unattended;
    }

    /**
     * Set remote
     *
     * @param boolean $remote
     *
     * @return Contest
     */
    public function setRemote($remote)
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * Get remote
     *
     * @return boolean
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * Set winners
     *
     * @param integer $winners
     *
     * @return Contest
     */
    public function setWinners($winners)
    {
        $this->winners = $winners;

        return $this;
    }

    /**
     * Get winners
     *
     * @return integer
     */
    public function getWinners()
    {
        return $this->winners;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Contest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set entries
     *
     * @param integer $entries
     *
     * @return Contest
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;

        return $this;
    }

    /**
     * Get entries
     *
     * @return integer
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Contest
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Contest
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set brandContest
     *
     * @param \Bubblz\EntitiesBundle\Entity\BrandContest $brandContest
     *
     * @return Contest
     */
    public function setBrandContest(\Bubblz\EntitiesBundle\Entity\BrandContest $brandContest = null)
    {
        $this->brandContest = $brandContest;

        return $this;
    }

    /**
     * Get brandContest
     *
     * @return \Bubblz\EntitiesBundle\Entity\BrandContest
     */
    public function getBrandContest()
    {
        return $this->brandContest;
    }

    /**
     * Set beforeCheckinMessage
     *
     * @param string $beforeCheckinMessage
     *
     * @return Contest
     */
    public function setBeforeCheckinMessage($beforeCheckinMessage)
    {
        $this->beforeCheckinMessage = $beforeCheckinMessage;

        return $this;
    }

    /**
     * Get beforeCheckinMessage
     *
     * @return string
     */
    public function getBeforeCheckinMessage()
    {
        return $this->beforeCheckinMessage;
    }

    /**
     * Set afterCheckinMessage
     *
     * @param string $afterCheckinMessage
     *
     * @return Contest
     */
    public function setAfterCheckinMessage($afterCheckinMessage)
    {
        $this->afterCheckinMessage = $afterCheckinMessage;

        return $this;
    }

    /**
     * Get afterCheckinMessage
     *
     * @return string
     */
    public function getAfterCheckinMessage()
    {
        return $this->afterCheckinMessage;
    }
}
