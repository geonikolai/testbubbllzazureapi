<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListsDetails
 *
 * @ORM\Table(name="lists_details", indexes={@ORM\Index(name="lists_id", columns={"lists_id", "store_id"}), @ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="IDX_1CC6A8209D26499B", columns={"lists_id"})})
 * @ORM\Entity
 */
class ListsDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Lists
     *
     * @ORM\ManyToOne(targetEntity="Lists")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lists_id", referencedColumnName="id")
     * })
     */
    private $lists;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lists
     *
     * @param \Bubblz\EntitiesBundle\Entity\Lists $lists
     *
     * @return ListsDetails
     */
    public function setLists(\Bubblz\EntitiesBundle\Entity\Lists $lists = null)
    {
        $this->lists = $lists;

        return $this;
    }

    /**
     * Get lists
     *
     * @return \Bubblz\EntitiesBundle\Entity\Lists
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return ListsDetails
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
