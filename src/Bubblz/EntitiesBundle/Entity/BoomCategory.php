<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoomCategory
 *
 * @ORM\Table(name="boom_category")
 * @ORM\Entity
 */
class BoomCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="parent", type="string", length=45, nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=45, nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_tags", type="string", length=45, nullable=false)
     */
    private $hashTags;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BoomCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return BoomCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return BoomCategory
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set hashTags
     *
     * @param string $hashTags
     *
     * @return BoomCategory
     */
    public function setHashTags($hashTags)
    {
        $this->hashTags = $hashTags;

        return $this;
    }

    /**
     * Get hashTags
     *
     * @return string
     */
    public function getHashTags()
    {
        return $this->hashTags;
    }
}
