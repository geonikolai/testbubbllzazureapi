<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KernelUser
 *
 * @ORM\Table(name="kernel_user")
 * @ORM\Entity
 */
class KernelUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="default_message", type="string", length=500, nullable=true)
     */
    private $defaultMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="store_kernel_message", type="string", length=500, nullable=false)
     */
    private $storeKernelMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64, nullable=false)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set defaultMessage
     *
     * @param string $defaultMessage
     *
     * @return KernelUser
     */
    public function setDefaultMessage($defaultMessage)
    {
        $this->defaultMessage = $defaultMessage;

        return $this;
    }

    /**
     * Get defaultMessage
     *
     * @return string
     */
    public function getDefaultMessage()
    {
        return $this->defaultMessage;
    }

    /**
     * Set storeKernelMessage
     *
     * @param string $storeKernelMessage
     *
     * @return KernelUser
     */
    public function setStoreKernelMessage($storeKernelMessage)
    {
        $this->storeKernelMessage = $storeKernelMessage;

        return $this;
    }

    /**
     * Get storeKernelMessage
     *
     * @return string
     */
    public function getStoreKernelMessage()
    {
        return $this->storeKernelMessage;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return KernelUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return KernelUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return KernelUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
