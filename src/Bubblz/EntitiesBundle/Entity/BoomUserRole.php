<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoomUserRole
 *
 * @ORM\Table(name="boom_user_role")
 * @ORM\Entity
 */
class BoomUserRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="min_followers", type="string", length=45, nullable=true)
     */
    private $minFollowers;

    /**
     * @var string
     *
     * @ORM\Column(name="min_boom", type="string", length=45, nullable=true)
     */
    private $minBoom;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BoomUserRole
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set minFollowers
     *
     * @param string $minFollowers
     *
     * @return BoomUserRole
     */
    public function setMinFollowers($minFollowers)
    {
        $this->minFollowers = $minFollowers;

        return $this;
    }

    /**
     * Get minFollowers
     *
     * @return string
     */
    public function getMinFollowers()
    {
        return $this->minFollowers;
    }

    /**
     * Set minBoom
     *
     * @param string $minBoom
     *
     * @return BoomUserRole
     */
    public function setMinBoom($minBoom)
    {
        $this->minBoom = $minBoom;

        return $this;
    }

    /**
     * Get minBoom
     *
     * @return string
     */
    public function getMinBoom()
    {
        return $this->minBoom;
    }
}
