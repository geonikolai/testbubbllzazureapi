<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BrandContest
 *
 * @ORM\Table(name="brand_contest", indexes={@ORM\Index(name="brand_id", columns={"brand_id"}), @ORM\Index(name="lists_id", columns={"lists_id"})})
 * @ORM\Entity
 */
class BrandContest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="notification", type="string", length=255, nullable=false)
     */
    private $notification;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="before_checkin_message", type="string", length=100, nullable=true)
     */
    private $beforeCheckinMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="after_checkin_message", type="string", length=100, nullable=true)
     */
    private $afterCheckinMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unattended", type="boolean", nullable=false)
     */
    private $unattended = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="remote", type="boolean", nullable=false)
     */
    private $remote = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="winners", type="integer", nullable=false)
     */
    private $winners = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="entries", type="integer", nullable=false)
     */
    private $entries = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Lists
     *
     * @ORM\ManyToOne(targetEntity="Lists")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lists_id", referencedColumnName="id")
     * })
     */
    private $lists;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BrandContest
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set notification
     *
     * @param string $notification
     *
     * @return BrandContest
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return string
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BrandContest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set beforeCheckinMessage
     *
     * @param string $beforeCheckinMessage
     *
     * @return BrandContest
     */
    public function setBeforeCheckinMessage($beforeCheckinMessage)
    {
        $this->beforeCheckinMessage = $beforeCheckinMessage;

        return $this;
    }

    /**
     * Get beforeCheckinMessage
     *
     * @return string
     */
    public function getBeforeCheckinMessage()
    {
        return $this->beforeCheckinMessage;
    }

    /**
     * Set afterCheckinMessage
     *
     * @param string $afterCheckinMessage
     *
     * @return BrandContest
     */
    public function setAfterCheckinMessage($afterCheckinMessage)
    {
        $this->afterCheckinMessage = $afterCheckinMessage;

        return $this;
    }

    /**
     * Get afterCheckinMessage
     *
     * @return string
     */
    public function getAfterCheckinMessage()
    {
        return $this->afterCheckinMessage;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return BrandContest
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return BrandContest
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return BrandContest
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set unattended
     *
     * @param boolean $unattended
     *
     * @return BrandContest
     */
    public function setUnattended($unattended)
    {
        $this->unattended = $unattended;

        return $this;
    }

    /**
     * Get unattended
     *
     * @return boolean
     */
    public function getUnattended()
    {
        return $this->unattended;
    }

    /**
     * Set remote
     *
     * @param boolean $remote
     *
     * @return BrandContest
     */
    public function setRemote($remote)
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * Get remote
     *
     * @return boolean
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * Set winners
     *
     * @param integer $winners
     *
     * @return BrandContest
     */
    public function setWinners($winners)
    {
        $this->winners = $winners;

        return $this;
    }

    /**
     * Get winners
     *
     * @return integer
     */
    public function getWinners()
    {
        return $this->winners;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BrandContest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set entries
     *
     * @param integer $entries
     *
     * @return BrandContest
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;

        return $this;
    }

    /**
     * Get entries
     *
     * @return integer
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return BrandContest
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lists
     *
     * @param \Bubblz\EntitiesBundle\Entity\Lists $lists
     *
     * @return BrandContest
     */
    public function setLists(\Bubblz\EntitiesBundle\Entity\Lists $lists = null)
    {
        $this->lists = $lists;

        return $this;
    }

    /**
     * Get lists
     *
     * @return \Bubblz\EntitiesBundle\Entity\Lists
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return BrandContest
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
