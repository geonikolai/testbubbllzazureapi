<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brand
 *
 * @ORM\Table(name="brand", indexes={@ORM\Index(name="fk_brand_country1_idx", columns={"country_id"}), @ORM\Index(name="fk_brand_city1_idx", columns={"city_id"}), @ORM\Index(name="fk_brand_state_union1_idx", columns={"state_id"})})
 * @ORM\Entity
 */
class Brand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="brandname", type="string", length=45, nullable=false)
     */
    private $brandname;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=60, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=60, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=100, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="vat", type="string", length=10, nullable=true)
     */
    private $vat;

    /**
     * @var string
     *
     * @ORM\Column(name="doy", type="string", length=100, nullable=true)
     */
    private $doy;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_of", type="integer", nullable=true)
     */
    private $categoryOf;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_tags", type="string", length=255, nullable=true)
     */
    private $hashTags;

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=100, nullable=true)
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_image", type="string", length=100, nullable=true)
     */
    private $logoImage;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=13, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone2", type="string", length=13, nullable=true)
     */
    private $telephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=9, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=9, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Brand
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set brandname
     *
     * @param string $brandname
     *
     * @return Brand
     */
    public function setBrandname($brandname)
    {
        $this->brandname = $brandname;

        return $this;
    }

    /**
     * Get brandname
     *
     * @return string
     */
    public function getBrandname()
    {
        return $this->brandname;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Brand
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Brand
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Brand
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set vat
     *
     * @param string $vat
     *
     * @return Brand
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat
     *
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set doy
     *
     * @param string $doy
     *
     * @return Brand
     */
    public function setDoy($doy)
    {
        $this->doy = $doy;

        return $this;
    }

    /**
     * Get doy
     *
     * @return string
     */
    public function getDoy()
    {
        return $this->doy;
    }

    /**
     * Set categoryOf
     *
     * @param integer $categoryOf
     *
     * @return Brand
     */
    public function setCategoryOf($categoryOf)
    {
        $this->categoryOf = $categoryOf;

        return $this;
    }

    /**
     * Get categoryOf
     *
     * @return integer
     */
    public function getCategoryOf()
    {
        return $this->categoryOf;
    }

    /**
     * Set hashTags
     *
     * @param string $hashTags
     *
     * @return Brand
     */
    public function setHashTags($hashTags)
    {
        $this->hashTags = $hashTags;

        return $this;
    }

    /**
     * Get hashTags
     *
     * @return string
     */
    public function getHashTags()
    {
        return $this->hashTags;
    }

    /**
     * Set mainImage
     *
     * @param string $mainImage
     *
     * @return Brand
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Get mainImage
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set logoImage
     *
     * @param string $logoImage
     *
     * @return Brand
     */
    public function setLogoImage($logoImage)
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    /**
     * Get logoImage
     *
     * @return string
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Brand
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Brand
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     *
     * @return Brand
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Brand
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Brand
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Brand
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Brand
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Brand
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return Brand
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return Brand
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return Brand
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
