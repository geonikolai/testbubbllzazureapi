<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessageAgeRange
 *
 * @ORM\Table(name="message_age_range")
 * @ORM\Entity
 */
class MessageAgeRange
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="min", type="boolean", nullable=true)
     */
    private $min;

    /**
     * @var boolean
     *
     * @ORM\Column(name="max", type="boolean", nullable=true)
     */
    private $max;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set min
     *
     * @param boolean $min
     *
     * @return MessageAgeRange
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return boolean
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max
     *
     * @param boolean $max
     *
     * @return MessageAgeRange
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return boolean
     */
    public function getMax()
    {
        return $this->max;
    }
}
