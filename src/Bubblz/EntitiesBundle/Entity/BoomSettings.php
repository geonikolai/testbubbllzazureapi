<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoomSettings
 *
 * @ORM\Table(name="boom_settings")
 * @ORM\Entity
 */
class BoomSettings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_message", type="string", length=200, nullable=false)
     */
    private $mobileMessage;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mobileMessage
     *
     * @param string $mobileMessage
     *
     * @return BoomSettings
     */
    public function setMobileMessage($mobileMessage)
    {
        $this->mobileMessage = $mobileMessage;

        return $this;
    }

    /**
     * Get mobileMessage
     *
     * @return string
     */
    public function getMobileMessage()
    {
        return $this->mobileMessage;
    }
}
