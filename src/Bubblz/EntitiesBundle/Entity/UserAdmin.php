<?php

namespace Bubblz\EntitiesBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserAdmin
 *
 * @ORM\Table(name="user_admin", indexes={@ORM\Index(name="fk_user_store_store1_idx", columns={"address_id"}), @ORM\Index(name="fk_user_admin_organization1_idx", columns={"organization_id"}), @ORM\Index(name="fk_user_admin_brand1_idx", columns={"brand_id"}), @ORM\Index(name="fk_user_admin_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class UserAdmin extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     */
    private $telephone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_booms_approve", type="boolean", nullable=true)
     */
    private $permitBoomsApprove;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permit_store_address_modify", type="boolean", nullable=true)
     */
    private $permitStoreAddressModify;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="status_data", type="string", length=64, nullable=true)
     */
    private $statusData;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_code", type="string", length=10, nullable=true)
     */
    private $partnerCode;

    /**
     * @var \Organization
     *
     * @ORM\ManyToOne(targetEntity="Organization")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * })
     */
    private $organization;

    /**
     * @var \Address
     *
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * })
     */
    private $address;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return UserAdmin
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return UserAdmin
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return UserAdmin
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return UserAdmin
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set permitBoomsApprove
     *
     * @param boolean $permitBoomsApprove
     *
     * @return UserAdmin
     */
    public function setPermitBoomsApprove($permitBoomsApprove)
    {
        $this->permitBoomsApprove = $permitBoomsApprove;

        return $this;
    }

    /**
     * Get permitBoomsApprove
     *
     * @return boolean
     */
    public function getPermitBoomsApprove()
    {
        return $this->permitBoomsApprove;
    }

    /**
     * Set permitStoreAddressModify
     *
     * @param boolean $permitStoreAddressModify
     *
     * @return UserAdmin
     */
    public function setPermitStoreAddressModify($permitStoreAddressModify)
    {
        $this->permitStoreAddressModify = $permitStoreAddressModify;

        return $this;
    }

    /**
     * Get permitStoreAddressModify
     *
     * @return boolean
     */
    public function getPermitStoreAddressModify()
    {
        return $this->permitStoreAddressModify;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return UserAdmin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusData
     *
     * @param string $statusData
     *
     * @return UserAdmin
     */
    public function setStatusData($statusData)
    {
        $this->statusData = $statusData;

        return $this;
    }

    /**
     * Get statusData
     *
     * @return string
     */
    public function getStatusData()
    {
        return $this->statusData;
    }

    /**
     * Set partnerCode
     *
     * @param string $partnerCode
     *
     * @return UserAdmin
     */
    public function setPartnerCode($partnerCode)
    {
        $this->partnerCode = $partnerCode;

        return $this;
    }

    /**
     * Get partnerCode
     *
     * @return string
     */
    public function getPartnerCode()
    {
        return $this->partnerCode;
    }

    /**
     * Set organization
     *
     * @param \Bubblz\EntitiesBundle\Entity\Organization $organization
     *
     * @return UserAdmin
     */
    public function setOrganization(\Bubblz\EntitiesBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \Bubblz\EntitiesBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set address
     *
     * @param \Bubblz\EntitiesBundle\Entity\Address $address
     *
     * @return UserAdmin
     */
    public function setAddress(\Bubblz\EntitiesBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Bubblz\EntitiesBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return UserAdmin
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return UserAdmin
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
