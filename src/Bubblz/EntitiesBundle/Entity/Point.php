<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Point
 *
 * @ORM\Table(name="point", indexes={@ORM\Index(name="fk_point_boom1_idx", columns={"boom_id"})})
 * @ORM\Entity
 */
class Point
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_user_id", type="integer", nullable=true)
     */
    private $fromUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="point_value", type="integer", nullable=true)
     */
    private $pointValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="point_type", type="boolean", nullable=true)
     */
    private $pointType;

    /**
     * @var \Boom
     *
     * @ORM\ManyToOne(targetEntity="Boom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_id", referencedColumnName="id")
     * })
     */
    private $boom;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Point
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set fromUserId
     *
     * @param integer $fromUserId
     *
     * @return Point
     */
    public function setFromUserId($fromUserId)
    {
        $this->fromUserId = $fromUserId;

        return $this;
    }

    /**
     * Get fromUserId
     *
     * @return integer
     */
    public function getFromUserId()
    {
        return $this->fromUserId;
    }

    /**
     * Set pointValue
     *
     * @param integer $pointValue
     *
     * @return Point
     */
    public function setPointValue($pointValue)
    {
        $this->pointValue = $pointValue;

        return $this;
    }

    /**
     * Get pointValue
     *
     * @return integer
     */
    public function getPointValue()
    {
        return $this->pointValue;
    }

    /**
     * Set pointType
     *
     * @param boolean $pointType
     *
     * @return Point
     */
    public function setPointType($pointType)
    {
        $this->pointType = $pointType;

        return $this;
    }

    /**
     * Get pointType
     *
     * @return boolean
     */
    public function getPointType()
    {
        return $this->pointType;
    }

    /**
     * Set boom
     *
     * @param \Bubblz\EntitiesBundle\Entity\Boom $boom
     *
     * @return Point
     */
    public function setBoom(\Bubblz\EntitiesBundle\Entity\Boom $boom = null)
    {
        $this->boom = $boom;

        return $this;
    }

    /**
     * Get boom
     *
     * @return \Bubblz\EntitiesBundle\Entity\Boom
     */
    public function getBoom()
    {
        return $this->boom;
    }
}
