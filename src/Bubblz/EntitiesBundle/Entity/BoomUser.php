<?php

namespace Bubblz\EntitiesBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * BoomUser
 *
 * @ORM\Table(name="boom_user", indexes={@ORM\Index(name="fk_user_country_idx", columns={"country_id"}), @ORM\Index(name="fk_user_city1_idx", columns={"city_id"}), @ORM\Index(name="fk_user_state1_idx", columns={"state_id"})})
 * @ORM\Entity
 */
class BoomUser extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="superboomuser_name", type="string", length=100, nullable=true)
     */
    private $superboomuserName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=32, nullable=true)
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth", type="datetime", nullable=true)
     */
    private $birth;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_age", type="integer", nullable=true)
     */
    private $minAge;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_age", type="integer", nullable=true)
     */
    private $maxAge;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=15, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_phone", type="string", length=20, nullable=true)
     */
    private $mobilePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=60, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=45, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=45, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image", type="string", length=255, nullable=true)
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image_file", type="string", length=255, nullable=true)
     */
    private $profileImageFile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_superboomuser", type="boolean", nullable=true)
     */
    private $isSuperboomuser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="status_data", type="string", length=64, nullable=true)
     */
    private $statusData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registration_date", type="datetime", nullable=true)
     */
    private $registrationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login_date", type="datetime", nullable=true)
     */
    private $lastLoginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified_date", type="datetime", nullable=true)
     */
    private $lastModifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_booms", type="integer", nullable=true)
     */
    private $countBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes", type="integer", nullable=true)
     */
    private $fbLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes", type="integer", nullable=true)
     */
    private $twLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes", type="integer", nullable=true)
     */
    private $instLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes", type="integer", nullable=true)
     */
    private $pinterLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_likes", type="integer", nullable=true)
     */
    private $boomLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_counts", type="integer", nullable=true)
     */
    private $couponCounts;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_booms", type="integer", nullable=true)
     */
    private $fbBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_booms", type="integer", nullable=true)
     */
    private $twBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_booms", type="integer", nullable=true)
     */
    private $instBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_booms", type="integer", nullable=true)
     */
    private $pinterBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_views", type="integer", nullable=true)
     */
    private $fbViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_views", type="integer", nullable=true)
     */
    private $twViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_views", type="integer", nullable=true)
     */
    private $instViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_views", type="integer", nullable=true)
     */
    private $pinterViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares", type="integer", nullable=true)
     */
    private $fbShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares", type="integer", nullable=true)
     */
    private $twShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares", type="integer", nullable=true)
     */
    private $instShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares", type="integer", nullable=true)
     */
    private $pinterShares;

    /**
     * @var float
     *
     * @ORM\Column(name="total_causes", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_causes", type="integer", nullable=true)
     */
    private $countCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_points", type="integer", nullable=true)
     */
    private $loyaltyPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="total_boom_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalBoomDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_boom_discount", type="integer", nullable=true)
     */
    private $countBoomDiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_coupon_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCouponDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_coupon_discount", type="integer", nullable=true)
     */
    private $countCouponDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_id", type="string", length=100, nullable=true)
     */
    private $fbId;

    /**
     * @var string
     *
     * @ORM\Column(name="os_version", type="string", length=64, nullable=true)
     */
    private $osVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="device_model", type="string", length=64, nullable=true)
     */
    private $deviceModel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=100, nullable=true)
     */
    private $fbAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=64, nullable=true)
     */
    private $manufacturer;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return BoomUser
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return BoomUser
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return BoomUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return BoomUser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set superboomuserName
     *
     * @param string $superboomuserName
     *
     * @return BoomUser
     */
    public function setSuperboomuserName($superboomuserName)
    {
        $this->superboomuserName = $superboomuserName;

        return $this;
    }

    /**
     * Get superboomuserName
     *
     * @return string
     */
    public function getSuperboomuserName()
    {
        return $this->superboomuserName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return BoomUser
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return BoomUser
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set minAge
     *
     * @param integer $minAge
     *
     * @return BoomUser
     */
    public function setMinAge($minAge)
    {
        $this->minAge = $minAge;

        return $this;
    }

    /**
     * Get minAge
     *
     * @return integer
     */
    public function getMinAge()
    {
        return $this->minAge;
    }

    /**
     * Set maxAge
     *
     * @param integer $maxAge
     *
     * @return BoomUser
     */
    public function setMaxAge($maxAge)
    {
        $this->maxAge = $maxAge;

        return $this;
    }

    /**
     * Get maxAge
     *
     * @return integer
     */
    public function getMaxAge()
    {
        return $this->maxAge;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return BoomUser
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return BoomUser
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return BoomUser
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return BoomUser
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return BoomUser
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return BoomUser
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set profileImage
     *
     * @param string $profileImage
     *
     * @return BoomUser
     */
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get profileImage
     *
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set profileImageFile
     *
     * @param string $profileImageFile
     *
     * @return BoomUser
     */
    public function setProfileImageFile($profileImageFile)
    {
        $this->profileImageFile = $profileImageFile;

        return $this;
    }

    /**
     * Get profileImageFile
     *
     * @return string
     */
    public function getProfileImageFile()
    {
        return $this->profileImageFile;
    }

    /**
     * Set isSuperboomuser
     *
     * @param boolean $isSuperboomuser
     *
     * @return BoomUser
     */
    public function setIsSuperboomuser($isSuperboomuser)
    {
        $this->isSuperboomuser = $isSuperboomuser;

        return $this;
    }

    /**
     * Get isSuperboomuser
     *
     * @return boolean
     */
    public function getIsSuperboomuser()
    {
        return $this->isSuperboomuser;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return BoomUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusData
     *
     * @param string $statusData
     *
     * @return BoomUser
     */
    public function setStatusData($statusData)
    {
        $this->statusData = $statusData;

        return $this;
    }

    /**
     * Get statusData
     *
     * @return string
     */
    public function getStatusData()
    {
        return $this->statusData;
    }

    /**
     * Set registrationDate
     *
     * @param \DateTime $registrationDate
     *
     * @return BoomUser
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return \DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * Set lastLoginDate
     *
     * @param \DateTime $lastLoginDate
     *
     * @return BoomUser
     */
    public function setLastLoginDate($lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;

        return $this;
    }

    /**
     * Get lastLoginDate
     *
     * @return \DateTime
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * Set lastModifiedDate
     *
     * @param \DateTime $lastModifiedDate
     *
     * @return BoomUser
     */
    public function setLastModifiedDate($lastModifiedDate)
    {
        $this->lastModifiedDate = $lastModifiedDate;

        return $this;
    }

    /**
     * Get lastModifiedDate
     *
     * @return \DateTime
     */
    public function getLastModifiedDate()
    {
        return $this->lastModifiedDate;
    }

    /**
     * Set countBooms
     *
     * @param integer $countBooms
     *
     * @return BoomUser
     */
    public function setCountBooms($countBooms)
    {
        $this->countBooms = $countBooms;

        return $this;
    }

    /**
     * Get countBooms
     *
     * @return integer
     */
    public function getCountBooms()
    {
        return $this->countBooms;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return BoomUser
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set fbLikes
     *
     * @param integer $fbLikes
     *
     * @return BoomUser
     */
    public function setFbLikes($fbLikes)
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    /**
     * Get fbLikes
     *
     * @return integer
     */
    public function getFbLikes()
    {
        return $this->fbLikes;
    }

    /**
     * Set twLikes
     *
     * @param integer $twLikes
     *
     * @return BoomUser
     */
    public function setTwLikes($twLikes)
    {
        $this->twLikes = $twLikes;

        return $this;
    }

    /**
     * Get twLikes
     *
     * @return integer
     */
    public function getTwLikes()
    {
        return $this->twLikes;
    }

    /**
     * Set instLikes
     *
     * @param integer $instLikes
     *
     * @return BoomUser
     */
    public function setInstLikes($instLikes)
    {
        $this->instLikes = $instLikes;

        return $this;
    }

    /**
     * Get instLikes
     *
     * @return integer
     */
    public function getInstLikes()
    {
        return $this->instLikes;
    }

    /**
     * Set pinterLikes
     *
     * @param integer $pinterLikes
     *
     * @return BoomUser
     */
    public function setPinterLikes($pinterLikes)
    {
        $this->pinterLikes = $pinterLikes;

        return $this;
    }

    /**
     * Get pinterLikes
     *
     * @return integer
     */
    public function getPinterLikes()
    {
        return $this->pinterLikes;
    }

    /**
     * Set boomLikes
     *
     * @param integer $boomLikes
     *
     * @return BoomUser
     */
    public function setBoomLikes($boomLikes)
    {
        $this->boomLikes = $boomLikes;

        return $this;
    }

    /**
     * Get boomLikes
     *
     * @return integer
     */
    public function getBoomLikes()
    {
        return $this->boomLikes;
    }

    /**
     * Set couponCounts
     *
     * @param integer $couponCounts
     *
     * @return BoomUser
     */
    public function setCouponCounts($couponCounts)
    {
        $this->couponCounts = $couponCounts;

        return $this;
    }

    /**
     * Get couponCounts
     *
     * @return integer
     */
    public function getCouponCounts()
    {
        return $this->couponCounts;
    }

    /**
     * Set fbBooms
     *
     * @param integer $fbBooms
     *
     * @return BoomUser
     */
    public function setFbBooms($fbBooms)
    {
        $this->fbBooms = $fbBooms;

        return $this;
    }

    /**
     * Get fbBooms
     *
     * @return integer
     */
    public function getFbBooms()
    {
        return $this->fbBooms;
    }

    /**
     * Set twBooms
     *
     * @param integer $twBooms
     *
     * @return BoomUser
     */
    public function setTwBooms($twBooms)
    {
        $this->twBooms = $twBooms;

        return $this;
    }

    /**
     * Get twBooms
     *
     * @return integer
     */
    public function getTwBooms()
    {
        return $this->twBooms;
    }

    /**
     * Set instBooms
     *
     * @param integer $instBooms
     *
     * @return BoomUser
     */
    public function setInstBooms($instBooms)
    {
        $this->instBooms = $instBooms;

        return $this;
    }

    /**
     * Get instBooms
     *
     * @return integer
     */
    public function getInstBooms()
    {
        return $this->instBooms;
    }

    /**
     * Set pinterBooms
     *
     * @param integer $pinterBooms
     *
     * @return BoomUser
     */
    public function setPinterBooms($pinterBooms)
    {
        $this->pinterBooms = $pinterBooms;

        return $this;
    }

    /**
     * Get pinterBooms
     *
     * @return integer
     */
    public function getPinterBooms()
    {
        return $this->pinterBooms;
    }

    /**
     * Set fbViews
     *
     * @param integer $fbViews
     *
     * @return BoomUser
     */
    public function setFbViews($fbViews)
    {
        $this->fbViews = $fbViews;

        return $this;
    }

    /**
     * Get fbViews
     *
     * @return integer
     */
    public function getFbViews()
    {
        return $this->fbViews;
    }

    /**
     * Set twViews
     *
     * @param integer $twViews
     *
     * @return BoomUser
     */
    public function setTwViews($twViews)
    {
        $this->twViews = $twViews;

        return $this;
    }

    /**
     * Get twViews
     *
     * @return integer
     */
    public function getTwViews()
    {
        return $this->twViews;
    }

    /**
     * Set instViews
     *
     * @param integer $instViews
     *
     * @return BoomUser
     */
    public function setInstViews($instViews)
    {
        $this->instViews = $instViews;

        return $this;
    }

    /**
     * Get instViews
     *
     * @return integer
     */
    public function getInstViews()
    {
        return $this->instViews;
    }

    /**
     * Set pinterViews
     *
     * @param integer $pinterViews
     *
     * @return BoomUser
     */
    public function setPinterViews($pinterViews)
    {
        $this->pinterViews = $pinterViews;

        return $this;
    }

    /**
     * Get pinterViews
     *
     * @return integer
     */
    public function getPinterViews()
    {
        return $this->pinterViews;
    }

    /**
     * Set fbShares
     *
     * @param integer $fbShares
     *
     * @return BoomUser
     */
    public function setFbShares($fbShares)
    {
        $this->fbShares = $fbShares;

        return $this;
    }

    /**
     * Get fbShares
     *
     * @return integer
     */
    public function getFbShares()
    {
        return $this->fbShares;
    }

    /**
     * Set twShares
     *
     * @param integer $twShares
     *
     * @return BoomUser
     */
    public function setTwShares($twShares)
    {
        $this->twShares = $twShares;

        return $this;
    }

    /**
     * Get twShares
     *
     * @return integer
     */
    public function getTwShares()
    {
        return $this->twShares;
    }

    /**
     * Set instShares
     *
     * @param integer $instShares
     *
     * @return BoomUser
     */
    public function setInstShares($instShares)
    {
        $this->instShares = $instShares;

        return $this;
    }

    /**
     * Get instShares
     *
     * @return integer
     */
    public function getInstShares()
    {
        return $this->instShares;
    }

    /**
     * Set pinterShares
     *
     * @param integer $pinterShares
     *
     * @return BoomUser
     */
    public function setPinterShares($pinterShares)
    {
        $this->pinterShares = $pinterShares;

        return $this;
    }

    /**
     * Get pinterShares
     *
     * @return integer
     */
    public function getPinterShares()
    {
        return $this->pinterShares;
    }

    /**
     * Set totalCauses
     *
     * @param float $totalCauses
     *
     * @return BoomUser
     */
    public function setTotalCauses($totalCauses)
    {
        $this->totalCauses = $totalCauses;

        return $this;
    }

    /**
     * Get totalCauses
     *
     * @return float
     */
    public function getTotalCauses()
    {
        return $this->totalCauses;
    }

    /**
     * Set countCauses
     *
     * @param integer $countCauses
     *
     * @return BoomUser
     */
    public function setCountCauses($countCauses)
    {
        $this->countCauses = $countCauses;

        return $this;
    }

    /**
     * Get countCauses
     *
     * @return integer
     */
    public function getCountCauses()
    {
        return $this->countCauses;
    }

    /**
     * Set loyaltyPoints
     *
     * @param integer $loyaltyPoints
     *
     * @return BoomUser
     */
    public function setLoyaltyPoints($loyaltyPoints)
    {
        $this->loyaltyPoints = $loyaltyPoints;

        return $this;
    }

    /**
     * Get loyaltyPoints
     *
     * @return integer
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Set totalBoomDiscount
     *
     * @param float $totalBoomDiscount
     *
     * @return BoomUser
     */
    public function setTotalBoomDiscount($totalBoomDiscount)
    {
        $this->totalBoomDiscount = $totalBoomDiscount;

        return $this;
    }

    /**
     * Get totalBoomDiscount
     *
     * @return float
     */
    public function getTotalBoomDiscount()
    {
        return $this->totalBoomDiscount;
    }

    /**
     * Set countBoomDiscount
     *
     * @param integer $countBoomDiscount
     *
     * @return BoomUser
     */
    public function setCountBoomDiscount($countBoomDiscount)
    {
        $this->countBoomDiscount = $countBoomDiscount;

        return $this;
    }

    /**
     * Get countBoomDiscount
     *
     * @return integer
     */
    public function getCountBoomDiscount()
    {
        return $this->countBoomDiscount;
    }

    /**
     * Set totalCouponDiscount
     *
     * @param float $totalCouponDiscount
     *
     * @return BoomUser
     */
    public function setTotalCouponDiscount($totalCouponDiscount)
    {
        $this->totalCouponDiscount = $totalCouponDiscount;

        return $this;
    }

    /**
     * Get totalCouponDiscount
     *
     * @return float
     */
    public function getTotalCouponDiscount()
    {
        return $this->totalCouponDiscount;
    }

    /**
     * Set countCouponDiscount
     *
     * @param integer $countCouponDiscount
     *
     * @return BoomUser
     */
    public function setCountCouponDiscount($countCouponDiscount)
    {
        $this->countCouponDiscount = $countCouponDiscount;

        return $this;
    }

    /**
     * Get countCouponDiscount
     *
     * @return integer
     */
    public function getCountCouponDiscount()
    {
        return $this->countCouponDiscount;
    }

    /**
     * Set fbId
     *
     * @param string $fbId
     *
     * @return BoomUser
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;

        return $this;
    }

    /**
     * Get fbId
     *
     * @return string
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set osVersion
     *
     * @param string $osVersion
     *
     * @return BoomUser
     */
    public function setOsVersion($osVersion)
    {
        $this->osVersion = $osVersion;

        return $this;
    }

    /**
     * Get osVersion
     *
     * @return string
     */
    public function getOsVersion()
    {
        return $this->osVersion;
    }

    /**
     * Set deviceModel
     *
     * @param string $deviceModel
     *
     * @return BoomUser
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;

        return $this;
    }

    /**
     * Get deviceModel
     *
     * @return string
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }
    
    /**
     * Set fbAccessToken
     *
     * @param string $fbAccessToken
     *
     * @return string
     */
    public function setFbAccessToken($fbAccessToken)
    {
        $this->fbAccessToken = $fbAccessToken;

        return $this;
    }

    /**
     * Get fbAccessToken
     *
     * @return string
     */
    public function getFbAccessToken()
    {
        return $this->fbAccessToken;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return BoomUser
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return BoomUser
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return BoomUser
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return BoomUser
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }
}
