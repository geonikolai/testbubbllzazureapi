<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatabaseVersion
 *
 * @ORM\Table(name="database_version")
 * @ORM\Entity
 */
class DatabaseVersion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="database_v", type="string", length=50, nullable=false)
     */
    private $databaseV;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set databaseV
     *
     * @param string $databaseV
     *
     * @return DatabaseVersion
     */
    public function setDatabaseV($databaseV)
    {
        $this->databaseV = $databaseV;

        return $this;
    }

    /**
     * Get databaseV
     *
     * @return string
     */
    public function getDatabaseV()
    {
        return $this->databaseV;
    }
}
