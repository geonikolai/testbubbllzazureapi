<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcceptCouponMarketplace
 *
 * @ORM\Table(name="accept_coupon_marketplace", indexes={@ORM\Index(name="fk_accept_coupon_marketplace_store1_idx", columns={"store_id"}), @ORM\Index(name="fk_accept_coupon_marketplace_marketplace1_idx", columns={"marketplace_id"})})
 * @ORM\Entity
 */
class AcceptCouponMarketplace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var \Marketplace
     *
     * @ORM\ManyToOne(targetEntity="Marketplace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     * })
     */
    private $marketplace;

    /**
     * @var \Address
     *
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return AcceptCouponMarketplace
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return AcceptCouponMarketplace
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AcceptCouponMarketplace
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set marketplace
     *
     * @param \Bubblz\EntitiesBundle\Entity\Marketplace $marketplace
     *
     * @return AcceptCouponMarketplace
     */
    public function setMarketplace(\Bubblz\EntitiesBundle\Entity\Marketplace $marketplace = null)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return \Bubblz\EntitiesBundle\Entity\Marketplace
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Address $store
     *
     * @return AcceptCouponMarketplace
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Address $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Address
     */
    public function getStore()
    {
        return $this->store;
    }
}
