<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BrandContestExclusions
 *
 * @ORM\Table(name="brand_contest_exclusions", indexes={@ORM\Index(name="fk_brand_contest_exclusions_boom_user_id1_idx", columns={"boom_user_id"}), @ORM\Index(name="fk_brand_contest_exclusions_brand_contest_id1_idx", columns={"brand_contest_id"})})
 * @ORM\Entity
 */
class BrandContestExclusions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \BrandContest
     *
     * @ORM\ManyToOne(targetEntity="BrandContest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_contest_id", referencedColumnName="id")
     * })
     */
    private $brandContest;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return BrandContestExclusions
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return BrandContestExclusions
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set brandContest
     *
     * @param \Bubblz\EntitiesBundle\Entity\BrandContest $brandContest
     *
     * @return BrandContestExclusions
     */
    public function setBrandContest(\Bubblz\EntitiesBundle\Entity\BrandContest $brandContest = null)
    {
        $this->brandContest = $brandContest;

        return $this;
    }

    /**
     * Get brandContest
     *
     * @return \Bubblz\EntitiesBundle\Entity\BrandContest
     */
    public function getBrandContest()
    {
        return $this->brandContest;
    }
}
