<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marketplace
 *
 * @ORM\Table(name="marketplace", indexes={@ORM\Index(name="fk_marketplace_country1_idx", columns={"country_id"}), @ORM\Index(name="fk_marketplace_city1_idx", columns={"city_id"}), @ORM\Index(name="fk_marketplace_state1_idx", columns={"state_id"}), @ORM\Index(name="fk_marketplace_area1_idx", columns={"area_id"}), @ORM\Index(name="fk_marketplace_organization1_idx", columns={"organization_id"})})
 * @ORM\Entity
 */
class Marketplace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=60, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_name", type="string", length=45, nullable=true)
     */
    private $logoName;

    /**
     * @var float
     *
     * @ORM\Column(name="longitute", type="float", precision=10, scale=0, nullable=true)
     */
    private $longitute;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="bing_map_link", type="string", length=255, nullable=true)
     */
    private $bingMapLink;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_booms", type="integer", nullable=true)
     */
    private $countBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes", type="integer", nullable=true)
     */
    private $fbLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes", type="integer", nullable=true)
     */
    private $twLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes", type="integer", nullable=true)
     */
    private $instLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes", type="integer", nullable=true)
     */
    private $pinterLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_counts", type="integer", nullable=true)
     */
    private $couponCounts;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_booms", type="integer", nullable=true)
     */
    private $fbBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_booms", type="integer", nullable=true)
     */
    private $twBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_booms", type="integer", nullable=true)
     */
    private $instBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_booms", type="integer", nullable=true)
     */
    private $pinterBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_views", type="integer", nullable=true)
     */
    private $fbViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_views", type="integer", nullable=true)
     */
    private $twViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_views", type="integer", nullable=true)
     */
    private $instViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_views", type="integer", nullable=true)
     */
    private $pinterViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares", type="integer", nullable=true)
     */
    private $fbShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares", type="integer", nullable=true)
     */
    private $twShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares", type="integer", nullable=true)
     */
    private $instShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares", type="integer", nullable=true)
     */
    private $pinterShares;

    /**
     * @var float
     *
     * @ORM\Column(name="total_causes", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_causes", type="integer", nullable=true)
     */
    private $countCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_points", type="integer", nullable=true)
     */
    private $loyaltyPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="total_boom_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalBoomDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_boom_discount", type="integer", nullable=true)
     */
    private $countBoomDiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_coupon_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCouponDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_coupon_discount", type="integer", nullable=true)
     */
    private $countCouponDiscount;

    /**
     * @var \Area
     *
     * @ORM\ManyToOne(targetEntity="Area")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     * })
     */
    private $area;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \Organization
     *
     * @ORM\ManyToOne(targetEntity="Organization")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     * })
     */
    private $organization;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Marketplace
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Marketplace
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Marketplace
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Marketplace
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Marketplace
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Marketplace
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logoName
     *
     * @param string $logoName
     *
     * @return Marketplace
     */
    public function setLogoName($logoName)
    {
        $this->logoName = $logoName;

        return $this;
    }

    /**
     * Get logoName
     *
     * @return string
     */
    public function getLogoName()
    {
        return $this->logoName;
    }

    /**
     * Set longitute
     *
     * @param float $longitute
     *
     * @return Marketplace
     */
    public function setLongitute($longitute)
    {
        $this->longitute = $longitute;

        return $this;
    }

    /**
     * Get longitute
     *
     * @return float
     */
    public function getLongitute()
    {
        return $this->longitute;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Marketplace
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set bingMapLink
     *
     * @param string $bingMapLink
     *
     * @return Marketplace
     */
    public function setBingMapLink($bingMapLink)
    {
        $this->bingMapLink = $bingMapLink;

        return $this;
    }

    /**
     * Get bingMapLink
     *
     * @return string
     */
    public function getBingMapLink()
    {
        return $this->bingMapLink;
    }

    /**
     * Set countBooms
     *
     * @param integer $countBooms
     *
     * @return Marketplace
     */
    public function setCountBooms($countBooms)
    {
        $this->countBooms = $countBooms;

        return $this;
    }

    /**
     * Get countBooms
     *
     * @return integer
     */
    public function getCountBooms()
    {
        return $this->countBooms;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Marketplace
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set fbLikes
     *
     * @param integer $fbLikes
     *
     * @return Marketplace
     */
    public function setFbLikes($fbLikes)
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    /**
     * Get fbLikes
     *
     * @return integer
     */
    public function getFbLikes()
    {
        return $this->fbLikes;
    }

    /**
     * Set twLikes
     *
     * @param integer $twLikes
     *
     * @return Marketplace
     */
    public function setTwLikes($twLikes)
    {
        $this->twLikes = $twLikes;

        return $this;
    }

    /**
     * Get twLikes
     *
     * @return integer
     */
    public function getTwLikes()
    {
        return $this->twLikes;
    }

    /**
     * Set instLikes
     *
     * @param integer $instLikes
     *
     * @return Marketplace
     */
    public function setInstLikes($instLikes)
    {
        $this->instLikes = $instLikes;

        return $this;
    }

    /**
     * Get instLikes
     *
     * @return integer
     */
    public function getInstLikes()
    {
        return $this->instLikes;
    }

    /**
     * Set pinterLikes
     *
     * @param integer $pinterLikes
     *
     * @return Marketplace
     */
    public function setPinterLikes($pinterLikes)
    {
        $this->pinterLikes = $pinterLikes;

        return $this;
    }

    /**
     * Get pinterLikes
     *
     * @return integer
     */
    public function getPinterLikes()
    {
        return $this->pinterLikes;
    }

    /**
     * Set couponCounts
     *
     * @param integer $couponCounts
     *
     * @return Marketplace
     */
    public function setCouponCounts($couponCounts)
    {
        $this->couponCounts = $couponCounts;

        return $this;
    }

    /**
     * Get couponCounts
     *
     * @return integer
     */
    public function getCouponCounts()
    {
        return $this->couponCounts;
    }

    /**
     * Set fbBooms
     *
     * @param integer $fbBooms
     *
     * @return Marketplace
     */
    public function setFbBooms($fbBooms)
    {
        $this->fbBooms = $fbBooms;

        return $this;
    }

    /**
     * Get fbBooms
     *
     * @return integer
     */
    public function getFbBooms()
    {
        return $this->fbBooms;
    }

    /**
     * Set twBooms
     *
     * @param integer $twBooms
     *
     * @return Marketplace
     */
    public function setTwBooms($twBooms)
    {
        $this->twBooms = $twBooms;

        return $this;
    }

    /**
     * Get twBooms
     *
     * @return integer
     */
    public function getTwBooms()
    {
        return $this->twBooms;
    }

    /**
     * Set instBooms
     *
     * @param integer $instBooms
     *
     * @return Marketplace
     */
    public function setInstBooms($instBooms)
    {
        $this->instBooms = $instBooms;

        return $this;
    }

    /**
     * Get instBooms
     *
     * @return integer
     */
    public function getInstBooms()
    {
        return $this->instBooms;
    }

    /**
     * Set pinterBooms
     *
     * @param integer $pinterBooms
     *
     * @return Marketplace
     */
    public function setPinterBooms($pinterBooms)
    {
        $this->pinterBooms = $pinterBooms;

        return $this;
    }

    /**
     * Get pinterBooms
     *
     * @return integer
     */
    public function getPinterBooms()
    {
        return $this->pinterBooms;
    }

    /**
     * Set fbViews
     *
     * @param integer $fbViews
     *
     * @return Marketplace
     */
    public function setFbViews($fbViews)
    {
        $this->fbViews = $fbViews;

        return $this;
    }

    /**
     * Get fbViews
     *
     * @return integer
     */
    public function getFbViews()
    {
        return $this->fbViews;
    }

    /**
     * Set twViews
     *
     * @param integer $twViews
     *
     * @return Marketplace
     */
    public function setTwViews($twViews)
    {
        $this->twViews = $twViews;

        return $this;
    }

    /**
     * Get twViews
     *
     * @return integer
     */
    public function getTwViews()
    {
        return $this->twViews;
    }

    /**
     * Set instViews
     *
     * @param integer $instViews
     *
     * @return Marketplace
     */
    public function setInstViews($instViews)
    {
        $this->instViews = $instViews;

        return $this;
    }

    /**
     * Get instViews
     *
     * @return integer
     */
    public function getInstViews()
    {
        return $this->instViews;
    }

    /**
     * Set pinterViews
     *
     * @param integer $pinterViews
     *
     * @return Marketplace
     */
    public function setPinterViews($pinterViews)
    {
        $this->pinterViews = $pinterViews;

        return $this;
    }

    /**
     * Get pinterViews
     *
     * @return integer
     */
    public function getPinterViews()
    {
        return $this->pinterViews;
    }

    /**
     * Set fbShares
     *
     * @param integer $fbShares
     *
     * @return Marketplace
     */
    public function setFbShares($fbShares)
    {
        $this->fbShares = $fbShares;

        return $this;
    }

    /**
     * Get fbShares
     *
     * @return integer
     */
    public function getFbShares()
    {
        return $this->fbShares;
    }

    /**
     * Set twShares
     *
     * @param integer $twShares
     *
     * @return Marketplace
     */
    public function setTwShares($twShares)
    {
        $this->twShares = $twShares;

        return $this;
    }

    /**
     * Get twShares
     *
     * @return integer
     */
    public function getTwShares()
    {
        return $this->twShares;
    }

    /**
     * Set instShares
     *
     * @param integer $instShares
     *
     * @return Marketplace
     */
    public function setInstShares($instShares)
    {
        $this->instShares = $instShares;

        return $this;
    }

    /**
     * Get instShares
     *
     * @return integer
     */
    public function getInstShares()
    {
        return $this->instShares;
    }

    /**
     * Set pinterShares
     *
     * @param integer $pinterShares
     *
     * @return Marketplace
     */
    public function setPinterShares($pinterShares)
    {
        $this->pinterShares = $pinterShares;

        return $this;
    }

    /**
     * Get pinterShares
     *
     * @return integer
     */
    public function getPinterShares()
    {
        return $this->pinterShares;
    }

    /**
     * Set totalCauses
     *
     * @param float $totalCauses
     *
     * @return Marketplace
     */
    public function setTotalCauses($totalCauses)
    {
        $this->totalCauses = $totalCauses;

        return $this;
    }

    /**
     * Get totalCauses
     *
     * @return float
     */
    public function getTotalCauses()
    {
        return $this->totalCauses;
    }

    /**
     * Set countCauses
     *
     * @param integer $countCauses
     *
     * @return Marketplace
     */
    public function setCountCauses($countCauses)
    {
        $this->countCauses = $countCauses;

        return $this;
    }

    /**
     * Get countCauses
     *
     * @return integer
     */
    public function getCountCauses()
    {
        return $this->countCauses;
    }

    /**
     * Set loyaltyPoints
     *
     * @param integer $loyaltyPoints
     *
     * @return Marketplace
     */
    public function setLoyaltyPoints($loyaltyPoints)
    {
        $this->loyaltyPoints = $loyaltyPoints;

        return $this;
    }

    /**
     * Get loyaltyPoints
     *
     * @return integer
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Set totalBoomDiscount
     *
     * @param float $totalBoomDiscount
     *
     * @return Marketplace
     */
    public function setTotalBoomDiscount($totalBoomDiscount)
    {
        $this->totalBoomDiscount = $totalBoomDiscount;

        return $this;
    }

    /**
     * Get totalBoomDiscount
     *
     * @return float
     */
    public function getTotalBoomDiscount()
    {
        return $this->totalBoomDiscount;
    }

    /**
     * Set countBoomDiscount
     *
     * @param integer $countBoomDiscount
     *
     * @return Marketplace
     */
    public function setCountBoomDiscount($countBoomDiscount)
    {
        $this->countBoomDiscount = $countBoomDiscount;

        return $this;
    }

    /**
     * Get countBoomDiscount
     *
     * @return integer
     */
    public function getCountBoomDiscount()
    {
        return $this->countBoomDiscount;
    }

    /**
     * Set totalCouponDiscount
     *
     * @param float $totalCouponDiscount
     *
     * @return Marketplace
     */
    public function setTotalCouponDiscount($totalCouponDiscount)
    {
        $this->totalCouponDiscount = $totalCouponDiscount;

        return $this;
    }

    /**
     * Get totalCouponDiscount
     *
     * @return float
     */
    public function getTotalCouponDiscount()
    {
        return $this->totalCouponDiscount;
    }

    /**
     * Set countCouponDiscount
     *
     * @param integer $countCouponDiscount
     *
     * @return Marketplace
     */
    public function setCountCouponDiscount($countCouponDiscount)
    {
        $this->countCouponDiscount = $countCouponDiscount;

        return $this;
    }

    /**
     * Get countCouponDiscount
     *
     * @return integer
     */
    public function getCountCouponDiscount()
    {
        return $this->countCouponDiscount;
    }

    /**
     * Set area
     *
     * @param \Bubblz\EntitiesBundle\Entity\Area $area
     *
     * @return Marketplace
     */
    public function setArea(\Bubblz\EntitiesBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \Bubblz\EntitiesBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return Marketplace
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return Marketplace
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set organization
     *
     * @param \Bubblz\EntitiesBundle\Entity\Organization $organization
     *
     * @return Marketplace
     */
    public function setOrganization(\Bubblz\EntitiesBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \Bubblz\EntitiesBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return Marketplace
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }
}
