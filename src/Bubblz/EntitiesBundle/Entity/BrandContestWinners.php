<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BrandContestWinners
 *
 * @ORM\Table(name="brand_contest_winners", indexes={@ORM\Index(name="fk_brand_contest_winners_boom1_idx", columns={"boom_id"}), @ORM\Index(name="fk_brand_contest_winners_boom_user1_idx", columns={"boom_user_id"}), @ORM\Index(name="fk_brand_contest_winners_brand_contest1_idx", columns={"brand_contest_id"}), @ORM\Index(name="fk_brand_contest_winners_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class BrandContestWinners
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="draw_date", type="datetime", nullable=false)
     */
    private $drawDate;

    /**
     * @var string
     *
     * @ORM\Column(name="prize", type="string", length=64, nullable=true)
     */
    private $prize;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \Boom
     *
     * @ORM\ManyToOne(targetEntity="Boom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_id", referencedColumnName="id")
     * })
     */
    private $boom;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \BrandContest
     *
     * @ORM\ManyToOne(targetEntity="BrandContest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_contest_id", referencedColumnName="id")
     * })
     */
    private $brandContest;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set drawDate
     *
     * @param \DateTime $drawDate
     *
     * @return BrandContestWinners
     */
    public function setDrawDate($drawDate)
    {
        $this->drawDate = $drawDate;

        return $this;
    }

    /**
     * Get drawDate
     *
     * @return \DateTime
     */
    public function getDrawDate()
    {
        return $this->drawDate;
    }

    /**
     * Set prize
     *
     * @param string $prize
     *
     * @return BrandContestWinners
     */
    public function setPrize($prize)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return string
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return BrandContestWinners
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set boom
     *
     * @param \Bubblz\EntitiesBundle\Entity\Boom $boom
     *
     * @return BrandContestWinners
     */
    public function setBoom(\Bubblz\EntitiesBundle\Entity\Boom $boom = null)
    {
        $this->boom = $boom;

        return $this;
    }

    /**
     * Get boom
     *
     * @return \Bubblz\EntitiesBundle\Entity\Boom
     */
    public function getBoom()
    {
        return $this->boom;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return BrandContestWinners
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set brandContest
     *
     * @param \Bubblz\EntitiesBundle\Entity\BrandContest $brandContest
     *
     * @return BrandContestWinners
     */
    public function setBrandContest(\Bubblz\EntitiesBundle\Entity\BrandContest $brandContest = null)
    {
        $this->brandContest = $brandContest;

        return $this;
    }

    /**
     * Get brandContest
     *
     * @return \Bubblz\EntitiesBundle\Entity\BrandContest
     */
    public function getBrandContest()
    {
        return $this->brandContest;
    }
}
