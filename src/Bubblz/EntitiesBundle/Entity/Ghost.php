<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Private
 *
 * @ORM\Table(name="ghost", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="store_id", columns={"store_id"})})
 * @ORM\Entity
 */
class Ghost
{

    /**
     * @var \Ghost
     *
     * @ORM\ManyToOne(targetEntity="Ghost")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;
    
    /**
     * @var \Ghost
     *
     * @ORM\ManyToOne(targetEntity="Ghost")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Contest
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
    
    /**
     * Set user
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $user
     *
     * @return Contest
     */
    public function setUser(\Bubblz\EntitiesBundle\Entity\BoomUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getUser()
    {
        return $this->user;
    }
    
}
