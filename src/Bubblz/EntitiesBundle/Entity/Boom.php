<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boom
 *
 * @ORM\Table(name="boom", indexes={@ORM\Index(name="fk_boom_boom_category1_idx", columns={"boom_category_id"}), @ORM\Index(name="fk_boom_boom_user1_idx", columns={"boom_user_id"}), @ORM\Index(name="fk_boom_store1_idx", columns={"store_id"}), @ORM\Index(name="fk_boom_discount1_idx", columns={"discount_id"}), @ORM\Index(name="fk_boom_coupon1_idx", columns={"store_coupon_id"}), @ORM\Index(name="fk_boom_coupon2_idx", columns={"brand_coupon_id"}), @ORM\Index(name="checkin_id", columns={"checkin_id"}), @ORM\Index(name="contest_id", columns={"contest_id"}), @ORM\Index(name="coupon_id", columns={"coupon_id"}), @ORM\Index(name="offer_id", columns={"offer_id"}), @ORM\Index(name="store_id_p_date", columns={"store_id", "p_date"}), @ORM\Index(name="boom_user_id_store_id", columns={"boom_user_id", "store_id"}), @ORM\Index(name="boom_user_id_store_id_p_date", columns={"boom_user_id", "store_id", "p_date"}), @ORM\Index(name="boom_user_id_store_id_c_date_p_date", columns={"boom_user_id", "store_id", "c_date", "p_date"}), @ORM\Index(name="fk_boom_brand1_idx", columns={"brand_id"}), @ORM\Index(name="fk_boom_brand_contest_id1_idx", columns={"brand_contest_id"})})
 * @ORM\Entity
 */
class Boom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_category_id", type="integer", nullable=false)
     */
    private $boomCategoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="string", length=45, nullable=false)
     */
    private $imageName;

    /**
     * @var string
     *
     * @ORM\Column(name="boom_user_message", type="string", length=255, nullable=true)
     */
    private $boomUserMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="p_date", type="datetime", nullable=true)
     */
    private $pDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes", type="integer", nullable=true)
     */
    private $fbLikes;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fb_love", type="integer", nullable=true)
     */
    private $fbLove;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fb_haha", type="integer", nullable=true)
     */
    private $fbHaha;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fb_wow", type="integer", nullable=true)
     */
    private $fbWow;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fb_sad", type="integer", nullable=true)
     */
    private $fbSad;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fb_angry", type="integer", nullable=true)
     */
    private $fbAngry;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes", type="integer", nullable=true)
     */
    private $twLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes", type="integer", nullable=true)
     */
    private $instLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes", type="integer", nullable=true)
     */
    private $pinterLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_booms", type="integer", nullable=true)
     */
    private $fbBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_booms", type="integer", nullable=true)
     */
    private $twBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_booms", type="integer", nullable=true)
     */
    private $instBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_booms", type="integer", nullable=true)
     */
    private $pinterBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares", type="integer", nullable=true)
     */
    private $fbShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares", type="integer", nullable=true)
     */
    private $twShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares", type="integer", nullable=true)
     */
    private $instShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares", type="integer", nullable=true)
     */
    private $pinterShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_likes", type="integer", nullable=true)
     */
    private $boomLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_shares", type="integer", nullable=true)
     */
    private $boomShares;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_points", type="integer", nullable=true)
     */
    private $loyaltyPoints;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_time", type="datetime", nullable=true)
     */
    private $cTime;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_boom_idcode", type="string", length=255, nullable=true)
     */
    private $facebookBoomIdcode;

    /**
     * @var string
     *
     * @ORM\Column(name="tweeter_boom_idcode", type="string", length=255, nullable=true)
     */
    private $tweeterBoomIdcode;

    /**
     * @var string
     *
     * @ORM\Column(name="inst_boom_idcode", type="string", length=255, nullable=true)
     */
    private $instBoomIdcode;

    /**
     * @var string
     *
     * @ORM\Column(name="pinter_boom_idcode", type="string", length=255, nullable=true)
     */
    private $pinterBoomIdcode;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket", type="string", length=12, nullable=true)
     */
    private $ticket;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_id", type="string", length=100, nullable=true)
     */
    private $fbId;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * @var \Contest
     *
     * @ORM\ManyToOne(targetEntity="Contest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contest_id", referencedColumnName="id")
     * })
     */
    private $contest;

    /**
     * @var \BrandContest
     *
     * @ORM\ManyToOne(targetEntity="BrandContest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_contest_id", referencedColumnName="id")
     * })
     */
    private $brandContest;

    /**
     * @var \Checkin
     *
     * @ORM\ManyToOne(targetEntity="Checkin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="checkin_id", referencedColumnName="id")
     * })
     */
    private $checkin;

    /**
     * @var \Coupon
     *
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coupon_id", referencedColumnName="id")
     * })
     */
    private $coupon;

    /**
     * @var \Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     * })
     */
    private $offer;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \Coupon
     *
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_coupon_id", referencedColumnName="id")
     * })
     */
    private $storeCoupon;

    /**
     * @var \Coupon
     *
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_coupon_id", referencedColumnName="id")
     * })
     */
    private $brandCoupon;

    /**
     * @var \Discount
     *
     * @ORM\ManyToOne(targetEntity="Discount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     * })
     */
    private $discount;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set boomCategoryId
     *
     * @param integer $boomCategoryId
     *
     * @return Boom
     */
    public function setBoomCategoryId($boomCategoryId)
    {
        $this->boomCategoryId = $boomCategoryId;

        return $this;
    }

    /**
     * Get boomCategoryId
     *
     * @return integer
     */
    public function getBoomCategoryId()
    {
        return $this->boomCategoryId;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Boom
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set boomUserMessage
     *
     * @param string $boomUserMessage
     *
     * @return Boom
     */
    public function setBoomUserMessage($boomUserMessage)
    {
        $this->boomUserMessage = $boomUserMessage;

        return $this;
    }

    /**
     * Get boomUserMessage
     *
     * @return string
     */
    public function getBoomUserMessage()
    {
        return $this->boomUserMessage;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Boom
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set pDate
     *
     * @param \DateTime $pDate
     *
     * @return Boom
     */
    public function setPDate($pDate)
    {
        $this->pDate = $pDate;

        return $this;
    }

    /**
     * Get pDate
     *
     * @return \DateTime
     */
    public function getPDate()
    {
        return $this->pDate;
    }

    /**
     * Set fbLikes
     *
     * @param integer $fbLikes
     *
     * @return Boom
     */
    public function setFbLikes($fbLikes)
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    /**
     * Get fbLikes
     *
     * @return integer
     */
    public function getFbLikes()
    {
        return $this->fbLikes;
    }

    /**
     * Set twLikes
     *
     * @param integer $twLikes
     *
     * @return Boom
     */
    public function setTwLikes($twLikes)
    {
        $this->twLikes = $twLikes;

        return $this;
    }

    /**
     * Get twLikes
     *
     * @return integer
     */
    public function getTwLikes()
    {
        return $this->twLikes;
    }

    /**
     * Set instLikes
     *
     * @param integer $instLikes
     *
     * @return Boom
     */
    public function setInstLikes($instLikes)
    {
        $this->instLikes = $instLikes;

        return $this;
    }

    /**
     * Get instLikes
     *
     * @return integer
     */
    public function getInstLikes()
    {
        return $this->instLikes;
    }

    /**
     * Set pinterLikes
     *
     * @param integer $pinterLikes
     *
     * @return Boom
     */
    public function setPinterLikes($pinterLikes)
    {
        $this->pinterLikes = $pinterLikes;

        return $this;
    }

    /**
     * Get pinterLikes
     *
     * @return integer
     */
    public function getPinterLikes()
    {
        return $this->pinterLikes;
    }

    /**
     * Set fbBooms
     *
     * @param integer $fbBooms
     *
     * @return Boom
     */
    public function setFbBooms($fbBooms)
    {
        $this->fbBooms = $fbBooms;

        return $this;
    }

    /**
     * Get fbBooms
     *
     * @return integer
     */
    public function getFbBooms()
    {
        return $this->fbBooms;
    }

    /**
     * Set twBooms
     *
     * @param integer $twBooms
     *
     * @return Boom
     */
    public function setTwBooms($twBooms)
    {
        $this->twBooms = $twBooms;

        return $this;
    }

    /**
     * Get twBooms
     *
     * @return integer
     */
    public function getTwBooms()
    {
        return $this->twBooms;
    }

    /**
     * Set instBooms
     *
     * @param integer $instBooms
     *
     * @return Boom
     */
    public function setInstBooms($instBooms)
    {
        $this->instBooms = $instBooms;

        return $this;
    }

    /**
     * Get instBooms
     *
     * @return integer
     */
    public function getInstBooms()
    {
        return $this->instBooms;
    }

    /**
     * Set pinterBooms
     *
     * @param integer $pinterBooms
     *
     * @return Boom
     */
    public function setPinterBooms($pinterBooms)
    {
        $this->pinterBooms = $pinterBooms;

        return $this;
    }

    /**
     * Get pinterBooms
     *
     * @return integer
     */
    public function getPinterBooms()
    {
        return $this->pinterBooms;
    }

    /**
     * Set fbShares
     *
     * @param integer $fbShares
     *
     * @return Boom
     */
    public function setFbShares($fbShares)
    {
        $this->fbShares = $fbShares;

        return $this;
    }

    /**
     * Get fbShares
     *
     * @return integer
     */
    public function getFbShares()
    {
        return $this->fbShares;
    }

    /**
     * Set twShares
     *
     * @param integer $twShares
     *
     * @return Boom
     */
    public function setTwShares($twShares)
    {
        $this->twShares = $twShares;

        return $this;
    }

    /**
     * Get twShares
     *
     * @return integer
     */
    public function getTwShares()
    {
        return $this->twShares;
    }

    /**
     * Set instShares
     *
     * @param integer $instShares
     *
     * @return Boom
     */
    public function setInstShares($instShares)
    {
        $this->instShares = $instShares;

        return $this;
    }

    /**
     * Get instShares
     *
     * @return integer
     */
    public function getInstShares()
    {
        return $this->instShares;
    }

    /**
     * Set pinterShares
     *
     * @param integer $pinterShares
     *
     * @return Boom
     */
    public function setPinterShares($pinterShares)
    {
        $this->pinterShares = $pinterShares;

        return $this;
    }

    /**
     * Get pinterShares
     *
     * @return integer
     */
    public function getPinterShares()
    {
        return $this->pinterShares;
    }

    /**
     * Set boomLikes
     *
     * @param integer $boomLikes
     *
     * @return Boom
     */
    public function setBoomLikes($boomLikes)
    {
        $this->boomLikes = $boomLikes;

        return $this;
    }

    /**
     * Get boomLikes
     *
     * @return integer
     */
    public function getBoomLikes()
    {
        return $this->boomLikes;
    }

    /**
     * Set boomShares
     *
     * @param integer $boomShares
     *
     * @return Boom
     */
    public function setBoomShares($boomShares)
    {
        $this->boomShares = $boomShares;

        return $this;
    }

    /**
     * Get boomShares
     *
     * @return integer
     */
    public function getBoomShares()
    {
        return $this->boomShares;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Boom
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set loyaltyPoints
     *
     * @param integer $loyaltyPoints
     *
     * @return Boom
     */
    public function setLoyaltyPoints($loyaltyPoints)
    {
        $this->loyaltyPoints = $loyaltyPoints;

        return $this;
    }

    /**
     * Get loyaltyPoints
     *
     * @return integer
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Set cTime
     *
     * @param \DateTime $cTime
     *
     * @return Boom
     */
    public function setCTime($cTime)
    {
        $this->cTime = $cTime;

        return $this;
    }

    /**
     * Get cTime
     *
     * @return \DateTime
     */
    public function getCTime()
    {
        return $this->cTime;
    }

    /**
     * Set facebookBoomIdcode
     *
     * @param string $facebookBoomIdcode
     *
     * @return Boom
     */
    public function setFacebookBoomIdcode($facebookBoomIdcode)
    {
        $this->facebookBoomIdcode = $facebookBoomIdcode;

        return $this;
    }

    /**
     * Get facebookBoomIdcode
     *
     * @return string
     */
    public function getFacebookBoomIdcode()
    {
        return $this->facebookBoomIdcode;
    }

    /**
     * Set tweeterBoomIdcode
     *
     * @param string $tweeterBoomIdcode
     *
     * @return Boom
     */
    public function setTweeterBoomIdcode($tweeterBoomIdcode)
    {
        $this->tweeterBoomIdcode = $tweeterBoomIdcode;

        return $this;
    }

    /**
     * Get tweeterBoomIdcode
     *
     * @return string
     */
    public function getTweeterBoomIdcode()
    {
        return $this->tweeterBoomIdcode;
    }

    /**
     * Set instBoomIdcode
     *
     * @param string $instBoomIdcode
     *
     * @return Boom
     */
    public function setInstBoomIdcode($instBoomIdcode)
    {
        $this->instBoomIdcode = $instBoomIdcode;

        return $this;
    }

    /**
     * Get instBoomIdcode
     *
     * @return string
     */
    public function getInstBoomIdcode()
    {
        return $this->instBoomIdcode;
    }

    /**
     * Set pinterBoomIdcode
     *
     * @param string $pinterBoomIdcode
     *
     * @return Boom
     */
    public function setPinterBoomIdcode($pinterBoomIdcode)
    {
        $this->pinterBoomIdcode = $pinterBoomIdcode;

        return $this;
    }

    /**
     * Get pinterBoomIdcode
     *
     * @return string
     */
    public function getPinterBoomIdcode()
    {
        return $this->pinterBoomIdcode;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return Boom
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Boom
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set fbId
     *
     * @param string $fbId
     *
     * @return Boom
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;

        return $this;
    }

    /**
     * Get fbId
     *
     * @return string
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return Boom
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set contest
     *
     * @param \Bubblz\EntitiesBundle\Entity\Contest $contest
     *
     * @return Boom
     */
    public function setContest(\Bubblz\EntitiesBundle\Entity\Contest $contest = null)
    {
        $this->contest = $contest;

        return $this;
    }

    /**
     * Get contest
     *
     * @return \Bubblz\EntitiesBundle\Entity\Contest
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Set brandContest
     *
     * @param \Bubblz\EntitiesBundle\Entity\BrandContest $brandContest
     *
     * @return Boom
     */
    public function setBrandContest(\Bubblz\EntitiesBundle\Entity\BrandContest $brandContest = null)
    {
        $this->brandContest = $brandContest;

        return $this;
    }

    /**
     * Get brandContest
     *
     * @return \Bubblz\EntitiesBundle\Entity\BrandContest
     */
    public function getBrandContest()
    {
        return $this->brandContest;
    }

    /**
     * Set checkin
     *
     * @param \Bubblz\EntitiesBundle\Entity\Checkin $checkin
     *
     * @return Boom
     */
    public function setCheckin(\Bubblz\EntitiesBundle\Entity\Checkin $checkin = null)
    {
        $this->checkin = $checkin;

        return $this;
    }

    /**
     * Get checkin
     *
     * @return \Bubblz\EntitiesBundle\Entity\Checkin
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * Set coupon
     *
     * @param \Bubblz\EntitiesBundle\Entity\Coupon $coupon
     *
     * @return Boom
     */
    public function setCoupon(\Bubblz\EntitiesBundle\Entity\Coupon $coupon = null)
    {
        $this->coupon = $coupon;

        return $this;
    }

    /**
     * Get coupon
     *
     * @return \Bubblz\EntitiesBundle\Entity\Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * Set offer
     *
     * @param \Bubblz\EntitiesBundle\Entity\Offer $offer
     *
     * @return Boom
     */
    public function setOffer(\Bubblz\EntitiesBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Bubblz\EntitiesBundle\Entity\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Boom
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return Boom
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set storeCoupon
     *
     * @param \Bubblz\EntitiesBundle\Entity\Coupon $storeCoupon
     *
     * @return Boom
     */
    public function setStoreCoupon(\Bubblz\EntitiesBundle\Entity\Coupon $storeCoupon = null)
    {
        $this->storeCoupon = $storeCoupon;

        return $this;
    }

    /**
     * Get storeCoupon
     *
     * @return \Bubblz\EntitiesBundle\Entity\Coupon
     */
    public function getStoreCoupon()
    {
        return $this->storeCoupon;
    }

    /**
     * Set brandCoupon
     *
     * @param \Bubblz\EntitiesBundle\Entity\Coupon $brandCoupon
     *
     * @return Boom
     */
    public function setBrandCoupon(\Bubblz\EntitiesBundle\Entity\Coupon $brandCoupon = null)
    {
        $this->brandCoupon = $brandCoupon;

        return $this;
    }

    /**
     * Get brandCoupon
     *
     * @return \Bubblz\EntitiesBundle\Entity\Coupon
     */
    public function getBrandCoupon()
    {
        return $this->brandCoupon;
    }

    /**
     * Set discount
     *
     * @param \Bubblz\EntitiesBundle\Entity\Discount $discount
     *
     * @return Boom
     */
    public function setDiscount(\Bubblz\EntitiesBundle\Entity\Discount $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return \Bubblz\EntitiesBundle\Entity\Discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set fbLove
     *
     * @param integer $fbLove
     *
     * @return Boom
     */
    public function setFbLove($fbLove)
    {
        $this->fbLove = $fbLove;

        return $this;
    }

    /**
     * Get fbLove
     *
     * @return integer
     */
    public function getFbLove()
    {
        return $this->fbLove;
    }

    /**
     * Set fbHaha
     *
     * @param integer $fbHaha
     *
     * @return Boom
     */
    public function setFbHaha($fbHaha)
    {
        $this->fbHaha = $fbHaha;

        return $this;
    }

    /**
     * Get fbHaha
     *
     * @return integer
     */
    public function getFbHaha()
    {
        return $this->fbHaha;
    }

    /**
     * Set fbWow
     *
     * @param integer $fbWow
     *
     * @return Boom
     */
    public function setFbWow($fbWow)
    {
        $this->fbWow = $fbWow;

        return $this;
    }

    /**
     * Get fbWow
     *
     * @return integer
     */
    public function getFbWow()
    {
        return $this->fbWow;
    }

    /**
     * Set fbSad
     *
     * @param integer $fbSad
     *
     * @return Boom
     */
    public function setFbSad($fbSad)
    {
        $this->fbSad = $fbSad;

        return $this;
    }

    /**
     * Get fbSad
     *
     * @return integer
     */
    public function getFbSad()
    {
        return $this->fbSad;
    }

    /**
     * Set fbAngry
     *
     * @param integer $fbAngry
     *
     * @return Boom
     */
    public function setFbAngry($fbAngry)
    {
        $this->fbAngry = $fbAngry;

        return $this;
    }

    /**
     * Get fbAngry
     *
     * @return integer
     */
    public function getFbAngry()
    {
        return $this->fbAngry;
    }
}
