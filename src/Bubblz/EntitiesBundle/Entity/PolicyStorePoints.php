<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PolicyStorePoints
 *
 * @ORM\Table(name="policy_store_points", indexes={@ORM\Index(name="fk_policy_store_points_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class PolicyStorePoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes_count", type="integer", nullable=true)
     */
    private $fbLikesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes_count", type="integer", nullable=true)
     */
    private $twLikesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes_count", type="integer", nullable=true)
     */
    private $instLikesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes_count", type="integer", nullable=true)
     */
    private $pinterLikesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares_count", type="integer", nullable=true)
     */
    private $fbSharesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares_count", type="integer", nullable=true)
     */
    private $twSharesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares_count", type="integer", nullable=true)
     */
    private $instSharesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares_count", type="integer", nullable=true)
     */
    private $pinterSharesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_likes_count", type="integer", nullable=true)
     */
    private $boomLikesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_shares_count", type="integer", nullable=true)
     */
    private $boomSharesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="booms_count", type="integer", nullable=true)
     */
    private $boomsCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes_point", type="integer", nullable=true)
     */
    private $fbLikesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes_point", type="integer", nullable=true)
     */
    private $twLikesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes_point", type="integer", nullable=true)
     */
    private $instLikesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes_point", type="integer", nullable=true)
     */
    private $pinterLikesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares_point", type="integer", nullable=true)
     */
    private $fbSharesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares_point", type="integer", nullable=true)
     */
    private $twSharesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares_point", type="integer", nullable=true)
     */
    private $instSharesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares_point", type="integer", nullable=true)
     */
    private $pinterSharesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_likes_point", type="integer", nullable=true)
     */
    private $boomLikesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_shares_point", type="integer", nullable=true)
     */
    private $boomSharesPoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="booms_point", type="integer", nullable=true)
     */
    private $boomsPoint;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fbLikesCount
     *
     * @param integer $fbLikesCount
     *
     * @return PolicyStorePoints
     */
    public function setFbLikesCount($fbLikesCount)
    {
        $this->fbLikesCount = $fbLikesCount;

        return $this;
    }

    /**
     * Get fbLikesCount
     *
     * @return integer
     */
    public function getFbLikesCount()
    {
        return $this->fbLikesCount;
    }

    /**
     * Set twLikesCount
     *
     * @param integer $twLikesCount
     *
     * @return PolicyStorePoints
     */
    public function setTwLikesCount($twLikesCount)
    {
        $this->twLikesCount = $twLikesCount;

        return $this;
    }

    /**
     * Get twLikesCount
     *
     * @return integer
     */
    public function getTwLikesCount()
    {
        return $this->twLikesCount;
    }

    /**
     * Set instLikesCount
     *
     * @param integer $instLikesCount
     *
     * @return PolicyStorePoints
     */
    public function setInstLikesCount($instLikesCount)
    {
        $this->instLikesCount = $instLikesCount;

        return $this;
    }

    /**
     * Get instLikesCount
     *
     * @return integer
     */
    public function getInstLikesCount()
    {
        return $this->instLikesCount;
    }

    /**
     * Set pinterLikesCount
     *
     * @param integer $pinterLikesCount
     *
     * @return PolicyStorePoints
     */
    public function setPinterLikesCount($pinterLikesCount)
    {
        $this->pinterLikesCount = $pinterLikesCount;

        return $this;
    }

    /**
     * Get pinterLikesCount
     *
     * @return integer
     */
    public function getPinterLikesCount()
    {
        return $this->pinterLikesCount;
    }

    /**
     * Set fbSharesCount
     *
     * @param integer $fbSharesCount
     *
     * @return PolicyStorePoints
     */
    public function setFbSharesCount($fbSharesCount)
    {
        $this->fbSharesCount = $fbSharesCount;

        return $this;
    }

    /**
     * Get fbSharesCount
     *
     * @return integer
     */
    public function getFbSharesCount()
    {
        return $this->fbSharesCount;
    }

    /**
     * Set twSharesCount
     *
     * @param integer $twSharesCount
     *
     * @return PolicyStorePoints
     */
    public function setTwSharesCount($twSharesCount)
    {
        $this->twSharesCount = $twSharesCount;

        return $this;
    }

    /**
     * Get twSharesCount
     *
     * @return integer
     */
    public function getTwSharesCount()
    {
        return $this->twSharesCount;
    }

    /**
     * Set instSharesCount
     *
     * @param integer $instSharesCount
     *
     * @return PolicyStorePoints
     */
    public function setInstSharesCount($instSharesCount)
    {
        $this->instSharesCount = $instSharesCount;

        return $this;
    }

    /**
     * Get instSharesCount
     *
     * @return integer
     */
    public function getInstSharesCount()
    {
        return $this->instSharesCount;
    }

    /**
     * Set pinterSharesCount
     *
     * @param integer $pinterSharesCount
     *
     * @return PolicyStorePoints
     */
    public function setPinterSharesCount($pinterSharesCount)
    {
        $this->pinterSharesCount = $pinterSharesCount;

        return $this;
    }

    /**
     * Get pinterSharesCount
     *
     * @return integer
     */
    public function getPinterSharesCount()
    {
        return $this->pinterSharesCount;
    }

    /**
     * Set boomLikesCount
     *
     * @param integer $boomLikesCount
     *
     * @return PolicyStorePoints
     */
    public function setBoomLikesCount($boomLikesCount)
    {
        $this->boomLikesCount = $boomLikesCount;

        return $this;
    }

    /**
     * Get boomLikesCount
     *
     * @return integer
     */
    public function getBoomLikesCount()
    {
        return $this->boomLikesCount;
    }

    /**
     * Set boomSharesCount
     *
     * @param integer $boomSharesCount
     *
     * @return PolicyStorePoints
     */
    public function setBoomSharesCount($boomSharesCount)
    {
        $this->boomSharesCount = $boomSharesCount;

        return $this;
    }

    /**
     * Get boomSharesCount
     *
     * @return integer
     */
    public function getBoomSharesCount()
    {
        return $this->boomSharesCount;
    }

    /**
     * Set boomsCount
     *
     * @param integer $boomsCount
     *
     * @return PolicyStorePoints
     */
    public function setBoomsCount($boomsCount)
    {
        $this->boomsCount = $boomsCount;

        return $this;
    }

    /**
     * Get boomsCount
     *
     * @return integer
     */
    public function getBoomsCount()
    {
        return $this->boomsCount;
    }

    /**
     * Set fbLikesPoint
     *
     * @param integer $fbLikesPoint
     *
     * @return PolicyStorePoints
     */
    public function setFbLikesPoint($fbLikesPoint)
    {
        $this->fbLikesPoint = $fbLikesPoint;

        return $this;
    }

    /**
     * Get fbLikesPoint
     *
     * @return integer
     */
    public function getFbLikesPoint()
    {
        return $this->fbLikesPoint;
    }

    /**
     * Set twLikesPoint
     *
     * @param integer $twLikesPoint
     *
     * @return PolicyStorePoints
     */
    public function setTwLikesPoint($twLikesPoint)
    {
        $this->twLikesPoint = $twLikesPoint;

        return $this;
    }

    /**
     * Get twLikesPoint
     *
     * @return integer
     */
    public function getTwLikesPoint()
    {
        return $this->twLikesPoint;
    }

    /**
     * Set instLikesPoint
     *
     * @param integer $instLikesPoint
     *
     * @return PolicyStorePoints
     */
    public function setInstLikesPoint($instLikesPoint)
    {
        $this->instLikesPoint = $instLikesPoint;

        return $this;
    }

    /**
     * Get instLikesPoint
     *
     * @return integer
     */
    public function getInstLikesPoint()
    {
        return $this->instLikesPoint;
    }

    /**
     * Set pinterLikesPoint
     *
     * @param integer $pinterLikesPoint
     *
     * @return PolicyStorePoints
     */
    public function setPinterLikesPoint($pinterLikesPoint)
    {
        $this->pinterLikesPoint = $pinterLikesPoint;

        return $this;
    }

    /**
     * Get pinterLikesPoint
     *
     * @return integer
     */
    public function getPinterLikesPoint()
    {
        return $this->pinterLikesPoint;
    }

    /**
     * Set fbSharesPoint
     *
     * @param integer $fbSharesPoint
     *
     * @return PolicyStorePoints
     */
    public function setFbSharesPoint($fbSharesPoint)
    {
        $this->fbSharesPoint = $fbSharesPoint;

        return $this;
    }

    /**
     * Get fbSharesPoint
     *
     * @return integer
     */
    public function getFbSharesPoint()
    {
        return $this->fbSharesPoint;
    }

    /**
     * Set twSharesPoint
     *
     * @param integer $twSharesPoint
     *
     * @return PolicyStorePoints
     */
    public function setTwSharesPoint($twSharesPoint)
    {
        $this->twSharesPoint = $twSharesPoint;

        return $this;
    }

    /**
     * Get twSharesPoint
     *
     * @return integer
     */
    public function getTwSharesPoint()
    {
        return $this->twSharesPoint;
    }

    /**
     * Set instSharesPoint
     *
     * @param integer $instSharesPoint
     *
     * @return PolicyStorePoints
     */
    public function setInstSharesPoint($instSharesPoint)
    {
        $this->instSharesPoint = $instSharesPoint;

        return $this;
    }

    /**
     * Get instSharesPoint
     *
     * @return integer
     */
    public function getInstSharesPoint()
    {
        return $this->instSharesPoint;
    }

    /**
     * Set pinterSharesPoint
     *
     * @param integer $pinterSharesPoint
     *
     * @return PolicyStorePoints
     */
    public function setPinterSharesPoint($pinterSharesPoint)
    {
        $this->pinterSharesPoint = $pinterSharesPoint;

        return $this;
    }

    /**
     * Get pinterSharesPoint
     *
     * @return integer
     */
    public function getPinterSharesPoint()
    {
        return $this->pinterSharesPoint;
    }

    /**
     * Set boomLikesPoint
     *
     * @param integer $boomLikesPoint
     *
     * @return PolicyStorePoints
     */
    public function setBoomLikesPoint($boomLikesPoint)
    {
        $this->boomLikesPoint = $boomLikesPoint;

        return $this;
    }

    /**
     * Get boomLikesPoint
     *
     * @return integer
     */
    public function getBoomLikesPoint()
    {
        return $this->boomLikesPoint;
    }

    /**
     * Set boomSharesPoint
     *
     * @param integer $boomSharesPoint
     *
     * @return PolicyStorePoints
     */
    public function setBoomSharesPoint($boomSharesPoint)
    {
        $this->boomSharesPoint = $boomSharesPoint;

        return $this;
    }

    /**
     * Get boomSharesPoint
     *
     * @return integer
     */
    public function getBoomSharesPoint()
    {
        return $this->boomSharesPoint;
    }

    /**
     * Set boomsPoint
     *
     * @param integer $boomsPoint
     *
     * @return PolicyStorePoints
     */
    public function setBoomsPoint($boomsPoint)
    {
        $this->boomsPoint = $boomsPoint;

        return $this;
    }

    /**
     * Get boomsPoint
     *
     * @return integer
     */
    public function getBoomsPoint()
    {
        return $this->boomsPoint;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return PolicyStorePoints
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
