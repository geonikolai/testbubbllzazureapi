<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Checkin
 *
 * @ORM\Table(name="checkin", indexes={@ORM\Index(name="fk_checkin_boom_user1_idx", columns={"boom_user_id"}), @ORM\Index(name="fk_checkin_store1_idx", columns={"store_id"}), @ORM\Index(name="boom_user_id_store_id_status", columns={"boom_user_id", "store_id", "status"}), @ORM\Index(name="boom_user_id_store_id", columns={"boom_user_id", "store_id"}), @ORM\Index(name="boom_user_id_store_id_is_bubbll", columns={"boom_user_id", "store_id", "is_bubbll"}), @ORM\Index(name="store_id_is_bubbll", columns={"store_id", "is_bubbll"}), @ORM\Index(name="boom_user_id_store_id_checkin_time", columns={"boom_user_id", "store_id", "checkin_time"}), @ORM\Index(name="fk_checkin_brand1_idx", columns={"brand_id"})})
 * @ORM\Entity
 */
class Checkin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkin_time", type="datetime", nullable=true)
     */
    private $checkinTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkout_time", type="datetime", nullable=true)
     */
    private $checkoutTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_bubbll", type="boolean", nullable=true)
     */
    private $isBubbll = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="bubbll_ticket", type="string", length=12, nullable=true)
     */
    private $bubbllTicket;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checkinTime
     *
     * @param \DateTime $checkinTime
     *
     * @return Checkin
     */
    public function setCheckinTime($checkinTime)
    {
        $this->checkinTime = $checkinTime;

        return $this;
    }

    /**
     * Get checkinTime
     *
     * @return \DateTime
     */
    public function getCheckinTime()
    {
        return $this->checkinTime;
    }

    /**
     * Set checkoutTime
     *
     * @param \DateTime $checkoutTime
     *
     * @return Checkin
     */
    public function setCheckoutTime($checkoutTime)
    {
        $this->checkoutTime = $checkoutTime;

        return $this;
    }

    /**
     * Get checkoutTime
     *
     * @return \DateTime
     */
    public function getCheckoutTime()
    {
        return $this->checkoutTime;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Checkin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isBubbll
     *
     * @param boolean $isBubbll
     *
     * @return Checkin
     */
    public function setIsBubbll($isBubbll)
    {
        $this->isBubbll = $isBubbll;

        return $this;
    }

    /**
     * Get isBubbll
     *
     * @return boolean
     */
    public function getIsBubbll()
    {
        return $this->isBubbll;
    }

    /**
     * Set bubbllTicket
     *
     * @param string $bubbllTicket
     *
     * @return Checkin
     */
    public function setBubbllTicket($bubbllTicket)
    {
        $this->bubbllTicket = $bubbllTicket;

        return $this;
    }

    /**
     * Get bubbllTicket
     *
     * @return string
     */
    public function getBubbllTicket()
    {
        return $this->bubbllTicket;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return Checkin
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Checkin
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return Checkin
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
