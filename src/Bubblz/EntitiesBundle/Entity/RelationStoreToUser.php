<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RelationStoreToUser
 *
 * @ORM\Table(name="relation_store_to_user", indexes={@ORM\Index(name="store_id_idx", columns={"master_store_id"}), @ORM\Index(name="user_id_idx", columns={"slave_user_id"})})
 * @ORM\Entity
 */
class RelationStoreToUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="master_store_id", referencedColumnName="id")
     * })
     */
    private $masterStore;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slave_user_id", referencedColumnName="id")
     * })
     */
    private $slaveUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return RelationStoreToUser
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return RelationStoreToUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set masterStore
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $masterStore
     *
     * @return RelationStoreToUser
     */
    public function setMasterStore(\Bubblz\EntitiesBundle\Entity\Store $masterStore = null)
    {
        $this->masterStore = $masterStore;

        return $this;
    }

    /**
     * Get masterStore
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getMasterStore()
    {
        return $this->masterStore;
    }

    /**
     * Set slaveUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $slaveUser
     *
     * @return RelationStoreToUser
     */
    public function setSlaveUser(\Bubblz\EntitiesBundle\Entity\BoomUser $slaveUser = null)
    {
        $this->slaveUser = $slaveUser;

        return $this;
    }

    /**
     * Get slaveUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getSlaveUser()
    {
        return $this->slaveUser;
    }
}
