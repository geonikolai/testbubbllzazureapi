<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organization
 *
 * @ORM\Table(name="organization", indexes={@ORM\Index(name="fk_organization_country1_idx", columns={"country_id"}), @ORM\Index(name="fk_organization_city1_idx", columns={"city_id"}), @ORM\Index(name="fk_organization_state1_idx", columns={"state_id"}), @ORM\Index(name="fk_organization_address1_idx", columns={"main_address_id"})})
 * @ORM\Entity
 */
class Organization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_type", type="integer", nullable=true)
     */
    private $organizationType;

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=45, nullable=true)
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="brandname", type="string", length=45, nullable=false)
     */
    private $brandname;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_number", type="string", length=20, nullable=true)
     */
    private $taxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="broadcast_message", type="string", length=100, nullable=true)
     */
    private $broadcastMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_image", type="string", length=45, nullable=true)
     */
    private $logoImage;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=13, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone2", type="string", length=13, nullable=true)
     */
    private $telephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=13, nullable=true)
     */
    private $fax;

    /**
     * @var \Address
     *
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_address_id", referencedColumnName="id")
     * })
     */
    private $mainAddress;

    /**
     * @var \City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \StateUnion
     *
     * @ORM\ManyToOne(targetEntity="StateUnion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * })
     */
    private $state;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Organization
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set organizationType
     *
     * @param integer $organizationType
     *
     * @return Organization
     */
    public function setOrganizationType($organizationType)
    {
        $this->organizationType = $organizationType;

        return $this;
    }

    /**
     * Get organizationType
     *
     * @return integer
     */
    public function getOrganizationType()
    {
        return $this->organizationType;
    }

    /**
     * Set mainImage
     *
     * @param string $mainImage
     *
     * @return Organization
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Get mainImage
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Organization
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set brandname
     *
     * @param string $brandname
     *
     * @return Organization
     */
    public function setBrandname($brandname)
    {
        $this->brandname = $brandname;

        return $this;
    }

    /**
     * Get brandname
     *
     * @return string
     */
    public function getBrandname()
    {
        return $this->brandname;
    }

    /**
     * Set taxNumber
     *
     * @param string $taxNumber
     *
     * @return Organization
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;

        return $this;
    }

    /**
     * Get taxNumber
     *
     * @return string
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * Set broadcastMessage
     *
     * @param string $broadcastMessage
     *
     * @return Organization
     */
    public function setBroadcastMessage($broadcastMessage)
    {
        $this->broadcastMessage = $broadcastMessage;

        return $this;
    }

    /**
     * Get broadcastMessage
     *
     * @return string
     */
    public function getBroadcastMessage()
    {
        return $this->broadcastMessage;
    }

    /**
     * Set logoImage
     *
     * @param string $logoImage
     *
     * @return Organization
     */
    public function setLogoImage($logoImage)
    {
        $this->logoImage = $logoImage;

        return $this;
    }

    /**
     * Get logoImage
     *
     * @return string
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Organization
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Organization
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     *
     * @return Organization
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Organization
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mainAddress
     *
     * @param \Bubblz\EntitiesBundle\Entity\Address $mainAddress
     *
     * @return Organization
     */
    public function setMainAddress(\Bubblz\EntitiesBundle\Entity\Address $mainAddress = null)
    {
        $this->mainAddress = $mainAddress;

        return $this;
    }

    /**
     * Get mainAddress
     *
     * @return \Bubblz\EntitiesBundle\Entity\Address
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }

    /**
     * Set city
     *
     * @param \Bubblz\EntitiesBundle\Entity\City $city
     *
     * @return Organization
     */
    public function setCity(\Bubblz\EntitiesBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Bubblz\EntitiesBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \Bubblz\EntitiesBundle\Entity\Country $country
     *
     * @return Organization
     */
    public function setCountry(\Bubblz\EntitiesBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Bubblz\EntitiesBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param \Bubblz\EntitiesBundle\Entity\StateUnion $state
     *
     * @return Organization
     */
    public function setState(\Bubblz\EntitiesBundle\Entity\StateUnion $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Bubblz\EntitiesBundle\Entity\StateUnion
     */
    public function getState()
    {
        return $this->state;
    }
}
