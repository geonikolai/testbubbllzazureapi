<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalExclusions
 *
 * @ORM\Table(name="global_exclusions", indexes={@ORM\Index(name="boom_user_id", columns={"boom_user_id"}), @ORM\Index(name="contest_id", columns={"contest_id"})})
 * @ORM\Entity
 */
class GlobalExclusions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \GlobalContest
     *
     * @ORM\ManyToOne(targetEntity="GlobalContest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contest_id", referencedColumnName="id")
     * })
     */
    private $contest;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return GlobalExclusions
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return GlobalExclusions
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set contest
     *
     * @param \Bubblz\EntitiesBundle\Entity\GlobalContest $contest
     *
     * @return GlobalExclusions
     */
    public function setContest(\Bubblz\EntitiesBundle\Entity\GlobalContest $contest = null)
    {
        $this->contest = $contest;

        return $this;
    }

    /**
     * Get contest
     *
     * @return \Bubblz\EntitiesBundle\Entity\GlobalContest
     */
    public function getContest()
    {
        return $this->contest;
    }
}
