<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="short_vat", type="string", length=3, nullable=true)
     */
    private $shortVat;

    /**
     * @var string
     *
     * @ORM\Column(name="short_net", type="string", length=3, nullable=true)
     */
    private $shortNet;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Country
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortVat
     *
     * @param string $shortVat
     *
     * @return Country
     */
    public function setShortVat($shortVat)
    {
        $this->shortVat = $shortVat;

        return $this;
    }

    /**
     * Get shortVat
     *
     * @return string
     */
    public function getShortVat()
    {
        return $this->shortVat;
    }

    /**
     * Set shortNet
     *
     * @param string $shortNet
     *
     * @return Country
     */
    public function setShortNet($shortNet)
    {
        $this->shortNet = $shortNet;

        return $this;
    }

    /**
     * Get shortNet
     *
     * @return string
     */
    public function getShortNet()
    {
        return $this->shortNet;
    }
}
