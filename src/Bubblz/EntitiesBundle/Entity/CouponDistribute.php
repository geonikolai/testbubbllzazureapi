<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CouponDistribute
 *
 * @ORM\Table(name="coupon_distribute", indexes={@ORM\Index(name="fk_coupon_distribute_coupon1_idx", columns={"coupon_id"}), @ORM\Index(name="fk_coupon_distribute_boom_user_role1_idx", columns={"boom_user_role_id"})})
 * @ORM\Entity
 */
class CouponDistribute
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="state_id", type="integer", nullable=true)
     */
    private $stateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer", nullable=true)
     */
    private $countryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="city_id", type="integer", nullable=true)
     */
    private $cityId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gender", type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @var boolean
     *
     * @ORM\Column(name="age_from", type="boolean", nullable=true)
     */
    private $ageFrom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="age_to", type="boolean", nullable=true)
     */
    private $ageTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_customer", type="boolean", nullable=true)
     */
    private $isCustomer;

    /**
     * @var \BoomUserRole
     *
     * @ORM\ManyToOne(targetEntity="BoomUserRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_role_id", referencedColumnName="id")
     * })
     */
    private $boomUserRole;

    /**
     * @var \Coupon
     *
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coupon_id", referencedColumnName="id")
     * })
     */
    private $coupon;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stateId
     *
     * @param integer $stateId
     *
     * @return CouponDistribute
     */
    public function setStateId($stateId)
    {
        $this->stateId = $stateId;

        return $this;
    }

    /**
     * Get stateId
     *
     * @return integer
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     *
     * @return CouponDistribute
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set cityId
     *
     * @param integer $cityId
     *
     * @return CouponDistribute
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get cityId
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set gender
     *
     * @param boolean $gender
     *
     * @return CouponDistribute
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return boolean
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set ageFrom
     *
     * @param boolean $ageFrom
     *
     * @return CouponDistribute
     */
    public function setAgeFrom($ageFrom)
    {
        $this->ageFrom = $ageFrom;

        return $this;
    }

    /**
     * Get ageFrom
     *
     * @return boolean
     */
    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    /**
     * Set ageTo
     *
     * @param boolean $ageTo
     *
     * @return CouponDistribute
     */
    public function setAgeTo($ageTo)
    {
        $this->ageTo = $ageTo;

        return $this;
    }

    /**
     * Get ageTo
     *
     * @return boolean
     */
    public function getAgeTo()
    {
        return $this->ageTo;
    }

    /**
     * Set isCustomer
     *
     * @param boolean $isCustomer
     *
     * @return CouponDistribute
     */
    public function setIsCustomer($isCustomer)
    {
        $this->isCustomer = $isCustomer;

        return $this;
    }

    /**
     * Get isCustomer
     *
     * @return boolean
     */
    public function getIsCustomer()
    {
        return $this->isCustomer;
    }

    /**
     * Set boomUserRole
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUserRole $boomUserRole
     *
     * @return CouponDistribute
     */
    public function setBoomUserRole(\Bubblz\EntitiesBundle\Entity\BoomUserRole $boomUserRole = null)
    {
        $this->boomUserRole = $boomUserRole;

        return $this;
    }

    /**
     * Get boomUserRole
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUserRole
     */
    public function getBoomUserRole()
    {
        return $this->boomUserRole;
    }

    /**
     * Set coupon
     *
     * @param \Bubblz\EntitiesBundle\Entity\Coupon $coupon
     *
     * @return CouponDistribute
     */
    public function setCoupon(\Bubblz\EntitiesBundle\Entity\Coupon $coupon = null)
    {
        $this->coupon = $coupon;

        return $this;
    }

    /**
     * Get coupon
     *
     * @return \Bubblz\EntitiesBundle\Entity\Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }
}
