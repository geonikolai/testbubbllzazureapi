<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StoreMarketplaceStat
 *
 * @ORM\Table(name="store_marketplace_stat", indexes={@ORM\Index(name="fk_user_admin_stat_marketplace1_idx", columns={"marketplace_id"}), @ORM\Index(name="fk_user_admin_stat_store1_idx", columns={"store_id"})})
 * @ORM\Entity
 */
class StoreMarketplaceStat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_booms", type="integer", nullable=true)
     */
    private $countBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupon_counts", type="integer", nullable=true)
     */
    private $couponCounts;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_likes", type="integer", nullable=true)
     */
    private $fbLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_likes", type="integer", nullable=true)
     */
    private $twLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_likes", type="integer", nullable=true)
     */
    private $instLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_likes", type="integer", nullable=true)
     */
    private $pinterLikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_booms", type="integer", nullable=true)
     */
    private $fbBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_booms", type="integer", nullable=true)
     */
    private $twBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_booms", type="integer", nullable=true)
     */
    private $instBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_booms", type="integer", nullable=true)
     */
    private $pinterBooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_views", type="integer", nullable=true)
     */
    private $fbViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_views", type="integer", nullable=true)
     */
    private $twViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_views", type="integer", nullable=true)
     */
    private $instViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_views", type="integer", nullable=true)
     */
    private $pinterViews;

    /**
     * @var integer
     *
     * @ORM\Column(name="fb_shares", type="integer", nullable=true)
     */
    private $fbShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_shares", type="integer", nullable=true)
     */
    private $twShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="inst_shares", type="integer", nullable=true)
     */
    private $instShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="pinter_shares", type="integer", nullable=true)
     */
    private $pinterShares;

    /**
     * @var float
     *
     * @ORM\Column(name="total_causes", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_causes", type="integer", nullable=true)
     */
    private $countCauses;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_points", type="integer", nullable=true)
     */
    private $loyaltyPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="total_boom_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalBoomDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_boom_discount", type="integer", nullable=true)
     */
    private $countBoomDiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_coupon_discount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalCouponDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_coupon_discount", type="integer", nullable=true)
     */
    private $countCouponDiscount;

    /**
     * @var \Marketplace
     *
     * @ORM\ManyToOne(targetEntity="Marketplace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marketplace_id", referencedColumnName="id")
     * })
     */
    private $marketplace;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return StoreMarketplaceStat
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return StoreMarketplaceStat
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set countBooms
     *
     * @param integer $countBooms
     *
     * @return StoreMarketplaceStat
     */
    public function setCountBooms($countBooms)
    {
        $this->countBooms = $countBooms;

        return $this;
    }

    /**
     * Get countBooms
     *
     * @return integer
     */
    public function getCountBooms()
    {
        return $this->countBooms;
    }

    /**
     * Set couponCounts
     *
     * @param integer $couponCounts
     *
     * @return StoreMarketplaceStat
     */
    public function setCouponCounts($couponCounts)
    {
        $this->couponCounts = $couponCounts;

        return $this;
    }

    /**
     * Get couponCounts
     *
     * @return integer
     */
    public function getCouponCounts()
    {
        return $this->couponCounts;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return StoreMarketplaceStat
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set fbLikes
     *
     * @param integer $fbLikes
     *
     * @return StoreMarketplaceStat
     */
    public function setFbLikes($fbLikes)
    {
        $this->fbLikes = $fbLikes;

        return $this;
    }

    /**
     * Get fbLikes
     *
     * @return integer
     */
    public function getFbLikes()
    {
        return $this->fbLikes;
    }

    /**
     * Set twLikes
     *
     * @param integer $twLikes
     *
     * @return StoreMarketplaceStat
     */
    public function setTwLikes($twLikes)
    {
        $this->twLikes = $twLikes;

        return $this;
    }

    /**
     * Get twLikes
     *
     * @return integer
     */
    public function getTwLikes()
    {
        return $this->twLikes;
    }

    /**
     * Set instLikes
     *
     * @param integer $instLikes
     *
     * @return StoreMarketplaceStat
     */
    public function setInstLikes($instLikes)
    {
        $this->instLikes = $instLikes;

        return $this;
    }

    /**
     * Get instLikes
     *
     * @return integer
     */
    public function getInstLikes()
    {
        return $this->instLikes;
    }

    /**
     * Set pinterLikes
     *
     * @param integer $pinterLikes
     *
     * @return StoreMarketplaceStat
     */
    public function setPinterLikes($pinterLikes)
    {
        $this->pinterLikes = $pinterLikes;

        return $this;
    }

    /**
     * Get pinterLikes
     *
     * @return integer
     */
    public function getPinterLikes()
    {
        return $this->pinterLikes;
    }

    /**
     * Set fbBooms
     *
     * @param integer $fbBooms
     *
     * @return StoreMarketplaceStat
     */
    public function setFbBooms($fbBooms)
    {
        $this->fbBooms = $fbBooms;

        return $this;
    }

    /**
     * Get fbBooms
     *
     * @return integer
     */
    public function getFbBooms()
    {
        return $this->fbBooms;
    }

    /**
     * Set twBooms
     *
     * @param integer $twBooms
     *
     * @return StoreMarketplaceStat
     */
    public function setTwBooms($twBooms)
    {
        $this->twBooms = $twBooms;

        return $this;
    }

    /**
     * Get twBooms
     *
     * @return integer
     */
    public function getTwBooms()
    {
        return $this->twBooms;
    }

    /**
     * Set instBooms
     *
     * @param integer $instBooms
     *
     * @return StoreMarketplaceStat
     */
    public function setInstBooms($instBooms)
    {
        $this->instBooms = $instBooms;

        return $this;
    }

    /**
     * Get instBooms
     *
     * @return integer
     */
    public function getInstBooms()
    {
        return $this->instBooms;
    }

    /**
     * Set pinterBooms
     *
     * @param integer $pinterBooms
     *
     * @return StoreMarketplaceStat
     */
    public function setPinterBooms($pinterBooms)
    {
        $this->pinterBooms = $pinterBooms;

        return $this;
    }

    /**
     * Get pinterBooms
     *
     * @return integer
     */
    public function getPinterBooms()
    {
        return $this->pinterBooms;
    }

    /**
     * Set fbViews
     *
     * @param integer $fbViews
     *
     * @return StoreMarketplaceStat
     */
    public function setFbViews($fbViews)
    {
        $this->fbViews = $fbViews;

        return $this;
    }

    /**
     * Get fbViews
     *
     * @return integer
     */
    public function getFbViews()
    {
        return $this->fbViews;
    }

    /**
     * Set twViews
     *
     * @param integer $twViews
     *
     * @return StoreMarketplaceStat
     */
    public function setTwViews($twViews)
    {
        $this->twViews = $twViews;

        return $this;
    }

    /**
     * Get twViews
     *
     * @return integer
     */
    public function getTwViews()
    {
        return $this->twViews;
    }

    /**
     * Set instViews
     *
     * @param integer $instViews
     *
     * @return StoreMarketplaceStat
     */
    public function setInstViews($instViews)
    {
        $this->instViews = $instViews;

        return $this;
    }

    /**
     * Get instViews
     *
     * @return integer
     */
    public function getInstViews()
    {
        return $this->instViews;
    }

    /**
     * Set pinterViews
     *
     * @param integer $pinterViews
     *
     * @return StoreMarketplaceStat
     */
    public function setPinterViews($pinterViews)
    {
        $this->pinterViews = $pinterViews;

        return $this;
    }

    /**
     * Get pinterViews
     *
     * @return integer
     */
    public function getPinterViews()
    {
        return $this->pinterViews;
    }

    /**
     * Set fbShares
     *
     * @param integer $fbShares
     *
     * @return StoreMarketplaceStat
     */
    public function setFbShares($fbShares)
    {
        $this->fbShares = $fbShares;

        return $this;
    }

    /**
     * Get fbShares
     *
     * @return integer
     */
    public function getFbShares()
    {
        return $this->fbShares;
    }

    /**
     * Set twShares
     *
     * @param integer $twShares
     *
     * @return StoreMarketplaceStat
     */
    public function setTwShares($twShares)
    {
        $this->twShares = $twShares;

        return $this;
    }

    /**
     * Get twShares
     *
     * @return integer
     */
    public function getTwShares()
    {
        return $this->twShares;
    }

    /**
     * Set instShares
     *
     * @param integer $instShares
     *
     * @return StoreMarketplaceStat
     */
    public function setInstShares($instShares)
    {
        $this->instShares = $instShares;

        return $this;
    }

    /**
     * Get instShares
     *
     * @return integer
     */
    public function getInstShares()
    {
        return $this->instShares;
    }

    /**
     * Set pinterShares
     *
     * @param integer $pinterShares
     *
     * @return StoreMarketplaceStat
     */
    public function setPinterShares($pinterShares)
    {
        $this->pinterShares = $pinterShares;

        return $this;
    }

    /**
     * Get pinterShares
     *
     * @return integer
     */
    public function getPinterShares()
    {
        return $this->pinterShares;
    }

    /**
     * Set totalCauses
     *
     * @param float $totalCauses
     *
     * @return StoreMarketplaceStat
     */
    public function setTotalCauses($totalCauses)
    {
        $this->totalCauses = $totalCauses;

        return $this;
    }

    /**
     * Get totalCauses
     *
     * @return float
     */
    public function getTotalCauses()
    {
        return $this->totalCauses;
    }

    /**
     * Set countCauses
     *
     * @param integer $countCauses
     *
     * @return StoreMarketplaceStat
     */
    public function setCountCauses($countCauses)
    {
        $this->countCauses = $countCauses;

        return $this;
    }

    /**
     * Get countCauses
     *
     * @return integer
     */
    public function getCountCauses()
    {
        return $this->countCauses;
    }

    /**
     * Set loyaltyPoints
     *
     * @param integer $loyaltyPoints
     *
     * @return StoreMarketplaceStat
     */
    public function setLoyaltyPoints($loyaltyPoints)
    {
        $this->loyaltyPoints = $loyaltyPoints;

        return $this;
    }

    /**
     * Get loyaltyPoints
     *
     * @return integer
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Set totalBoomDiscount
     *
     * @param float $totalBoomDiscount
     *
     * @return StoreMarketplaceStat
     */
    public function setTotalBoomDiscount($totalBoomDiscount)
    {
        $this->totalBoomDiscount = $totalBoomDiscount;

        return $this;
    }

    /**
     * Get totalBoomDiscount
     *
     * @return float
     */
    public function getTotalBoomDiscount()
    {
        return $this->totalBoomDiscount;
    }

    /**
     * Set countBoomDiscount
     *
     * @param integer $countBoomDiscount
     *
     * @return StoreMarketplaceStat
     */
    public function setCountBoomDiscount($countBoomDiscount)
    {
        $this->countBoomDiscount = $countBoomDiscount;

        return $this;
    }

    /**
     * Get countBoomDiscount
     *
     * @return integer
     */
    public function getCountBoomDiscount()
    {
        return $this->countBoomDiscount;
    }

    /**
     * Set totalCouponDiscount
     *
     * @param float $totalCouponDiscount
     *
     * @return StoreMarketplaceStat
     */
    public function setTotalCouponDiscount($totalCouponDiscount)
    {
        $this->totalCouponDiscount = $totalCouponDiscount;

        return $this;
    }

    /**
     * Get totalCouponDiscount
     *
     * @return float
     */
    public function getTotalCouponDiscount()
    {
        return $this->totalCouponDiscount;
    }

    /**
     * Set countCouponDiscount
     *
     * @param integer $countCouponDiscount
     *
     * @return StoreMarketplaceStat
     */
    public function setCountCouponDiscount($countCouponDiscount)
    {
        $this->countCouponDiscount = $countCouponDiscount;

        return $this;
    }

    /**
     * Get countCouponDiscount
     *
     * @return integer
     */
    public function getCountCouponDiscount()
    {
        return $this->countCouponDiscount;
    }

    /**
     * Set marketplace
     *
     * @param \Bubblz\EntitiesBundle\Entity\Marketplace $marketplace
     *
     * @return StoreMarketplaceStat
     */
    public function setMarketplace(\Bubblz\EntitiesBundle\Entity\Marketplace $marketplace = null)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return \Bubblz\EntitiesBundle\Entity\Marketplace
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return StoreMarketplaceStat
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
