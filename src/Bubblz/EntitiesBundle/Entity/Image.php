<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image", indexes={@ORM\Index(name="fk_image_store1_idx", columns={"store_id"}), @ORM\Index(name="store_id_is_main", columns={"store_id", "is_main"}), @ORM\Index(name="fk_image_brand1_idx", columns={"brand_id"})})
 * @ORM\Entity
 */
class Image
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="string", length=45, nullable=false)
     */
    private $imageName;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_main", type="integer", nullable=true)
     */
    private $isMain;

    /**
     * @var integer
     *
     * @ORM\Column(name="store_order", type="integer", nullable=true)
     */
    private $storeOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var \Brand
     *
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * })
     */
    private $brand;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Image
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set isMain
     *
     * @param integer $isMain
     *
     * @return Image
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return integer
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set storeOrder
     *
     * @param integer $storeOrder
     *
     * @return Image
     */
    public function setStoreOrder($storeOrder)
    {
        $this->storeOrder = $storeOrder;

        return $this;
    }

    /**
     * Get storeOrder
     *
     * @return integer
     */
    public function getStoreOrder()
    {
        return $this->storeOrder;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Image
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set brand
     *
     * @param \Bubblz\EntitiesBundle\Entity\Brand $brand
     *
     * @return Image
     */
    public function setBrand(\Bubblz\EntitiesBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Bubblz\EntitiesBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Image
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
