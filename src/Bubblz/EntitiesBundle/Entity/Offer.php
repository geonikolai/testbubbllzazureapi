<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 *
 * @ORM\Table(name="offer", indexes={@ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="store_id_status", columns={"store_id", "status"})})
 * @ORM\Entity
 */
class Offer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="before_checkin_message", type="string", length=100, nullable=true)
     */
    private $beforeCheckinMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="after_checkin_message", type="string", length=100, nullable=true)
     */
    private $afterCheckinMessage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="remote", type="boolean", nullable=false)
     */
    private $remote = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="unattended", type="boolean", nullable=false)
     */
    private $unattended = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=false)
     */
    private $cDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set beforeCheckinMessage
     *
     * @param string $beforeCheckinMessage
     *
     * @return Offer
     */
    public function setBeforeCheckinMessage($beforeCheckinMessage)
    {
        $this->beforeCheckinMessage = $beforeCheckinMessage;

        return $this;
    }

    /**
     * Get beforeCheckinMessage
     *
     * @return string
     */
    public function getBeforeCheckinMessage()
    {
        return $this->beforeCheckinMessage;
    }

    /**
     * Set afterCheckinMessage
     *
     * @param string $afterCheckinMessage
     *
     * @return Offer
     */
    public function setAfterCheckinMessage($afterCheckinMessage)
    {
        $this->afterCheckinMessage = $afterCheckinMessage;

        return $this;
    }

    /**
     * Get afterCheckinMessage
     *
     * @return string
     */
    public function getAfterCheckinMessage()
    {
        return $this->afterCheckinMessage;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Offer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set remote
     *
     * @param boolean $remote
     *
     * @return Offer
     */
    public function setRemote($remote)
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * Get remote
     *
     * @return boolean
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * Set unattended
     *
     * @param boolean $unattended
     *
     * @return Offer
     */
    public function setUnattended($unattended)
    {
        $this->unattended = $unattended;

        return $this;
    }

    /**
     * Get unattended
     *
     * @return boolean
     */
    public function getUnattended()
    {
        return $this->unattended;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Offer
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Offer
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Offer
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Offer
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
}
