<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coupon
 *
 * @ORM\Table(name="coupon")
 * @ORM\Entity
 */
class Coupon
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="store_id", type="integer", nullable=true)
     */
    private $storeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="brand_id", type="integer", nullable=true)
     */
    private $brandId;

    /**
     * @var integer
     *
     * @ORM\Column(name="marketplace_id", type="integer", nullable=true)
     */
    private $marketplaceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_category_id", type="integer", nullable=true)
     */
    private $boomCategoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="boom_brand_id", type="integer", nullable=true)
     */
    private $boomBrandId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_store_id", type="integer", nullable=true)
     */
    private $groupStoreId;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=true)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_coupons", type="integer", nullable=true)
     */
    private $maxCoupons;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set storeId
     *
     * @param integer $storeId
     *
     * @return Coupon
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }

    /**
     * Get storeId
     *
     * @return integer
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Set brandId
     *
     * @param integer $brandId
     *
     * @return Coupon
     */
    public function setBrandId($brandId)
    {
        $this->brandId = $brandId;

        return $this;
    }

    /**
     * Get brandId
     *
     * @return integer
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * Set marketplaceId
     *
     * @param integer $marketplaceId
     *
     * @return Coupon
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;

        return $this;
    }

    /**
     * Get marketplaceId
     *
     * @return integer
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * Set boomCategoryId
     *
     * @param integer $boomCategoryId
     *
     * @return Coupon
     */
    public function setBoomCategoryId($boomCategoryId)
    {
        $this->boomCategoryId = $boomCategoryId;

        return $this;
    }

    /**
     * Get boomCategoryId
     *
     * @return integer
     */
    public function getBoomCategoryId()
    {
        return $this->boomCategoryId;
    }

    /**
     * Set boomBrandId
     *
     * @param integer $boomBrandId
     *
     * @return Coupon
     */
    public function setBoomBrandId($boomBrandId)
    {
        $this->boomBrandId = $boomBrandId;

        return $this;
    }

    /**
     * Get boomBrandId
     *
     * @return integer
     */
    public function getBoomBrandId()
    {
        return $this->boomBrandId;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Coupon
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Coupon
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set groupStoreId
     *
     * @param integer $groupStoreId
     *
     * @return Coupon
     */
    public function setGroupStoreId($groupStoreId)
    {
        $this->groupStoreId = $groupStoreId;

        return $this;
    }

    /**
     * Get groupStoreId
     *
     * @return integer
     */
    public function getGroupStoreId()
    {
        return $this->groupStoreId;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Coupon
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set maxCoupons
     *
     * @param integer $maxCoupons
     *
     * @return Coupon
     */
    public function setMaxCoupons($maxCoupons)
    {
        $this->maxCoupons = $maxCoupons;

        return $this;
    }

    /**
     * Get maxCoupons
     *
     * @return integer
     */
    public function getMaxCoupons()
    {
        return $this->maxCoupons;
    }
}
