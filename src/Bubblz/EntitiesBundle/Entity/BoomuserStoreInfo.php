<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoomuserStoreInfo
 *
 * @ORM\Table(name="boomuser_store_info", indexes={@ORM\Index(name="fk_loyalty_boomuser_store_store1_idx", columns={"store_id"}), @ORM\Index(name="fk_loyalty_boomuser_store_boom_user1_idx", columns={"boom_user_id"})})
 * @ORM\Entity
 */
class BoomuserStoreInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="loyalty_consumed", type="integer", nullable=false)
     */
    private $loyaltyConsumed;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loyaltyConsumed
     *
     * @param integer $loyaltyConsumed
     *
     * @return BoomuserStoreInfo
     */
    public function setLoyaltyConsumed($loyaltyConsumed)
    {
        $this->loyaltyConsumed = $loyaltyConsumed;

        return $this;
    }

    /**
     * Get loyaltyConsumed
     *
     * @return integer
     */
    public function getLoyaltyConsumed()
    {
        return $this->loyaltyConsumed;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return BoomuserStoreInfo
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return BoomuserStoreInfo
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }
}
