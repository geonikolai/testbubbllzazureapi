<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContestWinners
 *
 * @ORM\Table(name="contest_winners", indexes={@ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="contest_id", columns={"contest_id"}), @ORM\Index(name="boom_id", columns={"boom_id"}), @ORM\Index(name="boom_user_id", columns={"boom_user_id"})})
 * @ORM\Entity
 */
class ContestWinners
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="draw_date", type="datetime", nullable=false)
     */
    private $drawDate;

    /**
     * @var string
     *
     * @ORM\Column(name="prize", type="string", length=64, nullable=true)
     */
    private $prize;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \Contest
     *
     * @ORM\ManyToOne(targetEntity="Contest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contest_id", referencedColumnName="id")
     * })
     */
    private $contest;

    /**
     * @var \Boom
     *
     * @ORM\ManyToOne(targetEntity="Boom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_id", referencedColumnName="id")
     * })
     */
    private $boom;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set drawDate
     *
     * @param \DateTime $drawDate
     *
     * @return ContestWinners
     */
    public function setDrawDate($drawDate)
    {
        $this->drawDate = $drawDate;

        return $this;
    }

    /**
     * Get drawDate
     *
     * @return \DateTime
     */
    public function getDrawDate()
    {
        return $this->drawDate;
    }

    /**
     * Set prize
     *
     * @param string $prize
     *
     * @return ContestWinners
     */
    public function setPrize($prize)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return string
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return ContestWinners
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set contest
     *
     * @param \Bubblz\EntitiesBundle\Entity\Contest $contest
     *
     * @return ContestWinners
     */
    public function setContest(\Bubblz\EntitiesBundle\Entity\Contest $contest = null)
    {
        $this->contest = $contest;

        return $this;
    }

    /**
     * Get contest
     *
     * @return \Bubblz\EntitiesBundle\Entity\Contest
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Set boom
     *
     * @param \Bubblz\EntitiesBundle\Entity\Boom $boom
     *
     * @return ContestWinners
     */
    public function setBoom(\Bubblz\EntitiesBundle\Entity\Boom $boom = null)
    {
        $this->boom = $boom;

        return $this;
    }

    /**
     * Get boom
     *
     * @return \Bubblz\EntitiesBundle\Entity\Boom
     */
    public function getBoom()
    {
        return $this->boom;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return ContestWinners
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }
}
