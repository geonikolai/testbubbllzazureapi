<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="boom_user_login_history")
 * @ORM\Entity
 */
class BoomUserLoginHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login_date", type="datetime")
     */
    private $loginDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="text")
     */
    private $userAgent;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loginDate
     *
     * @param \DateTime $loginDate
     *
     * @return BoomUserLoginHistory
     */
    public function setLoginDate($loginDate)
    {
        $this->loginDate = $loginDate;

        return $this;
    }

    /**
     * Get loginDate
     *
     * @return \DateTime
     */
    public function getLoginDate()
    {
        return $this->loginDate;
    }

    /**
     * Set user
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $user
     *
     * @return BoomUserLoginHistory
     */
    public function setUser(\Bubblz\EntitiesBundle\Entity\BoomUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     *
     * @return BoomUserLoginHistory
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }
}
