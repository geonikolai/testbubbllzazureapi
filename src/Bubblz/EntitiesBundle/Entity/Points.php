<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Points
 *
 * @ORM\Table(name="points", indexes={@ORM\Index(name="store_id", columns={"store_id"}), @ORM\Index(name="boom_user_id", columns={"boom_user_id"}), @ORM\Index(name="boom_user_id_store_id_point_type", columns={"boom_user_id", "store_id", "point_type"}), @ORM\Index(name="checkin_id", columns={"checkin_id"}), @ORM\Index(name="bubbllz_id", columns={"bubbllz_id"})})
 * @ORM\Entity
 */
class Points
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="point_type", type="integer", nullable=false)
     */
    private $pointType;

    /**
     * @var integer
     *
     * @ORM\Column(name="point_value", type="integer", nullable=false)
     */
    private $pointValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="redeemed", type="boolean", nullable=false)
     */
    private $redeemed = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="r_date", type="datetime", nullable=true)
     */
    private $rDate;

    /**
     * @var \BoomUser
     *
     * @ORM\ManyToOne(targetEntity="BoomUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boom_user_id", referencedColumnName="id")
     * })
     */
    private $boomUser;

    /**
     * @var \Store
     *
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * })
     */
    private $store;

    /**
     * @var \Checkin
     *
     * @ORM\ManyToOne(targetEntity="Checkin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="checkin_id", referencedColumnName="id")
     * })
     */
    private $checkin;

    /**
     * @var \Boom
     *
     * @ORM\ManyToOne(targetEntity="Boom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bubbllz_id", referencedColumnName="id")
     * })
     */
    private $bubbllz;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pointType
     *
     * @param integer $pointType
     *
     * @return Points
     */
    public function setPointType($pointType)
    {
        $this->pointType = $pointType;

        return $this;
    }

    /**
     * Get pointType
     *
     * @return integer
     */
    public function getPointType()
    {
        return $this->pointType;
    }

    /**
     * Set pointValue
     *
     * @param integer $pointValue
     *
     * @return Points
     */
    public function setPointValue($pointValue)
    {
        $this->pointValue = $pointValue;

        return $this;
    }

    /**
     * Get pointValue
     *
     * @return integer
     */
    public function getPointValue()
    {
        return $this->pointValue;
    }

    /**
     * Set redeemed
     *
     * @param boolean $redeemed
     *
     * @return Points
     */
    public function setRedeemed($redeemed)
    {
        $this->redeemed = $redeemed;

        return $this;
    }

    /**
     * Get redeemed
     *
     * @return boolean
     */
    public function getRedeemed()
    {
        return $this->redeemed;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Points
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set rDate
     *
     * @param \DateTime $rDate
     *
     * @return Points
     */
    public function setRDate($rDate)
    {
        $this->rDate = $rDate;

        return $this;
    }

    /**
     * Get rDate
     *
     * @return \DateTime
     */
    public function getRDate()
    {
        return $this->rDate;
    }

    /**
     * Set boomUser
     *
     * @param \Bubblz\EntitiesBundle\Entity\BoomUser $boomUser
     *
     * @return Points
     */
    public function setBoomUser(\Bubblz\EntitiesBundle\Entity\BoomUser $boomUser = null)
    {
        $this->boomUser = $boomUser;

        return $this;
    }

    /**
     * Get boomUser
     *
     * @return \Bubblz\EntitiesBundle\Entity\BoomUser
     */
    public function getBoomUser()
    {
        return $this->boomUser;
    }

    /**
     * Set store
     *
     * @param \Bubblz\EntitiesBundle\Entity\Store $store
     *
     * @return Points
     */
    public function setStore(\Bubblz\EntitiesBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Bubblz\EntitiesBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set checkin
     *
     * @param \Bubblz\EntitiesBundle\Entity\Checkin $checkin
     *
     * @return Points
     */
    public function setCheckin(\Bubblz\EntitiesBundle\Entity\Checkin $checkin = null)
    {
        $this->checkin = $checkin;

        return $this;
    }

    /**
     * Get checkin
     *
     * @return \Bubblz\EntitiesBundle\Entity\Checkin
     */
    public function getCheckin()
    {
        return $this->checkin;
    }

    /**
     * Set bubbllz
     *
     * @param \Bubblz\EntitiesBundle\Entity\Boom $bubbllz
     *
     * @return Points
     */
    public function setBubbllz(\Bubblz\EntitiesBundle\Entity\Boom $bubbllz = null)
    {
        $this->bubbllz = $bubbllz;

        return $this;
    }

    /**
     * Get bubbllz
     *
     * @return \Bubblz\EntitiesBundle\Entity\Boom
     */
    public function getBubbllz()
    {
        return $this->bubbllz;
    }
}
