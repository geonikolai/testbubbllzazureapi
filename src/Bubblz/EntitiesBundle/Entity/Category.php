<?php

namespace Bubblz\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="parent", type="string", length=45, nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=45, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_tags", type="string", length=255, nullable=true)
     */
    private $hashTags;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=45, nullable=true)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="c_date", type="datetime", nullable=true)
     */
    private $cDate;

    /**
     * @var string
     *
     * @ORM\Column(name="color_code", type="string", length=20, nullable=true)
     */
    private $colorCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_manual", type="integer", nullable=false)
     */
    private $isManual = '0';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return Category
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Category
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set hashTags
     *
     * @param string $hashTags
     *
     * @return Category
     */
    public function setHashTags($hashTags)
    {
        $this->hashTags = $hashTags;

        return $this;
    }

    /**
     * Get hashTags
     *
     * @return string
     */
    public function getHashTags()
    {
        return $this->hashTags;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Category
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set cDate
     *
     * @param \DateTime $cDate
     *
     * @return Category
     */
    public function setCDate($cDate)
    {
        $this->cDate = $cDate;

        return $this;
    }

    /**
     * Get cDate
     *
     * @return \DateTime
     */
    public function getCDate()
    {
        return $this->cDate;
    }

    /**
     * Set colorCode
     *
     * @param string $colorCode
     *
     * @return Category
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;

        return $this;
    }

    /**
     * Get colorCode
     *
     * @return string
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * Set isManual
     *
     * @param integer $isManual
     *
     * @return Category
     */
    public function setIsManual($isManual)
    {
        $this->isManual = $isManual;

        return $this;
    }

    /**
     * Get isManual
     *
     * @return integer
     */
    public function getIsManual()
    {
        return $this->isManual;
    }
}
