<?php
namespace Bubblz\EntitiesBundle\EventListeners;

use Bubblz\EntitiesBundle\Managers\UserLoginHistoryManager;
use Bubblz\OAuthServerBundle\Events\UserLogedInEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserLoginSubscriber  implements EventSubscriberInterface
{
    /**
     *
     * @var UserLoginHistoryManager
     */
    protected $em;
    
    public function __construct(UserLoginHistoryManager $em) {
        $this->em = $em;
    }


    public static function getSubscribedEvents()
    {
        return array(
            UserLogedInEvent::NAME=> 'onUserLogedIn',
        );
    }

    public function onUserLogedIn(UserLogedInEvent $event)
    {
        $user = $event->getUser();
        
        $userHistory = $this->em->CreateUserLoginHistory($user);
        $userHistory->setUserAgent($event->getUserAgent());
        
        $this->em->UpdateUserLoginHistory($userHistory);
    }
}
