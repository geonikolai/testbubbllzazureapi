<?php

namespace Bubblz\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/stores")
 */
class StoresController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $a = "kostas";
        return $this->render('BubblzBackendBundle:Stores:index.html.twig', array(
            'databaseName' => $a
        ));
    }

}
