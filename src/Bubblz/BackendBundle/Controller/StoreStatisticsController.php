<?php

namespace Bubblz\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
     * @Route("/storestatistics")
 */
class StoreStatisticsController extends Controller
{
    /**
     * @Route("/bubbllz")
     */
    public function bubbllzAction()
    {
        return $this->render('BubblzBackendBundle:StoreStatistics:bubbllz.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/checkins")
     */
    public function checkinsAction()
    {
        return $this->render('BubblzBackendBundle:StoreStatistics:checkins.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/loyaltyPoints")
     */
    public function loyaltyPointsAction()
    {
        return $this->render('BubblzBackendBundle:StoreStatistics:loyalty_points.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/contests")
     */
    public function contestsAction()
    {
        return $this->render('BubblzBackendBundle:StoreStatistics:contests.html.twig', array(
            // ...
        ));
    }

}
