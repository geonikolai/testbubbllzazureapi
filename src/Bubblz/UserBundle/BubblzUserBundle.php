<?php

namespace Bubblz\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BubblzUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
