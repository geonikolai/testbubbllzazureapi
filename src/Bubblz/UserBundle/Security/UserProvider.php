<?php

namespace Bubblz\UserBundle\Security;

use Bubblz\EntitiesBundle\Entity\BoomUser;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\UserProvider as BaseUserProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserProvider extends BaseUserProvider
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __construct(UserManagerInterface $userManager, $container)
    {
        parent::__construct($userManager);
        $this->container = $container;
    }

    public function loadUserByFacebookId($facebookId)
    {
        /* @var $em EntityManager */
        $em = $this->container->get("doctrine")->getEntityManager();


        try
        {
            /* @var $user BoomUser */
            $user = $em->getRepository("BubblzEntitiesBundle:BoomUser")->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.fbId = :id')
                    ->setParameter('id', $facebookId)
                    ->getQuery()
                    ->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $ex)
        {
            throw new \Symfony\Component\Security\Core\Exception\AuthenticationException();
        }

        if ($user->getStatus() == false)
        {
            throw new \Exception(sprintf('User is not enabled.'));
        }
        
        if (!$user)
        {
            throw new UsernameNotFoundException(sprintf('Facebook Id "%s" does not exist.', $facebookId));
        }

        return $user;
    }

}
